#!/bin/bash

readonly APP_IMAGE=vrcafehaarlem/vrcafe-app
readonly APP_TAG=latest

readonly BACKEND_IMAGE=vrcafehaarlem/vrcafe-backend
readonly BACKEND_TAG=latest


app=$(docker pull $APP_IMAGE:$APP_TAG)
backend=$(docker pull $BACKEND_IMAGE:$BACKEND_TAG)

if [[ $app != *"up to date"* || $backend != *"up to date"* ]]; then
    echo "an updated image has been donwloaded for '$IMAGE:$TAG'"
    # we actually launch a script here:
    docker-compose up -d
    docker image prune -af
else
    echo "no updates available for"
fi
