package nl.vrcafe.backend.service

import nl.vrcafe.backend.controller.EMAIL_ALREADY_EXISTS
import nl.vrcafe.backend.model.form.RegisterForm
import org.junit.jupiter.api.Assertions.assertAll
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.function.Executable
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.jdbc.SqlGroup
import org.springframework.web.server.ResponseStatusException

@SqlGroup(
    Sql(
        scripts = ["/InsertRoles.sql",
          "/InsertProfiles.sql",
          "/InsertAccounts.sql",
          "/InsertAccountRoles.sql",
          "/InsertXp.sql"],
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
    ),
    Sql(scripts = ["/EmptyDB.sql"], executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
)
class AccountServiceTests {

    @SpringBootTest
    @Nested
    inner class CreateAccount {


        @Autowired
        lateinit var accountService: AccountService

        @Test
        fun `should create an account`() {
            val registerForm = RegisterForm("admin-test", "admin-test")
            val account = accountService.register(registerForm)
            assert(account!!.email == "admin-test")
        }

        @Test
        fun `create account with existing email`() {
            val registerForm = RegisterForm("admin-test", "admin-test")
            accountService.register(registerForm)
            val result: ResponseStatusException = assertThrows { accountService.register(registerForm) }
            assertAll(
                Executable { assertEquals(409, result.statusCode.value()) },
                Executable { assertEquals(EMAIL_ALREADY_EXISTS, result.reason) }
            )
        }
    }
}
