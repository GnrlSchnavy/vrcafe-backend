package nl.vrcafe.backend.service

import nl.vrcafe.backend.Utils
import nl.vrcafe.backend.controller.LOCATION_NOT_FOUND
import nl.vrcafe.backend.controller.ROLE_NOT_FOUND
import nl.vrcafe.backend.model.form.CreateLocationForm
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.jdbc.SqlGroup
import org.springframework.web.server.ResponseStatusException

@SpringBootTest
class LocationServiceTests {

  @Autowired
  lateinit var locationService: LocationService

  @SpringBootTest
  @SqlGroup(
    Sql(
      scripts = ["/InsertRoles.sql", "/InsertAccounts.sql", "/InsertAccountRoles.sql", "/InsertLocations.sql"],
      executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
    ),
    Sql(scripts = ["/EmptyDB.sql"], executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
  )

  @Nested
  inner class CRUDLocations {


    @Test
    fun `should create a location`() {
      val location = locationService.createLocation(CreateLocationForm("location test name", "location test city", "location test street", "location test housenumber"))
      assert(location.id != null)
      assert(location.name == "location test name")
      assert(location.city == "location test city")
      assert(location.street == "location test street")
      assert(location.houseNumber == "location test housenumber")
    }

    @Test
    fun `should find a location`() {
      val foundLocation = locationService.getLocation(1L)
      assert(foundLocation.name == "VrcafeHaarlem")
      assert(foundLocation.city == "Haarlem")
      assert(foundLocation.street == "Stationsplein")
      assert(foundLocation.houseNumber == "68")
    }

    @Test
    fun `should find all locations`() {
      val locations = locationService.getAllLocations()
      assert(locations!!.size == 2)
    }

    @Test
    fun `should update a location`() {
      locationService.updateLocation(1, CreateLocationForm("updated location name"))
      val updatedLocation = locationService.getLocation(1)
      assert(updatedLocation.name == "updated location name")
    }

    @Test
    fun `should delete a location`() {
      locationService.deleteLocation(1)
      val deletedLocation: ResponseStatusException = assertThrows { locationService.getLocation(1) }
      Assertions.assertEquals(LOCATION_NOT_FOUND, deletedLocation.reason)
    }




  }

}
