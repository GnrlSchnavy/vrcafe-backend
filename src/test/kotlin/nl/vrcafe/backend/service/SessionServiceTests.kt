package nl.vrcafe.backend.service

import nl.vrcafe.backend.handler.WebSocketHandler
import nl.vrcafe.backend.repo.XpAccountHistoryRepo
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.jdbc.SqlGroup
import org.springframework.web.socket.TextMessage
import org.springframework.web.socket.WebSocketSession


@SpringBootTest

class SessionServiceTests {


  @Autowired
  lateinit var sessionService: SessionService

  @Autowired
  lateinit var xpAccountHistoryRepo: XpAccountHistoryRepo

  @Autowired
  lateinit var userDetailsService: MyUserDetailsService

  @SpringBootTest
  @SqlGroup(
    Sql(
      scripts = [
        "/InsertRoles.sql",
        "/InsertProfiles.sql",
        "/InsertAccounts.sql",
        "/InsertAccountRoles.sql",
        "/InsertLocations.sql",
        "/InsertXp.sql",
        "/InsertProducts.sql",
        "/InsertSessions.sql"
      ],
      executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
    ),
    Sql(scripts = ["/EmptyDB.sql"], executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
  )

  @Nested
  inner class CreateSessionTests {

    @Test
    fun `open session`() {
      val session: WebSocketSession = mock(WebSocketSession::class.java)
      WebSocketHandler().afterConnectionEstablished(session)
      WebSocketHandler().handleMessage(
        session,
        TextMessage("{\"command\":\"LOGIN\",\"email\":\"admin\"}".toByteArray())
      )
      val scannedSession = sessionService.scanSession("b4f709df-f807-4f58-aa33-1cdfee15e458")
      assertEquals(null, scannedSession.checkOut)
      assertEquals(null, scannedSession.minutes)
    }

    @Test
    fun `open and close session`() {
      val session: WebSocketSession = mock(WebSocketSession::class.java)
      WebSocketHandler().afterConnectionEstablished(session)
      WebSocketHandler().handleMessage(
        session,
        TextMessage("{\"command\":\"LOGIN\",\"email\":\"admin\"}".toByteArray())
      )
      val openSession = sessionService.scanSession("b4f709df-f807-4f58-aa33-1cdfee15e458")
      val closedSession = sessionService.scanSession("b4f709df-f807-4f58-aa33-1cdfee15e458")
      val historyEntry = xpAccountHistoryRepo.findAll()
      assertTrue(historyEntry.isNotEmpty())
      assertTrue(closedSession.checkOut != null)
      assertTrue(closedSession.minutes != null)
    }

  }

}
