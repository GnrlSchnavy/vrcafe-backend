package nl.vrcafe.backend.service

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class QrCodeServiceTests {

  @Autowired
  lateinit var qrCodeService: QrCodeService

  @Test
  fun `generate QR Image`() {
    val image = qrCodeService.generateQRCodeImage("test")
    assertEquals(-1, image.getRGB(22,22))
    assertEquals(-16777216, image.getRGB(43,88))
  }

  @Test
  fun `generate QR Base 64`(){
    val base64 = qrCodeService.generateQRCodeImageBase64("test")
    assertEquals("iVBORw0KGgoAAAANSUhEUgAAAMgAAADIAQAAAACFI5MzAAAA4UlEQVR4Xu2VQQ6FIAxE64pjcFOFm3IMVvR3ihij3zWzYGII9LmYtBkQ/ZI8C5cWWQRahIZUMW1ajhoTtpGJYJtrbEHzOPKQJDGba3DZCEl3zUlUndMRHb3M40hDPCUVTX2mZDoZSr3WRULgGn6PKnsoe6AisYnVbOAWlGKtJSKqzfMhwS5mW73EQtxplQO/2NNBR7KW7vo+bQLisl665TMrJKTXrKN2u7zzM5lEW6yjO1yX+7QZSEJK8LWnay7y7XomSUiJP7ZMxBa4xu3CRnpKTtfpnZ+J5K8WWQRahJv8AGnJc2zBo3PLAAAAAElFTkSuQmCC", base64)
  }

}
