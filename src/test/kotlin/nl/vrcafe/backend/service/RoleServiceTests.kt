package nl.vrcafe.backend.service

import nl.vrcafe.backend.controller.ROLE_NOT_FOUND
import nl.vrcafe.backend.model.form.CreateRoleForm
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.jdbc.SqlGroup
import org.springframework.web.server.ResponseStatusException

@SpringBootTest
class RoleServiceTests {


  @Autowired
  lateinit var roleService: RoleService


  @SpringBootTest
  @SqlGroup(
    Sql(
      scripts = ["/InsertRoles.sql", "/InsertAccounts.sql", "/InsertAccountRoles.sql"],
      executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
    ),
    Sql(scripts = ["/EmptyDB.sql"], executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
  )

  @Nested
  inner class CRUDRoles {

    @Test
    fun `should create a role`() {
      val role = roleService.createRole(CreateRoleForm("test"))
      assert(role.id != null)
      assert(role.name == "test")
    }

    @Test
    fun `should find a role`() {
      val role = roleService.getRole(1)
      assert(role.id == 1L)
      assert(role.name == "ROLE_ADMIN")
    }

    @Test
    fun `should find all roles`() {
      val roles = roleService.getAllRoles()
      assert(roles.size == 2)
    }

    @Test
    fun `should update a role`() {
      val role = roleService.getRole(1)
      roleService.updateRole(1, CreateRoleForm("test2"))
      val updatedRole = roleService.getRole(1)
      assert(updatedRole.name == "test2")
    }

    @Test
    fun `should delete a role`() {
      roleService.deleteRole(1)
      val deletedRole: ResponseStatusException = assertThrows { roleService.getRole(1) }
      assertEquals(ROLE_NOT_FOUND, deletedRole.reason)
    }
  }

}
