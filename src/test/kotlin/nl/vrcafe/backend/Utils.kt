package nl.vrcafe.backend

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import nl.vrcafe.backend.model.form.LoginForm
import nl.vrcafe.backend.model.view.AccountView
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders


open class Utils {

  private val mapper = jacksonObjectMapper()

  @Autowired
  open lateinit var testRestTemplate: TestRestTemplate

  fun loginAsAdmin(): String? {
    val response = testRestTemplate.postForEntity("/api/login", HttpEntity(LoginForm("admin","admin")), String::class.java)
    return mapper.readValue(response.body, AccountView::class.java).jwt
  }

  fun generateAdminJwtHeaders(): HttpHeaders {
    val jwt = loginAsAdmin()
    val headers = HttpHeaders()
    headers.add("Authorization", "Bearer $jwt")
    return headers
  }

  fun generateEmptyHeaders(): HttpHeaders {
    return HttpHeaders()
  }

  fun loginWith(username: String, password: String): String? {
    val response = testRestTemplate.postForEntity("/api/login", HttpEntity(LoginForm(username,password)), String::class.java)
    return mapper.readValue(response.body, AccountView::class.java).jwt
  }

  fun generateJWTfor(email: String, password: String): HttpHeaders {
    val jwt = loginWith(email, password)
    val headers = HttpHeaders()
    headers.add("Authorization", "Bearer $jwt")
    return headers
  }
}

