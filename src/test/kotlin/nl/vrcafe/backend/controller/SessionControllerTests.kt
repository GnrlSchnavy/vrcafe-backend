package nl.vrcafe.backend.controller

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import nl.vrcafe.backend.Utils
import nl.vrcafe.backend.model.form.AddProductsToSessionForm
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.jdbc.SqlGroup

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class SessionControllerTests : Utils() {

  @Autowired
  override lateinit var testRestTemplate: TestRestTemplate

  private val mapper = jacksonObjectMapper()


  @SqlGroup(
    Sql(
      scripts = [
        "/InsertRoles.sql",
        "/InsertProfiles.sql",
        "/InsertAccounts.sql",
        "/InsertAccountRoles.sql",
        "/InsertLocations.sql",
        "/InsertXp.sql",
        "/InsertProducts.sql",
        "/InsertXpAccountHistory.sql",
        "/InsertSessions.sql"
      ],
      executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
    ),
    Sql(scripts = ["/EmptyDB.sql"], executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
  )

  @Nested
  inner class Session {

    @Test
    fun `add products to user in session`() {
      val listOfProducts = listOf(AddProductsToSessionForm(1, 2), AddProductsToSessionForm(2, 3))
      val qrCode = "asdf"
      val addProductsResponse = testRestTemplate.exchange(
        "/api/session/addProductsToSession/$qrCode",
        HttpMethod.POST,
        HttpEntity<Any>(listOfProducts, generateAdminJwtHeaders()),
        String::class.java
      )
      assert(addProductsResponse.statusCode.value() == 200)
      val sessionOverviewResponse = testRestTemplate.exchange(
        "/api/session/latestSessionSummary",
        HttpMethod.GET,
        HttpEntity<Any>(generateJWTfor("session_user_test", "user")),
        String::class.java
      )
      assert(sessionOverviewResponse.statusCode.value() == 200)
      assert(sessionOverviewResponse.body.toString().contains("\"name\":\"Playtime\""))
    }

    @Test
    fun `get all sessions`() {
      val response = testRestTemplate.exchange(
        "/api/session",
        HttpMethod.GET,
        HttpEntity<Any>(generateJWTfor("session_user_test", "user")),
        String::class.java
      )
      assert(response.statusCode.value() == 200)
      assert(response.body.toString().contains("sessionId"))
      assert(response.body.toString().contains("99"))
    }
  }
}
