package nl.vrcafe.backend.controller

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import nl.vrcafe.backend.Utils
import nl.vrcafe.backend.model.DataContainer
import nl.vrcafe.backend.model.view.TinyAccountView
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.jdbc.SqlGroup

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class FriendControllerTests: Utils() {


    private val mapper = jacksonObjectMapper().registerModule(JavaTimeModule())
        .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)

    @SqlGroup(
        Sql(
            scripts = [
                "/InsertRoles.sql",
                "/InsertProfiles.sql",
                "/InsertAccounts.sql",
                "/InsertAccountRoles.sql",
                "/InsertXp.sql",
                "/InsertXpAccountHistory.sql"
            ],
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
        ),
        Sql(scripts = ["/EmptyDB.sql"], executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    )

    @Nested
    inner class FriendCRUD {

        @Test
        fun `send friend request`() {
            val adminHeaders = generateAdminJwtHeaders()
            val userRequest: HttpEntity<Any> = HttpEntity(adminHeaders)
            val response = testRestTemplate.exchange(
                "/api/friend/add/2",
                HttpMethod.GET,
                userRequest,
                TinyAccountView::class.java
            )
            val profile = response.body as TinyAccountView
            assert(response.statusCode.is2xxSuccessful)
            assert(profile.accountId == 2L)
            val friendListResponse = testRestTemplate.exchange(
                "/api/friend",
                HttpMethod.GET,
                userRequest,
                String::class.java
            )
            assert(friendListResponse.statusCodeValue == 200)
            val dataContainer = mapper.readValue(friendListResponse.body, DataContainer::class.java)
            val friendList = mapper.convertValue(dataContainer.data, object : TypeReference<List<TinyAccountView>>() {})
            assert(friendList.isEmpty())
            val userHeaders = generateJWTfor("user","user")
            val friendRequest: HttpEntity<Any> = HttpEntity(userHeaders)
            val friendListResponse2 = testRestTemplate.exchange(
                "/api/friend",
                HttpMethod.GET,
                friendRequest,
                String::class.java
            )
            assert(friendListResponse2.statusCodeValue == 200)
            val dataContainer2 = mapper.readValue(friendListResponse2.body, DataContainer::class.java)
            val friendList2 = mapper.convertValue(dataContainer2.data, object : TypeReference<List<TinyAccountView>>() {})
            assert(friendList2.isEmpty())
        }

        @Test
        fun `send friend request and accept`() {
        //send friend request
            val adminHeaders = generateAdminJwtHeaders()
            val userRequest: HttpEntity<Any> = HttpEntity(adminHeaders)
            val response = testRestTemplate.exchange(
                "/api/friend/add/2",
                HttpMethod.GET,
                userRequest,
                TinyAccountView::class.java
            )
            val profile = response.body as TinyAccountView
            assert(response.statusCode.is2xxSuccessful)
            assert(profile.accountId == 2L)
        //accept friend request
            val userHeaders = generateJWTfor("user","user")
            val friendRequest: HttpEntity<Any> = HttpEntity(userHeaders)
            val friendListResponse2 = testRestTemplate.exchange(
                "/api/friend/accept/1",
                HttpMethod.GET,
                friendRequest,
                String::class.java
            )
            assert(friendListResponse2.statusCodeValue == 200)
        //check if friends from friends side
            val friendListResponse3 = testRestTemplate.exchange(
                "/api/friend",
                HttpMethod.GET,
                friendRequest,
                String::class.java
            )
            assert(friendListResponse3.statusCodeValue == 200)
            val dataContainer3 = mapper.readValue(friendListResponse3.body, DataContainer::class.java)
            val friendList3 = mapper.convertValue(dataContainer3.data, object : TypeReference<List<TinyAccountView>>() {})
            assert(friendList3.size == 1)
        //check if friends from requester side
            val friendListResponse4 = testRestTemplate.exchange(
                "/api/friend",
                HttpMethod.GET,
                userRequest,
                String::class.java
            )
            assert(friendListResponse4.statusCodeValue == 200)
            val dataContainer4 = mapper.readValue(friendListResponse3.body, DataContainer::class.java)
            val friendList4 = mapper.convertValue(dataContainer4.data, object : TypeReference<List<TinyAccountView>>() {})
            assert(friendList4.size == 1)

        }
    }
}
