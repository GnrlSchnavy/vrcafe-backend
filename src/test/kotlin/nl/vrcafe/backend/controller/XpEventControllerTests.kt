package nl.vrcafe.backend.controller

import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import nl.vrcafe.backend.Utils
import nl.vrcafe.backend.model.form.CreateProductXpEvent
import nl.vrcafe.backend.model.form.CreateXpEventForm
import nl.vrcafe.backend.model.jpa.xp.XpMod
import nl.vrcafe.backend.service.XpMapService
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.jdbc.SqlGroup
import java.time.LocalDateTime


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class XpEventControllerTests : Utils() {

  @Autowired
  override lateinit var testRestTemplate: TestRestTemplate

  @Autowired
  lateinit var xpMapService: XpMapService

  private val mapper = jacksonObjectMapper().registerModule(JavaTimeModule())
    .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)

  @SqlGroup(
    Sql(
      scripts = [
        "/InsertRoles.sql",
        "/InsertProfiles.sql",
        "/InsertAccounts.sql",
        "/InsertAccountRoles.sql",
        "/InsertProducts.sql",
        "/InsertXp.sql",
        "/InsertXpAccountHistory.sql"
      ],
      executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
    ),
    Sql(scripts = ["/EmptyDB.sql"], executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
  )

  @Nested
  inner class XpEventCRUD {

    @Test
    fun `get xp event by id`(){
      val response = testRestTemplate.exchange("/api/xp-event/99", HttpMethod.GET, HttpEntity<Any>(generateAdminJwtHeaders()), String::class.java)
      assert(response.statusCode.value() == 200)
    }

    @Test
    fun `get all xp events`(){
      val response = testRestTemplate.exchange("/api/xp-event", HttpMethod.GET, HttpEntity<Any>(generateAdminJwtHeaders()), String::class.java)
      assert(response.statusCode.value() == 200)

    }

    @Test
    fun `delete xp event`(){
      val response = testRestTemplate.exchange("/api/xp-event/99", HttpMethod.DELETE, HttpEntity<Any>(generateAdminJwtHeaders()), String::class.java)
      assert(response.statusCode.value() == 200)
    }

    @Test fun `update xp event with no updated mods`(){
      val toUpdateEvent = CreateXpEventForm(
        id = 99L,
        name = "updated",
        description = "start end after activated"
      )
    }
    @Test fun `update xp event withupdated mods`(){
      val toUpdateEvent = CreateXpEventForm(
        id = 99L,
        name = "updated",
        description = "start end after activated",
        eventProducts = listOf(CreateProductXpEvent(1,12,12))
      )
    }

    @Test
    @Disabled
    fun `create new xp event that should be activated`() {
      val xpEventFormRequest = CreateXpEventForm(
        null,
        "Test event",
        "Test event description",
        LocalDateTime.now().minusDays(1),
        LocalDateTime.now().plusDays(1),
        listOf(
          CreateProductXpEvent(
            1, 1, 10
          )
        )
      )
      val response = testRestTemplate.postForEntity("/api/xp-event", HttpEntity<Any>(xpEventFormRequest, generateAdminJwtHeaders()), String::class.java)
      assert(response.statusCodeValue == 200)
      assert(response.body!!.contains("\"activated\":true"))
      val xpMod: XpMod? = xpMapService.findXpMapById(1)!!.xpMod
      assert(xpMod != null)
    }

    @Test
    fun `create new xp event that should not be activated`() {
      val xpEventFormRequest = CreateXpEventForm(
        null,
        "Test event",
        "Test event description",
        LocalDateTime.now().minusDays(2),
        LocalDateTime.now().minusDays(1),
        listOf(
          CreateProductXpEvent(
            1, 1, 10
          )
        )
      )
      val response = testRestTemplate.postForEntity("/api/xp-event", HttpEntity<Any>(xpEventFormRequest, generateAdminJwtHeaders()), String::class.java)
      assert(response.statusCodeValue == 200)
      assert(response.body!!.contains("\"activated\":false"))
    }

    @Test
    fun `create new future xp event that should not be activated`() {
      val xpEventFormRequest = CreateXpEventForm(
        -1,
        "Test event",
        "Test event description",
        LocalDateTime.now().plusDays(1),
        LocalDateTime.now().plusDays(2),
        listOf(
          CreateProductXpEvent(
            1, 1, 10
          )
        )
      )
      val response = testRestTemplate.postForEntity("/api/xp-event", HttpEntity<CreateXpEventForm>(xpEventFormRequest, generateAdminJwtHeaders()), String::class.java)
      assert(response.statusCodeValue == 200)
      assert(response.body!!.contains("\"activated\":false"))
    }
  }


}




