package nl.vrcafe.backend.controller

import nl.vrcafe.backend.Utils
import nl.vrcafe.backend.model.form.CreateLocationForm
import nl.vrcafe.backend.model.jpa.Location
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.jdbc.SqlGroup

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class LocationControllerTests : Utils() {

  @Autowired
  override lateinit var testRestTemplate: TestRestTemplate

  @SqlGroup(
    Sql(
      scripts = [
        "/InsertRoles.sql",
        "/InsertLocations.sql",
        "/InsertProfiles.sql",
        "/InsertAccounts.sql",
        "/InsertAccountRoles.sql",
        "/InsertXp.sql",
        "/InsertXpAccountHistory.sql"
      ],
      executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
    ),
    Sql(scripts = ["/EmptyDB.sql"], executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
  )

  @Nested
  inner class LocationCRUD {

    @Test
    fun `create location`() {
      val createLocationForm = CreateLocationForm(
        name = "Test Location",
        city = "Test City Description",
        street = "street",
        houseNumber = "123"
      )
      val request: HttpEntity<CreateLocationForm> =
        HttpEntity<CreateLocationForm>(createLocationForm, generateAdminJwtHeaders())
      val response = testRestTemplate.exchange("/api/location", HttpMethod.POST, request, Location::class.java)
      val location = response.body as Location
      assert(response.statusCodeValue == 200)
      assert(location.name == "Test Location")
    }

    @Test
    fun `get locations`() {
      val headers = generateAdminJwtHeaders()
      val request: HttpEntity<Any> = HttpEntity(headers)
      val response = testRestTemplate.exchange("/api/location", HttpMethod.GET, request, String::class.java)
      assert(response.statusCodeValue == 200)
    }

    @Test
    fun `remove location`() { //TODO check if delete but im lazy
      val response = testRestTemplate.exchange("/api/location/1", HttpMethod.DELETE, HttpEntity<Any>(generateAdminJwtHeaders()), String::class.java)
      assert(response.statusCodeValue == 202)
    }

    @Test
    fun `update location`() {
      val updateLocationRequest = CreateLocationForm(name = "updated location")
      val putResponse = testRestTemplate.exchange(
        "/api/location/1",
        HttpMethod.PUT,
        HttpEntity<Any>(updateLocationRequest, generateAdminJwtHeaders()),
        String::class.java
      )
      assert(putResponse.statusCodeValue == 200)
      assert(putResponse.body.toString().contains("updated location"))
    }

    @Test
    fun `get location by id`() {
      val response = testRestTemplate.exchange(
        "/api/location/2", HttpMethod.GET, HttpEntity<Any>(generateAdminJwtHeaders()), Location::class.java
      )
      val location = response.body as Location
      assert(response.statusCodeValue == 200)
      assert(location.name == "VrcafeAmsterdam")
    }
  }
}
