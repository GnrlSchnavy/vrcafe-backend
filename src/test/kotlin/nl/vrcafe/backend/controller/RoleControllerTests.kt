package nl.vrcafe.backend.controller

import nl.vrcafe.backend.Utils
import nl.vrcafe.backend.model.form.CreateRoleForm
import nl.vrcafe.backend.model.jpa.account.Role
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.jdbc.SqlGroup


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class RoleControllerTests : Utils() {

  @Autowired
  override lateinit var testRestTemplate: TestRestTemplate

  @SqlGroup(
    Sql(
      scripts = [
        "/InsertRoles.sql",
        "/InsertProfiles.sql",
        "/InsertAccounts.sql",
        "/InsertAccountRoles.sql",
        "/InsertXp.sql",
        "/InsertXpAccountHistory.sql"
      ],
      executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
    ),
    Sql(scripts = ["/EmptyDB.sql"], executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
  )

  @Nested
  inner class RoleCRUD {

    @Test
    fun `create role`() {
      val createRoleRequest = CreateRoleForm(name = "test role")
      val request: HttpEntity<CreateRoleForm> = HttpEntity<CreateRoleForm>(createRoleRequest, generateAdminJwtHeaders())
      val response = testRestTemplate.exchange("/api/role", HttpMethod.POST, request, Role::class.java)
      val role = response.body
      assert(response.statusCodeValue == 200)
      assert(role!!.name == "test role")
    }

    @Test
    fun `get roles`() {
      val response = testRestTemplate.exchange("/api/role", HttpMethod.GET, HttpEntity<Any>(generateAdminJwtHeaders()), String::class.java)
      assert(response.statusCodeValue == 200)
    }

    @Test
    fun `remove role`() { //TODO check if delete but im lazy
      val deleteResponse = testRestTemplate.exchange("/api/role/2", HttpMethod.DELETE, HttpEntity<Any>(generateAdminJwtHeaders()), Role::class.java)
      assert(deleteResponse.statusCodeValue == 202)
      val getResponse = testRestTemplate.exchange("/api/role/2", HttpMethod.GET, HttpEntity<Any>(generateAdminJwtHeaders()), String::class.java)
      assert(getResponse.statusCodeValue == 404)
    }

    @Test
    fun `update role`() {
      val updateRoleRequest = CreateRoleForm(name = "updated test role")
      val putResponse = testRestTemplate.exchange("/api/role/1", HttpMethod.PUT, HttpEntity<Any>(updateRoleRequest, generateAdminJwtHeaders()), String::class.java)
      assert(putResponse.statusCodeValue == 200)
      assert(putResponse.body.toString().contains("updated test role"))
    }
    @Test
    fun `get role by id`() {
      val response = testRestTemplate.exchange(
        "/api/role/2", HttpMethod.GET, HttpEntity<Any>(generateAdminJwtHeaders()), String::class.java
      )
      assert(response.statusCodeValue == 200)
    }
  }
}
