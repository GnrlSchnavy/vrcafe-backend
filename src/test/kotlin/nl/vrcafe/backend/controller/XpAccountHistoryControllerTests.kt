package nl.vrcafe.backend.controller

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import nl.vrcafe.backend.Utils
import nl.vrcafe.backend.model.DataContainer
import nl.vrcafe.backend.model.view.XpAccountHistoryView
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.exchange
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.RequestEntity
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.jdbc.SqlGroup
import java.net.URI


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class XpAccountHistoryControllerTests : Utils() {

  @Autowired
  override lateinit var testRestTemplate: TestRestTemplate

  private val mapper = jacksonObjectMapper()

  @SqlGroup(
    Sql(
      scripts = [
        "/InsertRoles.sql",
        "/InsertProfiles.sql",
        "/InsertAccounts.sql",
        "/InsertAccountRoles.sql",
        "/InsertProducts.sql",
        "/InsertXp.sql",
        "/InsertXpAccountHistory.sql"
      ],
      executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
    ),
    Sql(scripts = ["/EmptyDB.sql"], executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
  )

  @Test
  fun `should return xp history for account`() {
    val response = testRestTemplate.exchange<String>("/api/xphistory?offset=0&size=10", HttpMethod.GET, HttpEntity<Any>( generateAdminJwtHeaders()), String::class.java)
    val dataContainer = mapper.readValue(response.body, DataContainer::class.java)
    val xpAccountHistoryList= mapper.convertValue(dataContainer.data, object : TypeReference<List<XpAccountHistoryView>>() {})
    val response2 = testRestTemplate.exchange<String>("/api/xphistory?offset=10&size=10", HttpMethod.GET, HttpEntity<Any>( generateAdminJwtHeaders()), String::class.java)
    val dataContainer2 = mapper.readValue(response2.body, DataContainer::class.java)
    val xpAccountHistoryList2 = mapper.convertValue(dataContainer2.data, object : TypeReference<List<XpAccountHistoryView>>() {})
    println(xpAccountHistoryList2.size)
    println(xpAccountHistoryList)
    println(xpAccountHistoryList2)
    assert(xpAccountHistoryList.size == 10)
    assert(xpAccountHistoryList2.size == 9)
  }

}

