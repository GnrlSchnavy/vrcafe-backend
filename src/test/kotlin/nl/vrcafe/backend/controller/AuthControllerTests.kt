package nl.vrcafe.backend.controller

import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import nl.vrcafe.backend.Utils
import nl.vrcafe.backend.model.form.RegisterForm
import nl.vrcafe.backend.model.view.AccountView
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.jdbc.SqlGroup

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AuthControllerTests : Utils() {

  @Autowired
  override lateinit var testRestTemplate: TestRestTemplate

  private val mapper = jacksonObjectMapper().registerModule(JavaTimeModule())
    .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)

  @SqlGroup(
    Sql(
      scripts = [
        "/InsertRoles.sql",
        "/InsertProfiles.sql",
        "/InsertAccounts.sql",
        "/InsertAccountRoles.sql",
        "/InsertXp.sql",
        "/InsertXpAccountHistory.sql"
      ],
      executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
    ),
    Sql(scripts = ["/EmptyDB.sql"], executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
  )

  @Nested
  inner class AuthCRUD {

    @Test
    fun `register with account`() {
      val registerForm = RegisterForm(
        password = "test",
        email = "test"
      )
      val response = testRestTemplate.exchange(
        "/api/register",
        HttpMethod.POST,
        HttpEntity<Any>(registerForm),
        AccountView::class.java
      )
      val account = response.body as AccountView
      assert(response.statusCode.is2xxSuccessful)
      assert(account.email == "test")
      assert(account.jwt != null)
      assert(account.qrCode != null)
    }


    @Test
    fun `logout`() {
      val headers = generateAdminJwtHeaders()
      val response = testRestTemplate.exchange(
        "/api/logout",
        HttpMethod.GET,
        HttpEntity<Any>(headers),
        String::class.java
      )
      assert(response.statusCode.is2xxSuccessful)
    }
  }
}
