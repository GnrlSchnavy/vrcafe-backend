package nl.vrcafe.backend.controller

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import nl.vrcafe.backend.Utils
import nl.vrcafe.backend.model.DataContainer
import nl.vrcafe.backend.model.form.CreateProductForm
import nl.vrcafe.backend.model.jpa.Product
import nl.vrcafe.backend.model.view.ProductView
import nl.vrcafe.backend.model.view.XpAccountHistoryView
import nl.vrcafe.backend.model.view.XpMapView
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.jdbc.SqlGroup

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ProductControllerTests : Utils() {

  private val mapper = jacksonObjectMapper()

  @SqlGroup(
    Sql(
      scripts = [
        "/InsertRoles.sql",
        "/InsertLocations.sql",
        "/InsertProfiles.sql",
        "/InsertAccounts.sql",
        "/InsertAccountRoles.sql",
        "/InsertXp.sql",
        "/InsertXpAccountHistory.sql",
        "/InsertProducts.sql"
      ],
      executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
    ),
    Sql(scripts = ["/EmptyDB.sql"], executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
  )

  @Nested
  inner class ProductCRUD {

    @Test
    fun `create product`() {
      val headers = generateAdminJwtHeaders()
      val createProductForm = CreateProductForm(
        -1,
        "Test Product",
        1000,
        XpMapView(
          -1,
          null,
          1,
          1
        ),
        "FOOD",

      )
      val request: HttpEntity<CreateProductForm> = HttpEntity<CreateProductForm>(createProductForm, headers)
      val response = testRestTemplate.exchange("/api/product", HttpMethod.POST, request, ProductView::class.java)
      val product = response.body as ProductView
      assert(
        response.statusCodeValue == 200
            && product.name == "Test Product"
            && product.price == 1000L
      )
    }

    @Test
    fun `get product`() {
      val headers = generateAdminJwtHeaders()
      val request: HttpEntity<Any> = HttpEntity(headers)
      val response = testRestTemplate.exchange(
        "/api/product/3",
        HttpMethod.GET,
        request,
        Product::class.java)
      val product = response.body as Product
      assert(
        response.statusCodeValue == 200
            && product.name == "Cola"
            && product.price == 11000L
      )
    }

    @Test
    fun `get all products`() {
      val headers = generateAdminJwtHeaders()
      val request: HttpEntity<Any> = HttpEntity(headers)
      val response = testRestTemplate.exchange("/api/product", HttpMethod.GET, request, String::class.java)
      val dataContainer = mapper.readValue(response.body, DataContainer::class.java)
      val productList= mapper.convertValue(dataContainer.data, object : TypeReference<List<ProductView>>() {})
      assert(response.statusCodeValue == 200)
      assert(productList.size == 4)
    }

    @Test
    fun `get all products matching Cola `(){
      val headers = generateAdminJwtHeaders()
      val request: HttpEntity<Any> = HttpEntity(headers)
      val response = testRestTemplate.exchange("/api/product?query=name=Cola", HttpMethod.GET, request, String::class.java)
      val dataContainer = mapper.readValue(response.body, DataContainer::class.java)
      val productList= mapper.convertValue(dataContainer.data, object : TypeReference<List<ProductView>>() {})
      assert(response.statusCodeValue == 200)
      assert(productList.size == 1)
    }

    @Test
    fun `get all products matching DRINKS productType `(){
      val headers = generateAdminJwtHeaders()
      val request: HttpEntity<Any> = HttpEntity(headers)
      val response = testRestTemplate.exchange("/api/product?query=type=DRINKS", HttpMethod.GET, request, String::class.java)
      val dataContainer = mapper.readValue(response.body, DataContainer::class.java)
      val productList= mapper.convertValue(dataContainer.data, object : TypeReference<List<ProductView>>() {})
      assert(response.statusCodeValue == 200)
      assert(productList.size == 2)
    }

    @Test
    fun `get all products matching DRINKS productType and name Cola`(){
      val headers = generateAdminJwtHeaders()
      val request: HttpEntity<Any> = HttpEntity(headers)
      val response = testRestTemplate.exchange("/api/product?query=type=DRINKS+name=Cola", HttpMethod.GET, request, String::class.java)
      val dataContainer = mapper.readValue(response.body, DataContainer::class.java)
      val productList= mapper.convertValue(dataContainer.data, object : TypeReference<List<ProductView>>() {})
      assert(response.statusCodeValue == 200)
      assert(productList.size == 1)
    }

    @Test
    fun `update product`() {
      val headers = generateAdminJwtHeaders()
      val createProductForm = CreateProductForm(
        1,
        "Updated Test Product",
        2222L,
        XpMapView(
          1,
          null,
          234,
          123
        )
      )
      val request: HttpEntity<CreateProductForm> = HttpEntity<CreateProductForm>(createProductForm, headers)
      val response = testRestTemplate.exchange("/api/product/1", HttpMethod.PUT, request, String::class.java)
      assert(response.statusCodeValue == 200)
      assert(response.body!!.contains("Updated Test Product"))
      assert(response.body!!.contains("2222"))
      assert(response.body!!.contains("123"))
      assert(response.body!!.contains("234"))
    }

    @Test
    fun `delete product`() {
      val deleteResponse = testRestTemplate.exchange(
        "/api/product/1",
        HttpMethod.DELETE,
        HttpEntity<Any>(generateAdminJwtHeaders()),
        Product::class.java
      )
      assert(deleteResponse.statusCodeValue == 200)
      val getResponse = testRestTemplate.exchange(
        "/api/product/1",
        HttpMethod.GET,
        HttpEntity<Any>(generateAdminJwtHeaders()),
        String::class.java
      )
      assert(getResponse.statusCodeValue == 404)
    }


  }

}
