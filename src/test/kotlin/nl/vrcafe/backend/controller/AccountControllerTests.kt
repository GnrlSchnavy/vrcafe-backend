package nl.vrcafe.backend.controller

import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import nl.vrcafe.backend.Utils
import nl.vrcafe.backend.model.form.RoleToAccountForm
import nl.vrcafe.backend.model.form.UpdatePasswordForm
import nl.vrcafe.backend.model.jpa.account.Account
import nl.vrcafe.backend.model.view.TinyAccountView
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.jdbc.SqlGroup
import java.util.UUID

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AccountControllerTests : Utils() {

  @Autowired
  override lateinit var testRestTemplate: TestRestTemplate

//TODO  Mock aws email service

  private val mapper = jacksonObjectMapper().registerModule(JavaTimeModule())
    .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)

  @SqlGroup(
    Sql(
      scripts = [
        "/InsertRoles.sql",
        "/InsertProfiles.sql",
        "/InsertAccounts.sql",
        "/InsertAccountRoles.sql",
        "/InsertXp.sql",
        "/InsertXpAccountHistory.sql"
      ],
      executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
    ),
    Sql(scripts = ["/EmptyDB.sql"], executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
  )

  @Nested
  inner class AccountCRUD {

    @Test
    fun `add role to account`() {
      val roleToAccountForm = RoleToAccountForm("user", "ROLE_ADMIN")
      val response = testRestTemplate.exchange(
        "/api/account/add-role",
        HttpMethod.POST,
        HttpEntity<Any>(roleToAccountForm, generateAdminJwtHeaders()),
        String::class.java
      )
      assert(response.statusCode.is2xxSuccessful)
      assert(response.body!!.toString().contains("\"email\":\"user\""))
    }

    @Test
    fun `get profileview`() {
      val response = testRestTemplate.exchange(
        "/api/account/profile/1",
        HttpMethod.GET,
        HttpEntity<Any>(generateAdminJwtHeaders()),
        TinyAccountView::class.java
      )
      val profile = response.body!! as TinyAccountView
      assert(response.statusCode.is2xxSuccessful)
      assert(profile.email == "admin")
      assert(profile.xp == 0L)
      assert(profile.level == 1L)

    }

    @Test
    fun `get all accounts` () {
      val getAllAccountsResponse = testRestTemplate.exchange(
        "/api/account?page=0&size=10&offset=0",
        HttpMethod.GET,
        HttpEntity<Any>(generateAdminJwtHeaders()),
        String::class.java
      )
      assert(getAllAccountsResponse.statusCode.is2xxSuccessful)
      assert(getAllAccountsResponse.body!!.toString().contains("\"email\":\"user\""))
      assert(getAllAccountsResponse.body!!.toString().contains("\"email\":\"session_user_test\""))
    }

    @Test
    fun `get all accounts with query` () {
      val getAllAccountsResponse = testRestTemplate.exchange(
        "/api/account?page=0&size=2&offset=0&query=email=user",
        HttpMethod.GET,
        HttpEntity<Any>(generateAdminJwtHeaders()),
        String::class.java
      )
      assert(getAllAccountsResponse.statusCode.is2xxSuccessful)
      assert(getAllAccountsResponse.body!!.toString().contains("\"email\":\"user\""))
    }

    @Test
    fun `get account by query` () {
      val getAllAccountsResponse = testRestTemplate.exchange(
        "/api/account?page=0&size=2&offset=0&query=email=user",
        HttpMethod.GET,
        HttpEntity<Any>(generateAdminJwtHeaders()),
        String::class.java
      )
      assert(getAllAccountsResponse.statusCode.is2xxSuccessful)
      assert(getAllAccountsResponse.body!!.toString().contains("\"email\":\"user\""))
    }

    @Test
    fun `request password reset`() {
      val forgotPasswordResponse = testRestTemplate.exchange(
        "/api/account/forgot-password/user",
        HttpMethod.GET,
        HttpEntity<Any>(generateEmptyHeaders()),
        String::class.java
      )
      assert(forgotPasswordResponse.statusCode.is2xxSuccessful)
      val getProfileResponse = testRestTemplate.exchange(
        "/api/account/2",
        HttpMethod.GET,
        HttpEntity<Any>(generateAdminJwtHeaders()),
        Account::class.java
      )
      assert(getProfileResponse.statusCode.is2xxSuccessful)
      assert(getProfileResponse.body!!.recoverPasswordTime != null)
      assert(getProfileResponse.body!!.forgotPasswordToken != null)

      val forgotPasswordToken = getProfileResponse.body!!.forgotPasswordToken
      val updatePasswordForm = UpdatePasswordForm("newPassword", forgotPasswordToken!!)
      val updatePasswordResponse = testRestTemplate.exchange(
        "/api/account/update-password",
        HttpMethod.POST,
        HttpEntity<Any>(updatePasswordForm, generateEmptyHeaders()),
        String::class.java
      )
      assert(updatePasswordResponse.statusCode.is2xxSuccessful)
      val getProfileResponse2 = testRestTemplate.exchange(
        "/api/account/2",
        HttpMethod.GET,
        HttpEntity<Any>(generateAdminJwtHeaders()),
        Account::class.java
      )
      assert(getProfileResponse2.statusCode.is2xxSuccessful)
      assert(getProfileResponse2.body!!.recoverPasswordTime == null)
      assert(getProfileResponse2.body!!.forgotPasswordToken == null)
    }

    @Test
    fun `try to update password with incorrect uuid`(){
      val updatePasswordForm = UpdatePasswordForm( "newPassword", UUID.randomUUID())
      val updatePasswordResponse = testRestTemplate.exchange(
        "/api/account/update-password",
        HttpMethod.POST,
        HttpEntity<Any>(updatePasswordForm, generateEmptyHeaders()),
        String::class.java
      )
      assert(updatePasswordResponse.statusCode.is4xxClientError)
    }


    @Test
    fun `try to update password with incorrect email`(){
      val forgotPasswordResponse = testRestTemplate.exchange(
        "/api/account/forgot-password/user",
        HttpMethod.GET,
        HttpEntity<Any>(generateEmptyHeaders()),
        String::class.java
      )
      assert(forgotPasswordResponse.statusCode.is2xxSuccessful)
      val getProfileResponse = testRestTemplate.exchange(
        "/api/account/2",
        HttpMethod.GET,
        HttpEntity<Any>(generateAdminJwtHeaders()),
        Account::class.java
      )
      assert(getProfileResponse.statusCode.is2xxSuccessful)
      assert(getProfileResponse.body!!.recoverPasswordTime != null)
      assert(getProfileResponse.body!!.forgotPasswordToken != null)

      val forgotPasswordToken = getProfileResponse.body!!.forgotPasswordToken
      val updatePasswordForm = UpdatePasswordForm("newPassword", UUID.randomUUID())
      val updatePasswordResponse = testRestTemplate.exchange(
        "/api/account/update-password",
        HttpMethod.POST,
        HttpEntity<Any>(updatePasswordForm, generateEmptyHeaders()),
        String::class.java
      )
      assert(updatePasswordResponse.statusCode.is4xxClientError)
    }
  }
}

