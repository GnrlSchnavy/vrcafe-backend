DELETE
FROM account_role
where 1 = 1;
DELETE
FROM role
where 1 = 1;
DELETE
FROM account
where 1 = 1;
DELETE
FROM location
where 1 = 1;
DELETE
FROM session
where 1 = 1;
DELETE
FROM xp_account_history
where 1 = 1;
DELETE
FROM xp_level_map
where 1 = 1;
DELETE
FROM xp_map_history
where 1 = 1;
DELETE
FROM xp_map
where 1 = 1;
DELETE
FROM xp_mod
where 1 = 1;
DELETE
FROM xp_event
where 1 = 1;
DELETE
FROM xp_event
where 1 = 1;

insert into profile(id, created)
values (1, now()),
       (2, now()),
       (3, now()),
       (4, now()),
       (5, now()),
       (6, now()),
       (7, now()),
       (8, now());
ALTER SEQUENCE profile_sequence RESTART WITH 100;


insert into role(id, name)
values (1, 'ROLE_ADMIN'),
       (2, 'ROLE_USER');
ALTER SEQUENCE role_sequence RESTART WITH 100;

INSERT INTO public.account (id, rank, email, password, qr_code, encoded_qr_code, xp, level, forgot_password_token,
                            recover_password_time, created, missed_checkouts, profile_id)
VALUES (1, 'NOOB', 'admin', '{bcrypt}$2a$10$T7Icp.7j5rOaIL8AjKX/JuQAGEy43OK4Y.IbKsrmK1KP0D6DeKlAe',
        'b4f709df-f807-4f58-aa33-1cdfee15e458',
        'iVBORw0KGgoAAAANSUhEUgAAAMgAAADIAQAAAACFI5MzAAABf0lEQVR4Xu2WQW6DQAxFf9TFLHMEbgIXQyoSF0tuMkdgOQsU938jhaRptz+thCUQ5s3Csr/tQfxm+P7jbgc5iOxvkArgI1qPsYtrTPTObsJnrm1Y0KE/zxE3O2lIf4ttrsTvIPL31ztIlJB7eo3NQUL1KZfzCpbmtXIGIo3WpsTw9aJeA9mMiZl2z0sYGyMq11iBYRk7fplJsDcok57jIi5MUbnXx0cuQJeTgvVhqCf9sxLkoOSkkDdXBugmJZYPdSeLFKwPhjxuJJVDYs0j0dQve2wuwgZhbBxY1KiaNc9YCYPBp4Q6aVZFStZNbtLoKVa26HN9XITZGaE+rU0bQ61iJjkkOKYgj5Lds2MjAxOTfcpxMaLs6rWRnnuK4mCfcmqh3WeVi6TVfLNVQhvcTCo0H/LmULQxtqNWwmcO5mTaNsaDQlwkb5DqU5Sb6jO8h0CtkuJ41KiTaG8jrsuzek0k8g6bezsvss+Vc5DUqPZ2x72tW4yb/GwHOYjsf5IvcICFvR7Z2ggAAAAASUVORK5CYII=',
        0, 1, null, null, '2022-07-21 21:48:11.469446', 0, 1);
INSERT INTO public.account (id, rank, email, password, qr_code, encoded_qr_code, xp, level, forgot_password_token,
                            recover_password_time, created, missed_checkouts, profile_id)
VALUES (3, 'NOOB', 'session_user_test', '{bcrypt}$2a$10$IPQt4HDtuvaDYZXr0UYXPek95B8PbnTbY9sF3a1UucfvEh0wb/mam', 'asdf',
        'iVBORw0KGgoAAAANSUhEUgAAAMgAAADIAQAAAACFI5MzAAABcklEQVR4Xu2WTY6DMAyFXbHIkiNwE3qxkYLExeAmOUKWWaB63nOq4aedZc2MhKXShq+LJz/bsehvIccXP3GRizD+Bkki0iTp21H10Q44td4EnzHJnb+mir1JkXZM5Za/OuCRKk8hYdahg6zTCL6zdDTpDKL0Rydp9OiPF7EaLZSFx2v1fp7UCLBGWajP8CQpPJiYOTdJbrp0G9VeBK4o62Jhje7y5kfgDx7S4TRq8SflnmOdFEGzFao3MVmll/hSIY4kKmSJmD/7vHkQRWKaFFQXCRwS/iTZufZpz1m1+uNE1CYU+5SYLxieBFd2jsSLoExk65wTgSF2rn06bGaVG7EaRZnE59xc/XEiCWK4NLBVJii0P7sSCw4spKhvbZFwJtCGnGBM4caYuLv4E+XuUpcGatvOaydSuEFyYKkwdNYzCLe2RWq19jttboTlyVsz6sYfL6LmD/tUTOXRuc8TqTus9Sk6Nqz+OJH3cZGLMP4n+QZ5y19S0TA81gAAAABJRU5ErkJggg==',
        0, 1, null, null, '2022-07-21 21:48:11.469446', 0, 2);
INSERT INTO public.account (id, rank, email, password, qr_code, encoded_qr_code, xp, level, forgot_password_token,
                            recover_password_time, created, missed_checkouts, profile_id)
VALUES (2, 'NOOB', 'user', '{bcrypt}$2a$10$IPQt4HDtuvaDYZXr0UYXPek95B8PbnTbY9sF3a1UucfvEh0wb/mam',
        '00026777-deed-4f04-8005-21bc1cacfe65',
        'iVBORw0KGgoAAAANSUhEUgAAAMgAAADIAQAAAACFI5MzAAABcklEQVR4Xu2WTY6DMAyFXbHIkiNwE3qxkYLExeAmOUKWWaB63nOq4aedZc2MhKXShq+LJz/bsehvIccXP3GRizD+Bkki0iTp21H10Q44td4EnzHJnb+mir1JkXZM5Za/OuCRKk8hYdahg6zTCL6zdDTpDKL0Rydp9OiPF7EaLZSFx2v1fp7UCLBGWajP8CQpPJiYOTdJbrp0G9VeBK4o62Jhje7y5kfgDx7S4TRq8SflnmOdFEGzFao3MVmll/hSIY4kKmSJmD/7vHkQRWKaFFQXCRwS/iTZufZpz1m1+uNE1CYU+5SYLxieBFd2jsSLoExk65wTgSF2rn06bGaVG7EaRZnE59xc/XEiCWK4NLBVJii0P7sSCw4spKhvbZFwJtCGnGBM4caYuLv4E+XuUpcGatvOaydSuEFyYKkwdNYzCLe2RWq19jttboTlyVsz6sYfL6LmD/tUTOXRuc8TqTus9Sk6Nqz+OJH3cZGLMP4n+QZ5y19S0TA81gAAAABJRU5ErkJggg==',
        0, 1, null, null, '2022-07-21 21:48:11.469446', 0, 3);
INSERT INTO public.account (id, rank, email, password, qr_code, encoded_qr_code, xp, level, forgot_password_token,
                            recover_password_time, created, missed_checkouts, profile_id)
VALUES (4, 'NOOB', 'dirk@vrcafehaarlem.nl', '{bcrypt}$2a$10$BTzYZ59Mul8AJPbLsQKznOGC0Y6VHmUBc0VAHL4fsmZmClPejfxFi',
        'f10cfb8f-9519-440b-8d6f-60d131c7c956',
        'iVBORw0KGgoAAAANSUhEUgAAAMgAAADIAQAAAACFI5MzAAABb0lEQVR4Xu2WS46EMAxEjXrBkiNwE7hYS90SF6NvkiOwzCIaT5XTQmE+22JGwhIQ57Gw/Df/TezrxS4XuQjlb5BkZjfPk93HbMMT2qAmeJaU583GHif3DzmBRdTtEadk0ymEOhwznUccjoHa+SnEGZ9+HQq175ETEOZoynRMDhcds1dAqsAxT6tmVV1IYBss6l9ezObtPuIkJpQyolPc3Fe4CC8xeYcGncJ6x8tm/qskTp8gNOyWuEyNd0SkbRJG3KkJhsWema/tlvq9TlWETWJxJGqxqNPGahHxSAnoC60sFn1TSlgWYVFBiW6HyKkIlwZkCFsmJ0Y7t0XEV1xGiUIrbeR0ZChj3Ry67cGGpSbMTJAoVlTqsYspSEiqH4SGi4SYJIu5HXXKPar+IyXOjYXpGaFp+7WKhEWZdRrx4dg4hbBO3bk5NDmqJP4e2WGlmnjssJzbrFjzY+QUpOZoDG/ub018RORnuchFKP+TfALmdIqGapa7HAAAAABJRU5ErkJggg==',
        0, 1, null, null, '2022-07-22 16:09:02.977463', 0, 4);
INSERT INTO public.account (id, rank, email, password, qr_code, encoded_qr_code, xp, level, forgot_password_token,
                            recover_password_time, created, missed_checkouts, profile_id)
VALUES (5, 'NOOB', 'yvanstemmerik@gmail.com', '{bcrypt}$2a$10$K7pCquq8Vy14q7Zifgohy.qK04QD8ukt.jeXvD9TMBsALHOaWck.u',
        'ca5bf89c-bd81-493c-997b-5a8646942036',
        'iVBORw0KGgoAAAANSUhEUgAAAMgAAADIAQAAAACFI5MzAAABf0lEQVR4Xu2XTW6DQAyFjVjMkiNwk3CxSETKxchN5ggsZ4Hivuep+GnaZV9aCUtEY74sLP88D+Y/mX19sdpJTkL7GySbWevlYte+WHeD16kJnnsuw2y9Xbq7+1NOEBF9G+OUgd9B6CMxl/cRT0638bcQZ33S1C2G0rxWTkDYo7kwMSVSdOxeAanGxKwO/e306wSxIaL08AVhYVhxUpNiNlIpWvcHKuXTrnc0BCNqfShFHdZhjU1F2J58ee35qs6LlNTmCKVIE0V7kBOfUJ+IqDRzm9M6pypCrbr1+Fn6OqwN/6QmTj9kaqnhSolXvY45beaR5RKTnHy+skdb4psxVDEJkYg5LbGuwqTEmRjengw9Oub9NlORuD3VmwMle6uPiIR9auST3XrcmgKSjXsbi2uBVmFOdzqqInjukCkPuUDLbltTRTinbBNEyfrsdpaUWOxtKvdhowtJ3dsOpdh1r4p43GHrlwSMeRITY49iVyKswZfd3laR7+0kJ6H9T/IBM4eHiT766AIAAAAASUVORK5CYII=',
        0, 1, null, null, '2022-07-22 09:29:11.510096', 0, 5);
INSERT INTO public.account (id, rank, email, password, qr_code, encoded_qr_code, xp, level, forgot_password_token,
                            recover_password_time, created, missed_checkouts, profile_id)
VALUES (6, 'NOOB', 'lisanijssen@hotmail.com', '{bcrypt}$2a$10$FNEq9b/06QXTp23aSL/F5entpSe4AjNbEq./EY.YCNnOwnpxq7OEK',
        'a7be8966-8b0b-418b-9f1f-9140029e5b82',
        'iVBORw0KGgoAAAANSUhEUgAAAMgAAADIAQAAAACFI5MzAAABfklEQVR4Xu2XS46DMBBEG7HwkiNwE7hYpCBxMbiJj8DSC5ROlY0IM5NZTjGRaCmR7edFqX9uzH8z+36w20UuQvsfJJpZHVNntzZZM2DXqAl+o+Pg7qnHyh9yAkVjtN5X89mx6k4iHR3TldUpxKdmaPF3CnHGJ1XLPabqTeQEZMtRyErZRT+y969JsUSfWLXvlSRiD1mGOg0zXHSIj4hAFku0s9rDBN+ESU78YdYaO0VwX9viIiVhXnjpFImHId+WEiwHNomBAm9HbSKS4xMRFWBju9jrVEXomNGZnu5TuSMmfKlyaAajrHhULSKsDfokN+3VwsxDKcEeidpDW+qXLxkiInQHZPli+V58aVMRdgoMTtVSMzm2YtUSo2OCZ4JCyW+HltDYLkp6HqtERCL01OyRIw8RqVfkVMQ5u6BJ1Ju2vU5lhPObMy/wZEMnwnUGwfjAEkVyHGcKKUmc39iv/QTiTI4ywODV5OeEmjBHc4dChuDN0pP3dpGL0D6TPAEEVXEndCygCgAAAABJRU5ErkJggg==',
        0, 1, null, null, '2022-07-25 12:27:20.312431', 0, 6);
INSERT INTO public.account (id, rank, email, password, qr_code, encoded_qr_code, xp, level, forgot_password_token,
                            recover_password_time, created, missed_checkouts, profile_id)
VALUES (7, 'NOOB', 'opdeweeghsh@hotmail.com', '{bcrypt}$2a$10$7qIwlXT8dw6WUuu6D6mGjeb73PgpU2aKfP9fg7KU5JDbKOeyQdGge',
        '3c8d9fa4-830d-477b-b106-8e365bfa113c',
        'iVBORw0KGgoAAAANSUhEUgAAAMgAAADIAQAAAACFI5MzAAABfElEQVR4Xu2WQY6DMAxFjVhkyRFyE7gYEki9GNwkR2CZRVX3f4MgM8wsa2YkLLWK+7qwnO/viP4W8v2HPW5yE8bfIElEas2t9FFnHZE13gSfR8rdIhJwUn25kywNcxk0d/iPtJcQ5mPE12VEA09SnWvzIMr7CVPzFFzN+eYcCDWaMhuDr5N6HcgabMyeMD9OHyeoDRWFWZ8iHFacnMk2IC3sIswc1unojhMRmEQ0p8CcmmCcScrVMiQU2Mf1dPTNiVhtQqeAWy59LDTqRBJ7skbe5sWboKKahmVOMZbqdSOm0UrHSJtCetTmRHQS4crGnFYLZbLX5kVo1REF1gpszu1NoJARBwq1W1N3UqExgXOq8zJocT9+BF61vRxQX3E/TsTCepJMo8fe9iJJ6A9wKMhkwvL+ss18iNrLlfKEXWjp5F7EXpDc2+v9UKhXEO5tekbPFl1CYNpmWI9zdz5PlG9YNGZQtYesPzGNBltXirVReJUT+TluchPG/yRvAf6QqDAl8rkAAAAASUVORK5CYII=',
        0, 1, null, null, '2022-07-22 11:01:11.245152', 0, 7);
INSERT INTO public.account (id, rank, email, password, qr_code, encoded_qr_code, xp, level, forgot_password_token,
                            recover_password_time, created, missed_checkouts, profile_id)
VALUES (8, 'NOOB', 'navynoname@gmail.com', '{bcrypt}$2a$10$W7ipDNvYOA2m3eaBb3wCnu/FvD./PFHbxIiL8JnECPZG6YxQRh9A6',
        'a24ffd45-8e53-4cde-aa87-b577b7ee72ca',
        'iVBORw0KGgoAAAANSUhEUgAAAMgAAADIAQAAAACFI5MzAAABf0lEQVR4Xu2WS27CQBBEy2IxS47gm+CLIWGJi+Gb+AgsZ2HRqWoj2wnJMkUiuSWjmXksWt3VH8RPhq8Pi+1kJ7K/QUYAh6gnnNsYouft6Cb8rmPt7kDhKeJhJxVH3eWb/oPTW4jufYvn6R0kik5oXn1zkFBMyu04gal5zZyBSKNjPTEw/HlRr4HMpsAsF93X068T+kaPyhBTW+J+bnmyk453dorDrFZmyk3KDWjVKVqwYaF2i28uUhsdH7gEHzNOZqJHWsoz49S5yRjD/ZKdIuLGJJVNBZsI88OZhblT9JiD5ST0SI8DGxbJhFWjNlIGybOJCWgYp41vJsI6ZZOgTA6Rm8MmPy5S1auQmwMfKZjFNxthTEZtT8hinc1K+PVZp+pVtDU/JpKmJIU0qqJZ1eshIzS3c3Pgz1Wuuklocy2SJyv2k0JcJGdl5NzOzt29h8xzWxW7yY+VcG2BehUz9SU6BhK5w7JEn8XqJ5BGc1yxRPvt3DaR720nO5H9T/IBkExzayTctkYAAAAASUVORK5CYII=',
        0, 1, null, null, '2022-07-22 09:47:54.421486', 0, 8);
ALTER SEQUENCE account_sequence RESTART WITH 100;


insert into location(id, name, city, street, house_number, postal_code)
values (1, 'VrcafeHaarlem', 'Haarlem', 'Stationsplein', '68', '2025VL'),
       (2, 'VrcafeAmsterdam', 'Amsterdam', 'Marktplein', '101', '1054ZW');
ALTER SEQUENCE location_sequence RESTART WITH 100;



insert into account_role(id, account_id, role_id)
values (1, 1, 1),
       (2, 2, 2),
       (3, 3, 2),
       (4, 4, 1),
       (5, 5, 1),
       (6, 6, 1),
       (7, 7, 1),
       (8, 8, 1);
ALTER SEQUENCE hibernate_sequence RESTART WITH 100;

insert into xp_level_map(id, level, xp, rank_name)
values (1, 1, 0, 'Noob'),
       (2, 2, 30, 'Minder Noob'),
       (3, 3, 90, 'Opper Noob'),
       (4, 4, 200, 'Scrub'),
       (5, 5, 500, 'Epic'),
       (6, 6, 1000, 'Legendary'),
       (7, 7, 2000, 'SICK'),
       (8, 8, 4000, 'Hellslayer'),
       (9, 9, 8000, 'GOD'),
       (10, 10, 16000, 'DOG');
ALTER SEQUENCE xp_level_map_sequence RESTART WITH 100;

insert into xp_map(id, amount, reward, xp_mod_id)
values (1, 1, 1, null),
       (2, 1, 5, null),
       (3, 1, 10, null),
       (4, 1, 10, null),
       (5, 1, 10, null),
       (6, 1, 10, null),
       (7, 1, 10, null),
       (8, 1, 10, null),
       (9, 1, 10, null),
       (10, 1, 10, null),
       (11, 1, 10, null),
       (12, 1, 10, null),
       (13, 1, 10, null),
       (14, 1, 10, null),
       (15, 1, 10, null),
       (16, 1, 10, null);
ALTER SEQUENCE xp_map_sequence RESTART WITH 100;

insert into product(id, name, price, xp_map_id, product_type, created)
values (1, 'Speeltijd', 1, 1, 'PLAYTIME', now()),
       (2, 'Cola', 5, 2, 'DRINKS', now()),
       (3, 'Bitterballen', 10, 3, 'FOOD', now()),
       (4, 'Fanta', 3, 4, 'DRINKS', now()),
       (5, 'Bier 0.0', 3, 5, 'DRINKS', now()),
       (6, 'Chips', 3, 6, 'FOOD', now()),
       (7, 'Caprisun', 3, 7, 'DRINKS', now()),
       (8, 'Friet', 3, 8, 'FOOD', now()),
       (9, 'Cola zer', 3, 9, 'DRINKS', now()),
       (10, 'Fanta Zero', 3, 10, 'DRINKS', now()),
       (11, 'Sprite', 3, 11, 'DRINKS', now()),
       (12, 'Sprite Zero', 3, 12, 'DRINKS', now()),
       (13, 'Cappucino', 3, 13, 'DRINKS', now()),
       (14, 'Koffie', 3, 14, 'DRINKS', now()),
       (15, 'Ice Tea', 3, 15, 'DRINKS', now()),
       (16, 'Ginger Beer', 3, 16, 'DRINKS', now());
ALTER SEQUENCE product_sequence RESTART WITH 100;

INSERT INTO public.achievement (id, name, description)
VALUES (2, 'Coke drinker', 'How many Cokes can you drink');
INSERT INTO public.achievement (id, name, description)
VALUES (1, 'Addict', 'How many minutes have you played');
ALTER SEQUENCE achievement_sequence RESTART WITH 100;


INSERT INTO public.achievement_level_map (id, min_value, max_value, achievement_id, level, rank_name)
VALUES (1, 0, 90, 1, 1, 'Starting Player');
INSERT INTO public.achievement_level_map (id, min_value, max_value, achievement_id, level, rank_name)
VALUES (2, 90, 180, 1, 2, 'Beginning Player');
INSERT INTO public.achievement_level_map (id, min_value, max_value, achievement_id, level, rank_name)
VALUES (3, 180, 360, 1, 3, 'Medium Player');
INSERT INTO public.achievement_level_map (id, min_value, max_value, achievement_id, level, rank_name)
VALUES (4, 360, 720, 1, 4, 'Hard Player');
INSERT INTO public.achievement_level_map (id, min_value, max_value, achievement_id, level, rank_name)
VALUES (5, 720, 1500, 1, 5, 'Epic Player');
INSERT INTO public.achievement_level_map (id, min_value, max_value, achievement_id, level, rank_name)
VALUES (6, 0, 3, 2, 1, 'Under 18');
INSERT INTO public.achievement_level_map (id, min_value, max_value, achievement_id, level, rank_name)
VALUES (7, 3, 6, 2, 2, 'Your average drinker');
INSERT INTO public.achievement_level_map (id, min_value, max_value, achievement_id, level, rank_name)
VALUES (8, 6, 12, 2, 3, 'Seasoned drinker');
INSERT INTO public.achievement_level_map (id, min_value, max_value, achievement_id, level, rank_name)
VALUES (9, 12, 99999, 2, 4, 'Drinkin problem');
ALTER SEQUENCE achievement_level_map_sequence RESTART WITH 100;



