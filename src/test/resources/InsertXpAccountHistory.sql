INSERT INTO public.xp_account_history (id, account_id, product_type_id, amount, reward, actor_name, created) VALUES (19, 1, 1, 0, 0, 'SYSTEM', '2022-05-31 13:39:46.258000');
INSERT INTO public.xp_account_history (id, account_id, product_type_id, amount, reward, actor_name, created) VALUES (20, 1, 1, 0, 0, 'SYSTEM', '2022-05-31 13:40:09.086000');
INSERT INTO public.xp_account_history (id, account_id, product_type_id, amount, reward, actor_name, created) VALUES (21, 1, 1, 0, 0, 'SYSTEM', '2022-05-31 13:41:03.217000');
INSERT INTO public.xp_account_history (id, account_id, product_type_id, amount, reward, actor_name, created) VALUES (23, 1, 1, 0, 0, 'SYSTEM', '2022-05-31 13:43:10.800000');
INSERT INTO public.xp_account_history (id, account_id, product_type_id, amount, reward, actor_name, created) VALUES (24, 1, 1, 0, 0, 'SYSTEM', '2022-05-25 14:05:35.767000');
INSERT INTO public.xp_account_history (id, account_id, product_type_id, amount, reward, actor_name, created) VALUES (22, 1, 1, 0, 0, 'SYSTEM', '2022-05-30 03:42:50.755000');
INSERT INTO public.xp_account_history (id, account_id, product_type_id, amount, reward, actor_name, created) VALUES (25, 1, 1, 0, 0, 'SYSTEM', '2022-05-31 15:16:23.982000');
INSERT INTO public.xp_account_history (id, account_id, product_type_id, amount, reward, actor_name, created) VALUES (27, 1, 1, 1, 2, 'SYSTEM', '2022-05-25 15:31:53.000000');
INSERT INTO public.xp_account_history (id, account_id, product_type_id, amount, reward, actor_name, created) VALUES (28, 1, 1, 1, 2, 'SYSTEM', '2022-03-01 15:31:56.000000');
INSERT INTO public.xp_account_history (id, account_id, product_type_id, amount, reward, actor_name, created) VALUES (29, 1, 1, 1, 2, 'SYSTEM', '2022-01-31 15:31:59.000000');
INSERT INTO public.xp_account_history (id, account_id, product_type_id, amount, reward, actor_name, created) VALUES (31, 1, 1, 1, 3, 'SYSTEM', '2021-05-31 15:32:04.000000');
INSERT INTO public.xp_account_history (id, account_id, product_type_id, amount, reward, actor_name, created) VALUES (32, 1, 1, 1, 3, 'SYSTEM', '2022-05-31 05:32:15.000000');
INSERT INTO public.xp_account_history (id, account_id, product_type_id, amount, reward, actor_name, created) VALUES (33, 1, 1, 1, 5, 'SYSTEM', '2022-01-27 11:32:19.000000');
INSERT INTO public.xp_account_history (id, account_id, product_type_id, amount, reward, actor_name, created) VALUES (34, 1, 1, 1, 5, 'SYSTEM', '2022-05-19 15:32:25.000000');
INSERT INTO public.xp_account_history (id, account_id, product_type_id, amount, reward, actor_name, created) VALUES (35, 1, 1, 1, 5, 'SYSTEM', '2022-05-23 15:32:30.000000');
INSERT INTO public.xp_account_history (id, account_id, product_type_id, amount, reward, actor_name, created) VALUES (36, 1, 1, 1, 5, 'SYSTEM', '2022-05-09 15:32:33.000000');
INSERT INTO public.xp_account_history (id, account_id, product_type_id, amount, reward, actor_name, created) VALUES (37, 1, 1, 1, 5, 'SYSTEM', '2022-05-31 07:32:36.000000');
INSERT INTO public.xp_account_history (id, account_id, product_type_id, amount, reward, actor_name, created) VALUES (38, 1, 1, 1, 5, 'SYSTEM', '2022-05-31 08:32:40.000000');
INSERT INTO public.xp_account_history (id, account_id, product_type_id, amount, reward, actor_name, created) VALUES (39, 1, 1, 1, 5, 'SYSTEM', '2022-05-31 07:32:43.000000');

ALTER SEQUENCE xp_account_history_sequence RESTART WITH 100;
