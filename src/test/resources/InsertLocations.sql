-- noinspection SqlDialectInspectionForFile

insert into location(id, name, city, street, house_number, postal_code)
values  (1, 'VrcafeHaarlem','Haarlem','Stationsplein','68','2025VL'),
        (2, 'VrcafeAmsterdam','Amsterdam','Marktplein', '101', '1054ZW');

ALTER SEQUENCE location_sequence RESTART WITH 100;
