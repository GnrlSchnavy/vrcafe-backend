-- noinspection SqlDialectInspectionForFile

insert into role(id, name)
values  (1, 'ROLE_ADMIN'),
        (2, 'ROLE_USER');

ALTER SEQUENCE role_sequence RESTART WITH 100;
