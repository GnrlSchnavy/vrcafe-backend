-- noinspection SqlDialectInspectionForFile

DELETE FROM account_role;
DELETE FROM role;
DELETE FROM friend_list;
DELETE FROM session;
DELETE FROM account;
DELETE FROM location;
DELETE FROM session;
DELETE FROM xp_level_map;
DELETE FROM xp_map_history;
DELETE FROM xp_map;
DELETE FROM xp_mod;
DELETE FROM xp_event;
DELETE FROM xp_account_history;
DELETE FROM product;
DELETE FROM profile;


