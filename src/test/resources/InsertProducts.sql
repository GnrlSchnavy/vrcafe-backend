insert into product(id, name, price, product_type, created, xp_map_id)
values  (1, 'Playtime', 1000, 'PLAYTIME', now(),1),
 (2, 'Bitterballen', 5000, 'FOOD',now(),2),
 (3, 'Cola', 11000, 'DRINKS',now(),3),
 (4, 'Fanta', 11000, 'DRINKS',now(),4);

ALTER SEQUENCE product_sequence RESTART WITH 100;

