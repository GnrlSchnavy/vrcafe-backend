insert into xp_map(id, amount, reward)
values (1,1,1), (2,1,5), (3,1,10), (4,1,10);
ALTER SEQUENCE xp_map_sequence RESTART WITH 100;

insert into xp_level_map(id, level, xp, rank_name)
values (1,1,0,'Noob'), (2,2,30,'Minder Noob'),(3,3,90, 'Opper Noob'),(4,4,200,'Scrub'),(5,5,500,'Epic'),(6,6,1000,'Legendary'),(7,7,2000, 'SICK'),(8,8,4000,'Hellslayer'),(9,9,8000,'GOD'),(10,10,16000,'DOG');
ALTER SEQUENCE xp_level_map_sequence RESTART WITH 100;

insert into xp_event(id, event_name, description, event_start, event_end, activated,created, created_by)
values (97,'start end after activated','start date before end date after',now()::DATE - 1,now()::DATE - 1,true,now(),'USER'),
 (98,'start end after activated','start date before end date after',now()::DATE - 1,now()::DATE - 1,true,now(),'USER'),
 (99,'start end after activated','start date before end date after',now()::DATE - 1,now()::DATE - 1,true,now(),'USER');
ALTER sequence xp_event_sequence RESTART WITH 100;

insert into xp_mod(id, xp_event_id, xp_map_id, old_amount, old_reward, temp_amount, temp_reward)
values(1, 99, 1, 1, 1, 2, 2);
alter sequence xp_mod_sequence restart with 100;

-- insert into xp_event(id, event_name, description, event_start, event_end, activated)
-- values (1,"start end before activated","start date and end date are before today",now() - interval '2 day',now() - interval '1 day',true),
--        (2,"start end before deactivated","start date and end date are before today",now() - interval '2 day',now() - interval '1 day',false),
--        (3,"start before end after activated","start date before end date after",now() - interval '2 day',now() + interval '1 day',true),
--        (4,"start before end after deactivated","start date before end date after",now() - interval '2 day',now() + interval '1 day',false),
--        (5,"start end after activated","start date before end date after",now() + interval '1 day',now() + interval '2 day',false),
--        (6,"start end after deactivated","start date before end date after",now() + interval '1 day',now() + interval '2 day',false);
-- ALTER SEQUENCE xp_event_sequence RESTART WITH 100;
-- --
-- insert into xp_mod(id, xp_event_id, xp_map_id, old_amount, old_reward, temp_amount, temp_reqard)
-- values(1, 1, 1, 1, 1, 2, 2);
--
-- alter sequence xp_mod_sequence restart with 100;
