package nl.vrcafe.backend.controller

import nl.vrcafe.backend.model.DataContainer
import nl.vrcafe.backend.model.form.SendMessageForm
import nl.vrcafe.backend.model.jpa.ChatMessage
import nl.vrcafe.backend.model.view.ChatMessageView
import nl.vrcafe.backend.service.ChatService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/chat")
class ChatController {

    @Autowired
    lateinit var chatService: ChatService

    @PostMapping("/sendMessage")
    fun sendMessage(@RequestBody sendMessageRequest: SendMessageForm) {
        chatService.sendChatMessage(
            sendMessageRequest.message,
            sendMessageRequest.receiverId
        )
    }
    @Secured("ROLE_ADMIN", "ROLE_USER")
    @GetMapping("/{recipientId}")
    fun getMessages(
        @RequestParam(name = "offset", required = false, defaultValue = "0") offset: Int,
        @RequestParam(name = "size", required = false, defaultValue = "10") size: Int,
        @PathVariable("recipientId") recipientId: Long,
    ): DataContainer<List<ChatMessageView>> {
        //check if recipient is friend!
        return chatService.getChatMessages(recipientId, offset, size)
    }


}
