package nl.vrcafe.backend.controller


import nl.vrcafe.backend.model.DataContainer
import nl.vrcafe.backend.model.form.RoleToAccountForm
import nl.vrcafe.backend.model.form.UpdatePasswordForm
import nl.vrcafe.backend.model.jpa.account.Account
import nl.vrcafe.backend.model.view.AccountView
import nl.vrcafe.backend.model.view.TinyAccountView
import nl.vrcafe.backend.service.AccountService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/account")
class AccountController {

  @Autowired
  lateinit var accountService: AccountService

  @Secured("ROLE_ADMIN")
  @PostMapping("/add-role")
  fun addRoleToAccount(@RequestBody roleToAccountForm: RoleToAccountForm): ResponseEntity<Account> {
    return ResponseEntity.ok().body(accountService.addRoleToAccount(roleToAccountForm))
  }

  @Secured("ROLE_ADMIN", "ROLE_USER")
  @GetMapping("/profile/{id}")
  fun getProfileView(@PathVariable("id", required = false) id: Long?): ResponseEntity<TinyAccountView> {
    return ResponseEntity.ok().body(accountService.getProfileView(id))
  }

  @Secured("ROLE_ADMIN", "ROLE_USER")
  @GetMapping("/profile")
  fun getProfileView(): ResponseEntity<TinyAccountView> {
    return ResponseEntity.ok().body(accountService.getProfileView())
  }


  @Secured("ROLE_ADMIN", "ROLE_USER")
  @GetMapping("/{id}")
  fun getAccount(@PathVariable("id") id: Long?): ResponseEntity<Account> {
    return ResponseEntity.ok().body(accountService.getAccount(id))
  }

  @Secured("ROLE_ADMIN")
  @GetMapping()
  fun getAllAccounts(
    @RequestParam(name = "offset", required = false, defaultValue = "0") offset: Int,
    @RequestParam(name = "size", required = false, defaultValue = "10") size: Int,
    @RequestParam(name = "query", required = false, defaultValue = "") query: String
  ): ResponseEntity<DataContainer<List<AccountView>>> {
    return ResponseEntity.ok().body(accountService.getAllAccounts(offset, size, query))
  }

  @PostMapping("/update-password")
  fun updatePassword(@RequestBody updatePasswordForm: UpdatePasswordForm): ResponseEntity<AccountView> {
    return ResponseEntity.ok().body(accountService.updatePassword(updatePasswordForm))
  }

  @GetMapping("forgot-password/{email}")
  fun forgotPassword(@PathVariable("email") email: String): Any {
    return accountService.forgotPassword(email)
  }
}

