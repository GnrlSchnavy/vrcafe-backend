package nl.vrcafe.backend.controller

import nl.vrcafe.backend.model.DataContainer
import nl.vrcafe.backend.model.view.TinyAccountView
import nl.vrcafe.backend.service.FriendService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/friend")
class FriendController {

    @Autowired
    lateinit var friendService: FriendService

    @GetMapping()
    fun getFriends(
        @RequestParam(name = "offset", required = false, defaultValue = "0") offset: Int,
        @RequestParam(name = "size", required = false, defaultValue = "10") size: Int,
    ): ResponseEntity<DataContainer<List<TinyAccountView>>> {
        return ResponseEntity.ok().body(friendService.getFriends(size, offset))
    }

    @GetMapping("/add/{id}")
    fun sendFriendRequest(@PathVariable id: String): ResponseEntity<TinyAccountView> {
        return ResponseEntity.ok().body(friendService.sendFriendRequest(id.toLong()))
    }

    @GetMapping("/add-by-name/{username}")
    fun sendFriendrequestByUsername(@PathVariable username: String): ResponseEntity<Any>{
        return ResponseEntity.ok().body(friendService.sendFriendRequest(username))
    }

    @GetMapping("/accept/{id}")
    fun acceptFriendRequest(@PathVariable id: String): ResponseEntity<Any> {
        return ResponseEntity.ok().body(friendService.acceptFriendRequest(id.toLong()))
    }

    @GetMapping("/decline/{id}")
    fun declineFriendRequest(@PathVariable id: String): ResponseEntity<Any> {
        return ResponseEntity.ok().body(friendService.declineFriendRequest(id.toLong()))
    }

    @DeleteMapping("/remove/{id}")
    fun deleteFriend(@PathVariable id: String): ResponseEntity<Any> {
        return ResponseEntity.ok().body(friendService.deleteFriend(id.toLong()))
    }

    @GetMapping("/pending")
    fun getPendingRequests(
        @RequestParam(name = "offset", required = false, defaultValue = "0") offset: Int,
        @RequestParam(name = "size", required = false, defaultValue = "10") size: Int,
    ): ResponseEntity<DataContainer<List<TinyAccountView>>> {
        return ResponseEntity.ok().body(friendService.getPendingRequests(size, offset))
    }



}
