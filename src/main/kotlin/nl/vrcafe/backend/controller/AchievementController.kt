package nl.vrcafe.backend.controller

import nl.vrcafe.backend.model.DataContainer
import nl.vrcafe.backend.model.view.AchievementView
import nl.vrcafe.backend.service.AchievementService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/achievement")
class AchievementController {

  @Autowired
  lateinit var achievementService: AchievementService

  @GetMapping
  fun getAchievements(
    @RequestParam(name = "offset", required = false, defaultValue = "0") offset: Int,
    @RequestParam(name = "size", required = false, defaultValue = "10") size: Int
  ): ResponseEntity<DataContainer<List<AchievementView>>> {
    return ResponseEntity.ok().body(achievementService.getAllAchievementsProgress(offset, size))
  }

}
