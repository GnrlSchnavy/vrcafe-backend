package nl.vrcafe.backend.controller

import nl.vrcafe.backend.model.DataContainer
import nl.vrcafe.backend.model.view.XpAccountHistoryView
import nl.vrcafe.backend.service.XpAccountHistoryService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/xphistory")
class XpAccountHistoryController {

  @Autowired
  lateinit var xpAccountHistoryService: XpAccountHistoryService

  @Secured("ROLE_ADMIN", "ROLE_USER")
  @GetMapping()
  fun getXpAccountHistory(
    @RequestParam(name = "offset", required = false, defaultValue = "0") offset: Int,
    @RequestParam(name = "size", required = false, defaultValue = "10") size: Int
  ): ResponseEntity<DataContainer<List<XpAccountHistoryView>>> {
    return ResponseEntity.ok().body(xpAccountHistoryService.getXpAccountHistory(offset, size))
  }


}
