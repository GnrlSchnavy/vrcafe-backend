package nl.vrcafe.backend.controller

import nl.vrcafe.backend.model.form.LoginForm
import nl.vrcafe.backend.model.form.RegisterForm
import nl.vrcafe.backend.model.view.AccountView
import nl.vrcafe.backend.service.AccountService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/")
class AuthController {

    @Autowired
    lateinit var accountService: AccountService

    @PostMapping("/register")
    fun register(@RequestBody account: RegisterForm): ResponseEntity<AccountView?> {
        return ResponseEntity.ok().body(accountService.register(account))
    }

    @PostMapping("/login")
    fun login(@RequestBody account: LoginForm): ResponseEntity<Any?> {
        return ResponseEntity.ok().body(accountService.login(account))
    }

    @GetMapping("/logout")
    fun logout(): ResponseEntity<Any?> {
        return ResponseEntity.ok().body(accountService.logout())
    }

}
