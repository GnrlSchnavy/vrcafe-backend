package nl.vrcafe.backend.controller

import nl.vrcafe.backend.service.EmailService
import nl.vrcafe.backend.service.SessionService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/test/")
class TestController {

  @Autowired
  lateinit var emailService: EmailService

  @Autowired
  lateinit var sessionService: SessionService

  @Secured("ROLE_ADMIN")
  @GetMapping("/email")
  fun email(){
      sessionService.deleteOpenSessions();
  }


}
