package nl.vrcafe.backend.controller

import nl.vrcafe.backend.model.form.CreateRoleForm
import nl.vrcafe.backend.model.jpa.account.Role
import nl.vrcafe.backend.service.RoleService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/role")
class RoleController {

  @Autowired
  lateinit var roleService: RoleService

  @Secured("ROLE_ADMIN")
  @PostMapping("")
  fun save(@RequestBody role: CreateRoleForm): ResponseEntity<Role> {
    return ResponseEntity.ok().body(roleService.createRole(role))
  }

  @Secured("ROLE_ADMIN")
  @DeleteMapping("/{id}")
  fun deleteRole(@PathVariable id: Long): ResponseEntity<Any> {
    roleService.deleteRole(id)
    return ResponseEntity.accepted().build()
  }

  @Secured("ROLE_ADMIN")
  @GetMapping("/{id}")
  fun getRole(@PathVariable id: Long): ResponseEntity<Role> {
    return ResponseEntity.ok().body(roleService.getRole(id))
  }

  @Secured("ROLE_ADMIN")
  @PutMapping("/{id}")
  fun updateRole(@PathVariable id: Long, @RequestBody role: CreateRoleForm): ResponseEntity<Role> {
    return ResponseEntity.ok().body(roleService.updateRole(id, role))
  }

  @Secured("ROLE_ADMIN")
  @GetMapping("")
  fun getAllRoles(): ResponseEntity<List<Role>> {
    return ResponseEntity.ok().body(roleService.getAllRoles())
  }




}
