package nl.vrcafe.backend.controller

import nl.vrcafe.backend.model.form.CreateXpEventForm
import nl.vrcafe.backend.model.jpa.xp.XpEvent
import nl.vrcafe.backend.service.XpEventService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/xp-event")
class XpEventController {

  @Autowired
  lateinit var xpEventService: XpEventService

  @Secured("ROLE_ADMIN")
  @PostMapping("")
  fun createEvent(@RequestBody createXpEventForm: CreateXpEventForm): ResponseEntity<XpEvent> {
    return ResponseEntity.ok().body(xpEventService.createXpEvent(createXpEventForm))
  }

  @Secured("ROLE_ADMIN")
  @GetMapping("/update-events")
  fun updateEvents(): ResponseEntity<Map<String, String>> {
    return ResponseEntity.ok().body(xpEventService.checkForXpEventes())
  }

  @Secured("ROLE_ADMIN")
  @GetMapping("")
  fun getXpEvents():ResponseEntity<List<XpEvent>>{
    return ResponseEntity.ok().body(xpEventService.getXpEvents())
  }

  @Secured("ROLE_ADMIN")
  @GetMapping("/{id}")
  fun getXpEvent(@PathVariable id: Long):ResponseEntity<XpEvent>{
    return ResponseEntity.ok().body(xpEventService.getXpEventById(id))
  }

  @Secured("ROLE_ADMIN")
  @DeleteMapping("/{id}")
  fun deleteXpEvent(@PathVariable id: Long): ResponseEntity<String> {
    xpEventService.deleteXpEvent(id)
    return ResponseEntity.ok().body("XpEvent deleted")
  }
}
