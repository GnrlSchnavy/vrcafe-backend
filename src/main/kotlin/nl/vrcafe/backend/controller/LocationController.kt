package nl.vrcafe.backend.controller

import nl.vrcafe.backend.model.form.CreateLocationForm
import nl.vrcafe.backend.model.jpa.Location
import nl.vrcafe.backend.service.LocationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/location")
class LocationController {

  @Autowired
  lateinit var locationService: LocationService

  @Secured("ROLE_ADMIN")
  @PostMapping("")
  fun createLocation(@RequestBody location: CreateLocationForm): ResponseEntity<Location> {
    return ResponseEntity.ok().body(locationService.createLocation(location))
  }

  @Secured("ROLE_ADMIN")
  @DeleteMapping("/{id}")
  fun deleteLocation(@PathVariable id: Long): ResponseEntity<Any> {
    locationService.deleteLocation(id)
    return ResponseEntity.accepted().build()
  }

  @Secured("ROLE_ADMIN")
  @GetMapping("/{id}")
  fun getLocation(@PathVariable id: Long): ResponseEntity<Location> {
    return ResponseEntity.ok().body(locationService.getLocation(id))
  }

  @Secured("ROLE_ADMIN")
  @PutMapping("/{id}")
  fun updateLocation(@PathVariable id: Long, @RequestBody location: CreateLocationForm): ResponseEntity<Location> {
    return ResponseEntity.ok().body(locationService.updateLocation(id, location))
  }

  @Secured("ROLE_ADMIN")
  @GetMapping("")
  fun getAllLocations(): ResponseEntity<List<Location>> {
    return ResponseEntity.ok().body(locationService.getAllLocations())
  }


}
