package nl.vrcafe.backend.controller

import nl.vrcafe.backend.model.form.UpdateProfileForm
import nl.vrcafe.backend.model.jpa.Profile
import nl.vrcafe.backend.service.AccountService
import nl.vrcafe.backend.service.ProfileService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/profile")
internal class ProfileController {

    @Autowired
    lateinit var accountService: AccountService

    @PutMapping
    fun updateProfile(@RequestBody profile: UpdateProfileForm): ResponseEntity<Profile> {
        return ResponseEntity.ok().body(accountService.updateProfile(profile))
    }

}