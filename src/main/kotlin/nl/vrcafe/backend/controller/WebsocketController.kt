package nl.vrcafe.backend.controller

import nl.vrcafe.backend.handler.WebSocketHandler
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/api/websocket")
class WebsocketController {

  @Secured("ROLE_ADMIN")
  @GetMapping
  fun resetWebsockets(){
    WebSocketHandler().sendToAllUsers("Server has restarted", MessageType.SERVER_RESTART)
  }

}
