package nl.vrcafe.backend.controller

import nl.vrcafe.backend.model.DataContainer
import nl.vrcafe.backend.model.form.CreateProductForm
import nl.vrcafe.backend.model.jpa.Product
import nl.vrcafe.backend.model.view.ProductView
import nl.vrcafe.backend.service.ProductService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/product")
class ProductController {

  @Autowired
  lateinit var productService: ProductService

  @Secured("ROLE_ADMIN")
  @PostMapping
  fun createProduct(@RequestBody product: CreateProductForm): ResponseEntity<ProductView> {
    return ResponseEntity.ok().body(productService.createProduct(product))
  }

  @GetMapping
  fun getAllProducts(
    @RequestParam(name = "offset", required = false, defaultValue = "0") offset: Int,
    @RequestParam(name = "size", required = false, defaultValue = "10") size: Int,
    @RequestParam(name = "query", required = false, defaultValue = "") query: String
  ): ResponseEntity<DataContainer<List<ProductView>>> {
    return ResponseEntity.ok().body(productService.getAllProducts(offset, size, query))
  }

  @Secured("ROLE_ADMIN")
  @PutMapping("/{id}")
  fun updateProduct(
    @RequestBody product: CreateProductForm,
    @PathVariable(required = true) id: Long
  ): ResponseEntity<Product> {
    return ResponseEntity.ok().body(productService.updateProduct(product.copy(id = id)))
  }

  @Secured("ROLE_ADMIN", "ROLE_USER")
  @GetMapping("/{id}")
  fun getProduct(@PathVariable(required = true) id: Long): ResponseEntity<Product> {
    return ResponseEntity.ok().body(productService.getProductById(id))
  }

  @Secured("ROLE_ADMIN")
  @DeleteMapping("/{id}")
  fun deleteProduct(@PathVariable(required = true) id: Long): ResponseEntity<Void> {
    productService.removeProduct(id)
    return ResponseEntity.ok().build()
  }

}
