package nl.vrcafe.backend.controller

import SessionView
import jakarta.annotation.security.RolesAllowed
import nl.vrcafe.backend.model.DataContainer
import nl.vrcafe.backend.model.form.AddProductsToSessionForm
import nl.vrcafe.backend.model.form.ScanQrCodeForm
import nl.vrcafe.backend.model.view.AddProductToSessionView
import nl.vrcafe.backend.model.view.SessionSummaryView
import nl.vrcafe.backend.service.SessionService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/session")
class SessionController {

  @Autowired
  lateinit var sessionService: SessionService

  @Secured("ROLE_ADMIN")
  @PostMapping("/scanSession")
  fun scanSession(@RequestBody scanQrCodeForm: ScanQrCodeForm): ResponseEntity<SessionView> {
    return ResponseEntity.ok(sessionService.scanSession(scanQrCodeForm.qrCode))
  }

  @Secured("ROLE_ADMIN")
  @PostMapping("/addProductsToSession/{qrCode}")
  fun addProductsToSession(
    @RequestBody addProductsToSessionForm: List<AddProductsToSessionForm>,
    @PathVariable qrCode: String
  ): ResponseEntity<List<AddProductToSessionView>> {
    return ResponseEntity.ok().body(sessionService.addProductsToSession(qrCode, addProductsToSessionForm))
  }

  @Secured("ROLE_ADMIN", "ROLE_USER")
  @GetMapping(value = ["/latestSessionSummary/{id}", "/latestSessionSummary"])
  fun getLatestSessionSummary(@PathVariable(required = false) id: Long?): ResponseEntity<SessionSummaryView> {
    return ResponseEntity.ok().body(sessionService.getLatestSessionSummary(id))
  }

  @Secured("ROLE_ADMIN", "ROLE_USER")
  @GetMapping()
  fun getAllSessions(
    @RequestParam(name = "offset", required = false, defaultValue = "0") offset: Int,
    @RequestParam(name = "size", required = false, defaultValue = "10") size: Int
  ): ResponseEntity<DataContainer<List<SessionView>>> {
    return ResponseEntity.ok().body(sessionService.getSessionsAsAdmin(offset, size))
  }

  @Secured("ROLE_ADMIN", "ROLE_USER")
  @GetMapping("/{id}")
  fun getSession(@PathVariable id: Long): ResponseEntity<SessionSummaryView> {
    return ResponseEntity.ok().body(sessionService.getSession(id))
  }
}

