package nl.vrcafe.backend.controller

const val ACCOUNT_NOT_FOUND = "Account not found!"
const val FRIEND_NOT_FOUND = "Friend not found!"
const val APP_USER_NOT_FOUND = "App user not found!"
const val LOCATION_NOT_FOUND = "Location not found!"
const val ROLE_NOT_SUPPORTED = "Role not supported!"
const val INCORRECT_CREDENTIALS = "Incorrect credentials given during login!"
const val EXCESSIVE_LOGIN_ATTEMPTS = "Too many failed login attempts!"
const val ACCOUNT_LOCKED = "Account is locked!"
const val AUTHENTICATION_EXCEPTION = "Authentication exception!"
const val WEBSOCKET_NOT_CONNECTED = "No WebSocket connection to the client!"
const val WEBSOCKET_INVALID_JSESSIONID = "Cannot create WebSocket because invalid session!"
const val NO_LOCATION = "No location(s) given!"
const val EMAIL_ALREADY_EXISTS = "This email address is already in use!"
const val ROLE_NOT_FOUND = "Role not found!"
const val XP_TYPE_NOT_FOUND = "XP type not found!"
const val XPMAP_NOT_FOUND = "Xp map not found!"
const val XP_MOD_NOT_FOUND = "Xp mod not found!"
const val PRODUCT_NOT_FoUND = "Product not found!"
const val NO_ACTIVE_SESSION_FOUND = "No active session was found!"
const val SESSION_NOT_FOUND = "Session with given id not found"
const val USER_HAS_NEVER_HAD_SESSION = "User has never had any sessions"
const val USER_NOT_ACTIVE_SESSION = "User in not in an active session"
const val XP_EVENT_NOT_FOUND = "Xpevent not found"
const val NO_XP_MOD_PROVIDED = "No xp mod provided"
const val XP_MAP_NOT_FOUND = "No xp map found for product"
const val WEBSOCKET_NOT_ACTIVE = "Something went wrong, try refreshing the page"
const val FORGOT_PASSWORD_TOKEN_NOT_FOUND = "Account not found with given forgot password token"
const val ACHIEVEMENT_NOT_FOUND = "Achievement not found!"

