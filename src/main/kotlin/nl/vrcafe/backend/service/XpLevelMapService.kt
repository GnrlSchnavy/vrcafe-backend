package nl.vrcafe.backend.service

import nl.vrcafe.backend.repo.XpLevelMapRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class XpLevelMapService {

  @Autowired
  lateinit var xpLevelMapRepo: XpLevelMapRepo

  fun getLevelByXp(xp: Long): Long {
    return xpLevelMapRepo.findByXpGreaterThanEqual(xp).minByOrNull { it.level }!!.level
  }

  fun getRankNameByLevel(level: Long): String {
    return xpLevelMapRepo.findByLevel(level).rankName
  }

  fun calculateXpPercentage(currentXp: Long, level: Long): Double {
      val currentXpLevel = xpLevelMapRepo.findByLevel(if (level == 1L) 1L else level - 1L)
      val nextXpLevel = xpLevelMapRepo.findByLevel(level)
      val currentXpInLevel = currentXp - currentXpLevel.xp
      val totalXpInLevel = nextXpLevel.xp - currentXpLevel.xp
      return (currentXpInLevel.toDouble() / totalXpInLevel.toDouble())
  }


}
