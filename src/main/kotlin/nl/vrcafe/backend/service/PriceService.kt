package nl.vrcafe.backend.service

import nl.vrcafe.backend.model.ProductTypeEnum
import nl.vrcafe.backend.model.jpa.Product
import nl.vrcafe.backend.model.jpa.session.SessionProduct
import nl.vrcafe.backend.model.jpa.xp.XpMod
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class PriceService {

  @Autowired
  lateinit var xpModService: XpModService

  @Autowired
  lateinit var productService: ProductService

  fun calculateSessionPrice(product: Product, currentPlayMinutes: Long): Long {
    val product = productService.getProductByProductType(ProductTypeEnum.PLAYTIME)
    return doPriceMultiplication(product, currentPlayMinutes)
  }

  fun doPriceMultiplication(product: Product, amount: Long): Long {
    return if (product.xpMap.xpMod != null) {
      val xpMod: XpMod = xpModService.getXpModById(product.xpMap.xpMod!!.id)
      amount / xpMod.tempAmount * xpMod.tempReward
    } else {
      amount / product.xpMap.amount * product.xpMap.reward
    }
  }

  fun calculateProductPrice(sessionProduct: SessionProduct): Long {
    return doPriceMultiplication(sessionProduct.product, sessionProduct.amount)
  }
}
