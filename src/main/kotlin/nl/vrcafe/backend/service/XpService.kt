package nl.vrcafe.backend.service

import nl.vrcafe.backend.model.ProductTypeEnum
import nl.vrcafe.backend.model.jpa.Product
import nl.vrcafe.backend.model.jpa.session.Session
import nl.vrcafe.backend.model.jpa.xp.XpMod
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class XpService {

    @Autowired
    lateinit var xpModService: XpModService

    @Autowired
    lateinit var productService: ProductService

    fun calculateSessionXp(session: Session, sessionDuration: Long): Long {
        val product = productService.getProductByProductType(ProductTypeEnum.PLAYTIME)
        return doXpMultiplication(product, sessionDuration)
    }

    fun calculateProductXp(product: Product, amount: Long): Long {
        return doXpMultiplication(product, amount)
    }

    private fun doXpMultiplication(product: Product, amount: Long): Long {
        val xpAmount = if (product.xpMap.xpMod != null) {
            val xpMod: XpMod = xpModService.getXpModById(product.xpMap.xpMod!!.id)
            amount / xpMod.tempAmount * xpMod.tempReward
        } else {
            amount / product.xpMap.amount * product.xpMap.reward
        }
        return xpAmount
    }

}
