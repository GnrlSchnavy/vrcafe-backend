package nl.vrcafe.backend.service

import nl.vrcafe.backend.config.WebSecurityConfig.MyUserPrincipal
import nl.vrcafe.backend.controller.PRODUCT_NOT_FoUND
import nl.vrcafe.backend.model.DataContainer
import nl.vrcafe.backend.model.ProductTypeEnum
import nl.vrcafe.backend.model.jpa.account.Account
import nl.vrcafe.backend.model.jpa.xp.XpAccountHistory
import nl.vrcafe.backend.model.view.AccountView
import nl.vrcafe.backend.model.view.SessionProductView
import nl.vrcafe.backend.model.view.XpAccountHistoryView
import nl.vrcafe.backend.repo.XpAccountHistoryRepo
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.http.HttpStatus
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class XpAccountHistoryService {

  @Autowired
  lateinit var xpAccountHistoryRepo: XpAccountHistoryRepo

  @Autowired
  lateinit var productService: ProductService

  private val logger = LoggerFactory.getLogger(AccountService::class.java)

  fun addToHistory(productId: Long, account: Account, productType: ProductTypeEnum, minutes: Long, sessionXp: Long, name: String) {
    logger.info("Adding to history: accountId: $account, minutes: $minutes, xp: $sessionXp, $name")
    val product = productService.getProductById(productId) ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, PRODUCT_NOT_FoUND)
    xpAccountHistoryRepo.save(XpAccountHistory(-1, account, product, minutes, sessionXp, name))
  }

  fun getXpAccountHistory(offset: Int, size: Int): DataContainer<List<XpAccountHistoryView>> {
    val account = (SecurityContextHolder.getContext().authentication.principal as MyUserPrincipal).account
    val pageRequest: PageRequest = PageRequest.of(offset/size, size)
    val list = xpAccountHistoryRepo.findByAccountOrderByCreatedDesc(account, pageRequest).map{
      XpAccountHistoryView(
        it.id,
        amount = it.amount,
        reward = it.reward,
        actorName = it.actorName,
        created = it.created,
        sessionProductView = SessionProductView(name = it.product.name),
        account = AccountView(email = it.account.email)
      )
    }
    return DataContainer(list.content, list.totalElements, list.pageable.pageSize,offset + size)
  }
}
