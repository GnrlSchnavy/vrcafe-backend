package nl.vrcafe.backend.service

import com.amazonaws.services.simpleemail.AmazonSimpleEmailService
import com.amazonaws.services.simpleemail.model.Body
import com.amazonaws.services.simpleemail.model.Content
import com.amazonaws.services.simpleemail.model.Destination
import com.amazonaws.services.simpleemail.model.Message
import com.amazonaws.services.simpleemail.model.SendEmailRequest
import nl.vrcafe.backend.config.ConfigProperties
import nl.vrcafe.backend.model.jpa.session.Session
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

//TODO make more modular -> template should have it's own place
@Service
class EmailService {

  @Autowired
  lateinit var amazonSimpleEmailService: AmazonSimpleEmailService

  @Autowired
  lateinit var config: ConfigProperties

  private val logger = org.slf4j.LoggerFactory.getLogger(this.javaClass)

  fun sendForgotPasswordEmail(receipiantEmail: String, forgotPasswordToken: String) {
    val emailContent = """
      <!DOCTYPE html>
      <html lang="en">
      <head>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <title>VRcafe wachtwoord vergeten</title>
      </head>
      <body style="background: whitesmoke; padding: 30px; height: 100%">
          <h5 style="font-size: 18px; margin-bottom: 6px">Hoi!</h5>
          <p style="font-size: 16px; font-weight: 500">Vervelend dat je je wachtwoord vergeten bent! Als je deze <a
                  href="https://app.vrcafehaarlem.nl/change-password/${forgotPasswordToken}">link</a> volgt kun je opnieuw je
              wachtwoord instellen!</p>
          <p>Met vriendelijke groet,</p>
          <p>Het VRcafe team</p>
      </body>
      </html>
    """.trimIndent()

    val senderEmail = "forgotpassword@vrcafehaarlem.nl"
    val emailSubject = "Wachtwoord opnieuw instellen"
    try {
      val sendEmailRequest = SendEmailRequest()
        .withDestination(Destination().withToAddresses(receipiantEmail))
        .withMessage(
          Message().withBody(Body().withHtml(Content().withCharset("UTF-8").withData(emailContent)))
            .withSubject(Content().withCharset("UTF-8").withData(emailSubject))
        )
        .withSource(senderEmail)
      amazonSimpleEmailService.sendEmail(sendEmailRequest)
      logger.info("Send password reset email to $receipiantEmail")
    } catch (e: Exception) {
      e.printStackTrace()
    }
  }

  fun lingeringSessions(openSessions: List<Session>) {
    val emailContent = """
      <!DOCTYPE html>
      <html lang="en">
      <head>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <title>Niet afgesloten sessies</title>
      </head>
      <body style="background: whitesmoke; padding: 30px; height: 100%">
          <h5 style="font-size: 18px; margin-bottom: 6px">Hoi!</h5>
          <p style="font-size: 16px; font-weight: 500">Deze sessies zijn niet afgesloten gisteren:</p>
          ${openSessions.map {"<p>${it.account.email} ${it.id} </p>"}.joinToString("")}
          <p>Met vriendelijke groet,</p>
          <p>Het VRcafe team</p>
      </body>
      </html>
    """.trimIndent()
    val senderEmail = "lingering-sessions@vrcafehaarlem.nl"
    val emailSubject = "Wachtwoord opnieuw instellen"
    val receipiantEmail = "yvanstemmerik@gmail.com" //TODO change to correct email, maybe insert through app props
    try {
      val sendEmailRequest = SendEmailRequest()
        .withDestination(Destination().withToAddresses(receipiantEmail))
        .withMessage(
          Message().withBody(Body().withHtml(Content().withCharset("UTF-8").withData(emailContent)))
            .withSubject(Content().withCharset("UTF-8").withData(emailSubject))
        )
        .withSource(senderEmail)
      amazonSimpleEmailService.sendEmail(sendEmailRequest)
      logger.info("Send lingering passwords $receipiantEmail")
    } catch (e: Exception) {
      e.printStackTrace()
    }
  }
}
