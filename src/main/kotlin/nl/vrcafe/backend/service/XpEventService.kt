package nl.vrcafe.backend.service


import nl.vrcafe.backend.controller.NO_XP_MOD_PROVIDED
import nl.vrcafe.backend.controller.XP_EVENT_NOT_FOUND
import nl.vrcafe.backend.controller.XP_MAP_NOT_FOUND
import nl.vrcafe.backend.model.form.CreateXpEventForm
import nl.vrcafe.backend.model.jpa.xp.XpEvent
import nl.vrcafe.backend.model.jpa.xp.XpMod
import nl.vrcafe.backend.model.view.XpMapView
import nl.vrcafe.backend.repo.XpEventRepo
import nl.vrcafe.backend.util.Utils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.time.ZoneId
import java.time.ZonedDateTime

@Service
class XpEventService : Utils() {

  val logger: Logger = LoggerFactory.getLogger(this::class.java)

  @Autowired
  lateinit var xpModService: XpModService

  @Autowired
  lateinit var xpEventRepo: XpEventRepo

  @Autowired
  lateinit var xpMapService: XpMapService

  @Autowired
  lateinit var xpMapHistoryService: XpMapHistoryService

  @Autowired
  lateinit var productService: ProductService

  fun createXpEvent(createXpEventForm: CreateXpEventForm): XpEvent {
//TODO find a nicer way to check if any field in an object is null, maybe using reflection?
    val xpEvent = xpEventRepo.save(
      XpEvent(
        -1,
        createXpEventForm.name ?: cantBeNull("name"),
        createXpEventForm.description ?: cantBeNull("description"),
        createXpEventForm.eventStart ?: cantBeNull("eventStart"),
        createXpEventForm.eventEnd,
        false,
        SecurityContextHolder.getContext().authentication.name,
      )
    )
    val xpMods: List<XpMod> = createXpEventForm.eventProducts?.map {
      val product = productService.getProductById(it.productId)
      val xpMap = product?.xpMap ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, XP_MAP_NOT_FOUND)
      val xpMod = xpModService.createXpMod(
        XpMod(
          -1,
          xpEvent,
          xpMap,
          xpMap.amount,
          xpMap.reward,
          it.tempAmount,
          it.tempReward
        )
      )
      xpMod
    } ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, NO_XP_MOD_PROVIDED)
    xpEvent.xpMods = xpMods
    checkForXpEventes()
    return xpEvent
  }


  fun getXpEvents(): List<XpEvent>? {
    return xpEventRepo.findAll() //TODO create smaller view and don't fetch xpmods
  }

  fun getXpEventById(id: Long): XpEvent {
    return xpEventRepo.findByIdOrNull(id) ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, XP_EVENT_NOT_FOUND)
  }

  fun deleteXpEvent(id: Long) {
    val xpEvent = getXpEventById(id)
    return xpEventRepo.delete(xpEvent)
  }

  @Scheduled(fixedRate = 1000 * 60 * 60, initialDelay = 1000 * 60) //TODO keep in mind that this might not scale as well over multiple instances
  fun checkForXpEventes(): Map<String, String> {
//    activate non-active events -> start date before now, end date after now
    val localTime = ZonedDateTime.now(ZoneId.of("Europe/Amsterdam")).toLocalDateTime()
    val toActivate: List<XpEvent> =
      xpEventRepo.findAllByEventStartBeforeAndEventEndAfterAndActivated(localTime, localTime, false)
    toActivate.forEach { xpEvent ->
      logger.info("Activating ${xpEvent.name}")
      xpEvent.activated = true
      xpEvent.xpMods.forEach { xpMod ->
        val xpMap = xpMapService.findXpMapById(xpMod.xpMap.id)!!
        val updatedXpMap = XpMapView(xpMap.id, xpMod, xpMap.amount, xpMap.reward)
        xpMapService.updateXpMap(updatedXpMap)
        xpMapHistoryService.createXpMapHistory(xpMap, xpMod, xpEvent.createdBy)
      }
      xpEventRepo.save(xpEvent)
    }

    val toDeactivate: List<XpEvent> =
      xpEventRepo.findAllByEventEndBeforeAndActivated(localTime, true)
    toDeactivate.forEach { xpEvent ->
      logger.info("Deactivating ${xpEvent.name}")
      xpEvent.activated = false
      xpEvent.xpMods.forEach { xpMod ->
        val xpMap = xpMapService.findXpMapById(xpMod.xpMap.id)!!
        val updatedXpMap = XpMapView(xpMap.id,null, xpMap.amount, xpMap.reward)
        xpMapService.updateXpMap(updatedXpMap)
        xpMapHistoryService.createXpMapHistory(xpMap, null, xpEvent.createdBy)
      }
      xpEventRepo.save(xpEvent)
    }
    return mapOf(
      "toActivate" to toActivate.joinToString { it.name },
      "toDeactivate" to toDeactivate.joinToString { it.name })
  }


}
