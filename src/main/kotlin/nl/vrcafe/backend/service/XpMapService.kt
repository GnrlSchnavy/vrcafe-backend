package nl.vrcafe.backend.service

import nl.vrcafe.backend.controller.XPMAP_NOT_FOUND
import nl.vrcafe.backend.model.jpa.xp.XpMap
import nl.vrcafe.backend.model.view.XpMapView
import nl.vrcafe.backend.repo.XpMapRepo
import nl.vrcafe.backend.util.Utils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class XpMapService {

  @Autowired
  lateinit var xpMapRepo: XpMapRepo

  @Autowired
  lateinit var xpMapHistoryService: XpMapHistoryService

  fun createXpMap(xpMap: XpMap): XpMap {
    return xpMapRepo.save(xpMap)
  }

  fun findXpMapById(id: Long): XpMap? {
    return xpMapRepo.findByIdOrNull(id)
      ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, XPMAP_NOT_FOUND)
  }

  fun updateXpMap(xpMapView: XpMapView): XpMap {
    val toUpdateXpMap = findXpMapById(xpMapView.id!!)!!
    xpMapHistoryService.createXpMapHistory(toUpdateXpMap, null, Utils().getActiveUser().email)
    return xpMapRepo.save(
      XpMap(
        xpMapView.id,
        xpMapView.xpMod,
        xpMapView.amount ?: toUpdateXpMap.amount,
        xpMapView.reward ?: toUpdateXpMap.reward
      )
    )
  }
}
