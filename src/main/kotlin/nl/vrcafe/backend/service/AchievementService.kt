package nl.vrcafe.backend.service

import nl.vrcafe.backend.controller.ACHIEVEMENT_NOT_FOUND
import nl.vrcafe.backend.model.DataContainer
import nl.vrcafe.backend.model.jpa.account.Account
import nl.vrcafe.backend.model.jpa.achievement.Achievement
import nl.vrcafe.backend.model.jpa.achievement.AchievementProgress
import nl.vrcafe.backend.model.jpa.session.SessionProduct
import nl.vrcafe.backend.model.view.AchievementView
import nl.vrcafe.backend.repo.AchievementLevelMapRepo
import nl.vrcafe.backend.repo.AchievementProgressRepo
import nl.vrcafe.backend.repo.AchievementRepo
import nl.vrcafe.backend.util.Utils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class AchievementService {

  @Autowired
  lateinit var achievementRepo: AchievementRepo

  @Autowired
  lateinit var achievementProgressRepo: AchievementProgressRepo

  @Autowired
  lateinit var achievementLevelMapRepo: AchievementLevelMapRepo

  fun getAllAchievements(): List<Achievement> {
    return achievementRepo.findAll()
  }

  fun updateAchievementProgress(
    account: Account,
    sessionProducts: List<SessionProduct>,
    newXp: Long
  ): List<AchievementProgress> {
    val achievements = achievementRepo.findAll()
    return sessionProducts.mapNotNull { sessionProduct ->
      when (sessionProduct.product.name) {
        "Speeltijd" -> updatePlaytimeAchievement(sessionProduct.amount, achievements, account)
        "Cola" -> updateColaAchievementProgress(sessionProduct.amount, achievements, account)
        else -> null
      }
    }
  }

  private fun updatePlaytimeAchievement(
    amount: Long,
    achievements: MutableList<Achievement>,
    account: Account
  ): AchievementProgress {
    val achievement = achievements.firstOrNull { it.name == "Addict" } ?: throw ResponseStatusException(
      HttpStatus.NOT_FOUND,
      ACHIEVEMENT_NOT_FOUND
    )
    return updateAchievementProgress(account, achievement, amount)
  }

  private fun updateColaAchievementProgress(
    amount: Long,
    achievements: MutableList<Achievement>,
    account: Account
  ): AchievementProgress {
    val achievement = achievements.firstOrNull { it.name == "Coke drinker" } ?: throw ResponseStatusException(
      HttpStatus.NOT_FOUND,
      ACHIEVEMENT_NOT_FOUND
    )
    return updateAchievementProgress(account, achievement, amount)
  }

  private fun updateAchievementProgress(
    account: Account,
    achievement: Achievement,
    amount: Long
  ): AchievementProgress {
    val achievementProgress =
      achievementProgressRepo.findByAccountAndAchievement(account, achievement) ?: AchievementProgress(
        -1,
        account,
        achievement,
        0,
        "",
        0.0
      )
    val newProgress = achievementProgress.currentValue + amount
    val achievementMap = achievementLevelMapRepo.findFirstByAchievementAndMinValueLessThanEqualAndMaxValueAfter(
      achievement,
      newProgress,
      newProgress
    )
    return achievementProgressRepo.save(
      AchievementProgress(
        achievementProgress.id,
        account,
        achievement,
        newProgress,
        achievementMap.rankName,
        calculatePercentage(achievementMap.minValue, achievementMap.maxValue, newProgress)
      )
    )
  }

  private fun calculatePercentage(min: Long, max: Long, currentValue: Long): Double {
    return ((currentValue.toDouble() - min) / (max - min))
  }

  fun getAllAchievementsProgress(offset: Int, size: Int): DataContainer<List<AchievementView>> {
    val pageRequest = PageRequest.of(offset / size, size)
    val achievementProgressions = achievementProgressRepo.findAllByAccountIdOrderByPercentageDesc(
      Utils().getActiveUser().id,
      pageRequest
    )
    return DataContainer(
      achievementProgressions.content.map{it.toView()},
      achievementProgressions.totalElements,
      achievementProgressions.pageable.pageSize,
      offset + size
    )
  }
}
