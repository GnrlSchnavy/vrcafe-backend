package nl.vrcafe.backend.service

import nl.vrcafe.backend.config.WebSecurityConfig.MyUserPrincipal
import nl.vrcafe.backend.controller.ACCOUNT_NOT_FOUND
import nl.vrcafe.backend.controller.EMAIL_ALREADY_EXISTS
import nl.vrcafe.backend.controller.FORGOT_PASSWORD_TOKEN_NOT_FOUND
import nl.vrcafe.backend.handler.WebSocketHandler
import nl.vrcafe.backend.model.DataContainer
import nl.vrcafe.backend.model.form.*
import nl.vrcafe.backend.model.jpa.Profile
import nl.vrcafe.backend.model.jpa.account.Account
import nl.vrcafe.backend.model.jpa.achievement.AchievementProgress
import nl.vrcafe.backend.model.view.AccountView
import nl.vrcafe.backend.model.view.ProfileView
import nl.vrcafe.backend.model.view.TinyAccountView
import nl.vrcafe.backend.repo.AccountRepo
import nl.vrcafe.backend.repo.RoleRepo
import nl.vrcafe.backend.util.JwtUtil
import nl.vrcafe.backend.util.Utils
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.factory.PasswordEncoderFactories
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.server.ResponseStatusException
import java.time.Duration
import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.*

@Service
class AccountService : AbstractSearchService() {

    @Autowired
    lateinit var accountRepo: AccountRepo

    @Autowired
    lateinit var roleRepo: RoleRepo

    @Autowired
    lateinit var userDetailsService: MyUserDetailsService

    @Autowired
    lateinit var jwtUtil: JwtUtil

    @Autowired
    lateinit var qrCodeService: QrCodeService

    @Autowired
    lateinit var emailService: EmailService

    @Autowired
    lateinit var xpLevelMapService: XpLevelMapService

    @Autowired
    lateinit var achievementService: AchievementService

    @Autowired
    lateinit var profileService: ProfileService

    private val logger = LoggerFactory.getLogger(AccountService::class.java)

    fun register(account: RegisterForm): AccountView? {
        if (accountRepo.findByEmail(account.email) != null) {
            throw ResponseStatusException(HttpStatus.CONFLICT, EMAIL_ALREADY_EXISTS)
        }
        val encryptedPassword = PasswordEncoderFactories.createDelegatingPasswordEncoder().encode(account.password)
        saveAccount(account, encryptedPassword)
        return authenticate(account.email, account.password)
    }

    fun login(loginForm: LoginForm): AccountView? {
        return authenticate(loginForm.email, loginForm.password)
    }

    fun saveAccount(account: RegisterForm, encryptedPassword: String): Account {
        val uuid = UUID.randomUUID().toString()
        val rank = xpLevelMapService.getRankNameByLevel(1)
        val profile = profileService.createEmptyProfile()
        val account = accountRepo.save(
            Account(
                -1,
                account.email.lowercase().trim(),
                encryptedPassword,
                uuid,
                qrCodeService.generateQRCodeImageBase64(uuid),
                0,
                1,
                rank,
                null,
                null,
                0,
                profile
            )
        )
        achievementService.getAllAchievements().map {
            val achievementMap = achievementService.achievementLevelMapRepo.findByAchievementAndLevel(it, 1)
            achievementService.achievementProgressRepo.save(
                AchievementProgress(
                    -1, account, it, 0, achievementMap.rankName, 0.0
                )
            )
        }
        addRoleToAccount(RoleToAccountForm(account.email, "ROLE_USER"))
        logger.info("Saved account with ${account.id} and $uuid")
        return account
    }

    fun addRoleToAccount(roleToAccountForm: RoleToAccountForm): Account {
        logger.info("Adding role ${roleToAccountForm.role} to account with email ${roleToAccountForm.email}")
        val account = getAccount(roleToAccountForm.email)
        val role = roleRepo.findByName(roleToAccountForm.role)
        val updatedAccount = account.withRoles(mutableListOf(role!!))
        return accountRepo.save(updatedAccount)
    }

    fun getAccount(email: String): Account {
        logger.info("Getting account with email $email")
        return accountRepo.findByEmail(email) ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, ACCOUNT_NOT_FOUND)
    }

    fun getAccount(id: Long?): Account {
        val id = id ?: (SecurityContextHolder.getContext().authentication.principal as MyUserPrincipal).account.id
        logger.info("Getting account with id $id")
        return accountRepo.findByIdOrNull(id) ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, ACCOUNT_NOT_FOUND)
    }

    fun getAccountByProfile(profile: Profile): Account {
        return accountRepo.findByProfile(profile) ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }

    fun getProfileView(id: Long? = null): TinyAccountView {
        val account = getAccount(id)
        val percentage = xpLevelMapService.calculateXpPercentage(account.xp, account.level)
        return TinyAccountView(
            account.id, account.email, account.xp, account.rank, account.level, percentage, account.profile.username
        )
    }

    fun authenticate(email: String, password: String): AccountView? {
        val userDetails = userDetailsService.loadUserByUsername(email.lowercase().trim())
        val account: Account = (userDetails as MyUserPrincipal).account
        val encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder()
        logger.info(
            "Authenticating user ${account.email} , matching passwords: ${
                encoder.matches(
                    password, account.password
                )
            }"
        )
        return if (encoder.matches(password, account.password)) {
            val role: String = account.roles.first().name
            AccountView(
                id = account.id,
                jwt = jwtUtil.generateToken(userDetails),
                email = account.email,
                role = role,
                encodedQrCode = account.encodedQrCode,
                qrCode = account.qrCode,
                xp = account.xp,
                level = account.level
            )
        } else {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid credentials")
        }
    }

    fun findAccountByQrCode(qrCode: String): Account? {
        return accountRepo.findByQrCode(qrCode)
    }

    fun logout() {
        WebSocketHandler().logout(SecurityContextHolder.getContext().authentication.name)
    }

    @Transactional
    fun updateAccount(updatedAccountView: AccountView): Account {
        logger.info("Updating account with id ${updatedAccountView.id}")
        val account = getAccount(updatedAccountView.id!!)
        val updatedAccount = Account(
            id = account.id,
            email = updatedAccountView.email ?: account.email,
            password = updatedAccountView.password ?: account.password,
            qrCode = account.qrCode,
            encodedQrCode = account.encodedQrCode,
            xp = updatedAccountView.xp ?: account.xp,
            level = updatedAccountView.level ?: account.level,
            forgotPasswordToken = updatedAccountView.forgotPasswordToken,
            recoverPasswordTime = updatedAccountView.recoverPasswordTime,
            created = account.created,
            rank = updatedAccountView.rankName ?: account.rank,
            missedCheckouts = updatedAccountView.missedCheckouts ?: account.missedCheckouts,
            profile = updatedAccountView.profileView?.let { Profile(it) } ?: account.profile
        ).withRoles(account.roles)
            .withXpAccountHistory(account.xpAccountHistory)
            .withSessions(account.sessions)
            .withAchievementProgress(account.achievementProgess)
        return accountRepo.saveAndFlush(updatedAccount)
    }

    //  TODO make it so if no roles are given default searc is role_user and role_admin
    fun getAllAccounts(offset: Int, size: Int, query: String): DataContainer<List<AccountView>> {
        val pageRequest = PageRequest.of(offset / size, size)
        val fields = createQueryFields(query, "Account") //TODO get from enum somewhere later
        val accounts = accountRepo.getAccountsSortByLatestSesssion(
            fields["email"] ?: "", pageRequest
        ).map { it.toView() }
        return DataContainer(accounts.content, accounts.totalElements, accounts.pageable.pageSize, offset + size)
    }

    fun forgotPassword(email: String): Any {
        val account = accountRepo.findByEmail(email) ?: return "OK"
        val forgotPasswordToken = UUID.randomUUID()
        updateAccount(
            AccountView(
                id = account.id, forgotPasswordToken = forgotPasswordToken, recoverPasswordTime = Instant.now()
            )
        )
        emailService.sendForgotPasswordEmail(account.email, forgotPasswordToken.toString())
        return "OK"
    }

    fun updatePassword(updatePasswordForm: UpdatePasswordForm): AccountView? {
        val account = accountRepo.findByForgotPasswordToken(updatePasswordForm.token) ?: throw ResponseStatusException(
            HttpStatus.NOT_FOUND, FORGOT_PASSWORD_TOKEN_NOT_FOUND
        )
        val encryptedPassword =
            PasswordEncoderFactories.createDelegatingPasswordEncoder().encode(updatePasswordForm.password)
        updateAccount(AccountView(id = account.id, password = encryptedPassword))
        return authenticate(account.email, updatePasswordForm.password)
    }

    @Scheduled(cron = "0 0 3 * * *")
    fun deleteExpiredPasswordRecoveryToken() {
        val now = ZonedDateTime.now(ZoneId.of("Europe/Amsterdam")).toInstant().minus(Duration.ofHours(3))
        repeat(accountRepo.findByRecoverPasswordTimeBefore(now).size) {
            updateAccount(AccountView(forgotPasswordToken = null, recoverPasswordTime = null))
        }
    }

    fun updateProfile(profile: UpdateProfileForm): Profile {
        val account = Utils().getActiveUser()
        val updatedProfile = ProfileView(
            id = account.profile.id,
            firstName = profile.firstName ?: account.profile.firstName,
            lastName = profile.lastName ?: account.profile.lastName,
            username =  profile.username ?: account.profile.username,
            profilePicture = profile.profilePicture ?: account.profile.profilePicture,
        )
        val accountView = account.toView().copy(profileView = updatedProfile)
        return updateAccount(accountView).profile
    }
}

