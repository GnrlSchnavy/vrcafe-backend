package nl.vrcafe.backend.service

import nl.vrcafe.backend.handler.WebSocketHandler
import nl.vrcafe.backend.model.DataContainer
import nl.vrcafe.backend.model.jpa.ChatMessage
import nl.vrcafe.backend.model.jpa.account.Account
import nl.vrcafe.backend.model.view.ChatMessageView
import nl.vrcafe.backend.repo.ChatMessageRepo
import nl.vrcafe.backend.util.Utils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class ChatService {

    @Autowired
    lateinit var chatMessageRepository: ChatMessageRepo

    @Autowired
    lateinit var accountService: AccountService

    @Autowired
    lateinit var friendService: FriendService

    fun sendChatMessage(message: String, recipientId: Long) {
        val sender = Utils().getActiveUser()
        if (sender.id == recipientId) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "You can't send a message to yourself!")
        }

        val relation = friendService.getRelation(recipientId)
        if (relation.content.size != 2) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "User is not a friend")
        }
        val recipient = relation
            .find { it.friend.id == recipientId }
            ?.let { accountService.getAccount(it.friend.id) }

        val chatMessage = ChatMessage(
            -1,
            message,
            sender.id,
            recipientId,
        )

        chatMessageRepository.save(chatMessage)
        WebSocketHandler().sendToUser(
            recipient!!.email,
            chatMessage.withNames(sender.email, recipient.email),
            MessageType.CHAT_MESSAGE,
            throwError = false
        )
    }

    fun getChatMessages(recipientId: Long, offset: Int, size: Int): DataContainer<List<ChatMessageView>> {
        val account = Utils().getActiveUser()

        val recipient = friendService.findFriend(recipientId)?.let {
            accountService.getAccount(it.friend.id)
        } ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "You are not friends with this user!")

        val pageable = PageRequest.of(offset / size, size)
        val pageResult = chatMessageRepository.findSendAndReceivedMessages(account.id, recipient.id, pageable)
        val messages = addSenderReceiverToMessages(pageResult.content, account, recipient)
        return DataContainer(
            messages,
            pageResult.totalElements,
            pageResult.pageable.pageSize,
            offset + size
        )
    }

    private fun addSenderReceiverToMessages(
        content: List<ChatMessage>,
        account: Account,
        recipient: Account,
    ): List<ChatMessageView> {
        return content.map {
            if (it.senderId == account.id) {
                it.withNames(account.email, recipient.email)
            } else {
                it.withNames(recipient.email, account.email)
            }
        }
    }
}
