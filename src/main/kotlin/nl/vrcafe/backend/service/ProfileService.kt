package nl.vrcafe.backend.service

import nl.vrcafe.backend.model.jpa.Profile
import nl.vrcafe.backend.repo.ProfileRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class ProfileService {

    @Autowired
    lateinit var profileRepo: ProfileRepo

    fun createEmptyProfile(): Profile {
        return profileRepo.save(Profile())
    }

    fun getProfileByUsername(username: String): Profile {
        return profileRepo.findDistinctFirstByUsername(username) ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }

}
