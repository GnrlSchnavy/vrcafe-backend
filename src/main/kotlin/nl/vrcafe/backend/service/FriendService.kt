package nl.vrcafe.backend.service

import jakarta.transaction.Transactional
import nl.vrcafe.backend.handler.WebSocketHandler
import nl.vrcafe.backend.model.DataContainer
import nl.vrcafe.backend.model.FriendRequestStatus.DECLINED
import nl.vrcafe.backend.model.FriendRequestStatus.ACCEPTED
import nl.vrcafe.backend.model.FriendRequestStatus.PENDING
import nl.vrcafe.backend.model.jpa.FriendList
import nl.vrcafe.backend.model.jpa.account.Account
import nl.vrcafe.backend.model.view.TinyAccountView
import nl.vrcafe.backend.repo.FriendRepo
import nl.vrcafe.backend.util.Utils
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class FriendService(
    val friendRepo: FriendRepo,
    val accountService: AccountService,
    val profileService: ProfileService,
) {

    fun getFriends(size: Int, offset: Int): DataContainer<List<TinyAccountView>> {
        val accountId = Utils().getActiveUser().id
        val pageRequest = PageRequest.of(offset / size, size)
        val friends = friendRepo.findAllFriends(accountId, pageRequest).map {
            it.account.toTinyAccountView()
        }
        return DataContainer(friends.content, friends.totalElements, friends.pageable.pageSize, offset + size)
    }

    fun sendFriendRequest(friend: Long): TinyAccountView {
        val account = Utils().getActiveUser()
        if (account.id == friend) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "You can't add yourself as a friend")
        }
        if (friendRepo.findByAccountAndFriend(account, accountService.getAccount(friend)) != null) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "You are already friends with this user")
        }
        accountService.getAccount(friend).let {
            friendRepo.save(FriendList(-1, account, it, PENDING.name))
            friendRepo.save(FriendList(-1, it, account, PENDING.name))
            return it.toTinyAccountView()
        }
    }

    fun sendFriendRequest(username: String): TinyAccountView {
        val account = profileService.getProfileByUsername(username).let {
            accountService.getAccountByProfile(it)
        }
        WebSocketHandler().sendToUser(
            email = account.email,
            message = "friend request from $username",
            type = MessageType.FRIEND_REQUEST,
            throwError = false
        )
        return sendFriendRequest(account.id)
    }

    @Transactional
    fun deleteFriend(friend: Long) {
        val account = Utils().getActiveUser()
        accountService.getAccount(friend).let {
            friendRepo.deleteByAccountAndFriend(account, it)
            friendRepo.deleteByAccountAndFriend(it, account)
        }
    }

    fun acceptFriendRequest(friendRequestId: Long) {
        val account = Utils().getActiveUser()
        val friendAccount = accountService.getAccount(friendRequestId)
        runCatching { updateFriendStatus(account, friendAccount, ACCEPTED.name) }.getOrElse {
            throw ResponseStatusException(HttpStatus.PRECONDITION_FAILED)
        }
    }

    fun declineFriendRequest(toLong: Long) {
        val account = Utils().getActiveUser()
        val friendAccount = accountService.getAccount(toLong)
        runCatching {
            updateFriendStatus(friendAccount, account, DECLINED.name)
        }.getOrElse { throw ResponseStatusException(HttpStatus.PRECONDITION_FAILED) }
    }

    private fun updateFriendStatus(
        friendAccount: Account,
        account: Account,
        status: String,
    ) {
        updateRelation(friendAccount, account, status)
        updateRelation(account, friendAccount, status)
    }

    private fun updateRelation(
        rightSide: Account,
        leftSide: Account,
        status: String,
    ) {
        friendRepo.findByAccountAndFriend(leftSide, rightSide)?.let {
            it.requestStatus = status
            friendRepo.save(it)
        } ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Friend request not found")

        friendRepo.findByAccountAndFriend(rightSide, leftSide)?.let {
            it.requestStatus = status
            friendRepo.save(it)
        } ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Friend request not found")
    }

    //TODO merge with general searching and add fields as optional query parameters
    fun getPendingRequests(size: Int, offset: Int): DataContainer<List<TinyAccountView>>? {
        val account = Utils().getActiveUser()
        val pageRequest = PageRequest.of(offset / size, size)
        val pendingRequests = friendRepo.findAllByAccountAndRequestStatus(account, PENDING.name, pageRequest)
            .map { it.friend.toTinyAccountView() }
        return DataContainer(
            pendingRequests.content,
            pendingRequests.totalElements,
            pendingRequests.pageable.pageSize,
            offset + size
        )
    }

    fun findFriend(friendId: Long): FriendList? {
        val account = Utils().getActiveUser()
        return friendRepo.findByAccountAndFriend(account, accountService.getAccount(friendId))
    }

    fun getRelation(recipientId: Long): Page<FriendList> {
        val account = Utils().getActiveUser()
        val pageRequest = PageRequest.of(0, 2)
        return friendRepo.findFriendRelation(recipientId, account.id, pageRequest)
    }
}
