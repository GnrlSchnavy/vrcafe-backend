package nl.vrcafe.backend.service

import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException

abstract class AbstractSearchService {

  fun createQueryFields(query: String, type: String): MutableMap<String, String> {
    if (query.isEmpty()) {
      return mutableMapOf()
    }
    val correctFields = determineCorrectFields(type)
    val queryFields = mutableMapOf<String, String>()
    val fields = query.split(" ")
    fields.forEach {
      val splitQuery = it.split("=")
      queryFields[splitQuery[0]] = splitQuery[1]
    }
    checkQueryFields(correctFields, queryFields)
    return queryFields
  }

  private fun checkQueryFields(correctFields: List<String>, queryFields: Map<String, String>): Boolean {
    if (queryFields.isEmpty()) {
      return true
    }
    val incorrectFields = queryFields.keys.mapNotNull {
      if (it !in correctFields) {
        it
      }
      null
    }

    if (incorrectFields.isNotEmpty()) {
      throw ResponseStatusException(
        HttpStatus.BAD_REQUEST,
        "The following query fields are not allowed: ${incorrectFields.joinToString(", ")}"
      )
    }
    return correctFields.containsAll(queryFields.keys)
  }

//  TODO YS: Add non hardcode search field types
  private fun determineCorrectFields(type: String): List<String> {
    return when (type) {
      "Product" -> listOf("name", "type")
      "Account" -> listOf("email", "role")
      else -> throw ResponseStatusException(
        HttpStatus.BAD_REQUEST,
        "The following search type is not known: $type"
      )
    }
  }
}

