package nl.vrcafe.backend.service

import nl.vrcafe.backend.model.jpa.xp.XpMod
import nl.vrcafe.backend.repo.XpModRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import nl.vrcafe.backend.controller.XP_MOD_NOT_FOUND
import org.springframework.http.HttpStatus


@Service
class XpModService {

  @Autowired
  lateinit var xpModRepo: XpModRepo

  fun createXpMod(xpMod: XpMod): XpMod {
      return xpModRepo.save(xpMod)
  }

  fun getXpModById(id: Long): XpMod {
    return xpModRepo.findByIdOrNull(id) ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, XP_MOD_NOT_FOUND)
  }

}
