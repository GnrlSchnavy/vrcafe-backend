package nl.vrcafe.backend.service

import nl.vrcafe.backend.controller.LOCATION_NOT_FOUND
import nl.vrcafe.backend.model.form.CreateLocationForm
import nl.vrcafe.backend.model.jpa.Location
import nl.vrcafe.backend.repo.LocationRepo
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class LocationService {

    private val logger = LoggerFactory.getLogger(AccountService::class.java)

    @Autowired
    lateinit var locationRepo: LocationRepo

    fun getLocation(id: Long): Location {
        return locationRepo.findByIdOrNull(id)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, LOCATION_NOT_FOUND)
    }

    fun createLocation(locationForm: CreateLocationForm): Location {
        logger.info("Creating location with name: ${locationForm.name}")
        return locationRepo.save(
            Location(
                -1,
                locationForm.name!!,
                locationForm.city!!,
                locationForm.street!!,
                locationForm.houseNumber!!,
            ).withSessions(mutableListOf())
        )
    }

    fun deleteLocation(id: Long) {
        logger.info("Removing location with id: $id")
        locationRepo.deleteById(id)
    }

    fun updateLocation(id: Long, location: CreateLocationForm): Location {
        logger.info("Updating location with id: $id")
        val locationToUpdate = getLocation(id)
        val newLocation = Location(
            locationToUpdate.id,
            location.name ?: locationToUpdate.name,
            location.city ?: locationToUpdate.city,
            location.street ?: locationToUpdate.street,
            location.houseNumber ?: locationToUpdate.houseNumber,
        ).withSessions(locationToUpdate.sessions)
        return locationRepo.save(newLocation)
    }

    fun getAllLocations(): List<Location>? {
        return locationRepo.findAll()
    }
}
