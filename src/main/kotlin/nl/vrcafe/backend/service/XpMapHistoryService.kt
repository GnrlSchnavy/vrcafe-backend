package nl.vrcafe.backend.service

import nl.vrcafe.backend.model.jpa.xp.XpMap
import nl.vrcafe.backend.model.jpa.xp.XpMapHistory
import nl.vrcafe.backend.model.jpa.xp.XpMod
import nl.vrcafe.backend.repo.XpMapHistoryRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.ZoneId
import java.time.ZonedDateTime

@Service
class XpMapHistoryService {

  @Autowired
  lateinit var xpMapHistoryRepo: XpMapHistoryRepo

  fun createXpMapHistory(xpMap: XpMap, xpMod: XpMod?, actor: String) {
    xpMapHistoryRepo.save(
      XpMapHistory(
        -1,
        xpMap,
        xpMod?.tempAmount ?: xpMap.amount,
        xpMod?.tempReward?: xpMap.reward,
        actor,
        ZonedDateTime.now(ZoneId.of("Europe/Amsterdam")).toLocalDateTime()
      )
    )
  }
}
