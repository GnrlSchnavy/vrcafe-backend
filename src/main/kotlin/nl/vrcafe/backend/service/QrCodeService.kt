package nl.vrcafe.backend.service


import com.google.zxing.BarcodeFormat
import com.google.zxing.client.j2se.MatrixToImageWriter
import com.google.zxing.qrcode.QRCodeWriter
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.awt.image.BufferedImage
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.*
import javax.imageio.ImageIO

@Service
class QrCodeService {

    private val logger = LoggerFactory.getLogger(this::class.java)

    fun generateQRCodeImage(barcodeText: String): BufferedImage {
        val barcodeWriter = QRCodeWriter()
        // Height and width are pixels in the resulting image
        val bitMatrix = barcodeWriter.encode(barcodeText, BarcodeFormat.QR_CODE, 200, 200)
        return MatrixToImageWriter.toBufferedImage(bitMatrix)
    }

    fun generateQRCodeImageBase64(barcodeText: String): String {
        lateinit var imageString: String
        val bufferedImage = generateQRCodeImage(barcodeText)
        val bos = ByteArrayOutputStream()

        try {
            ImageIO.write(bufferedImage, "png", bos)
            val imageBytes = bos.toByteArray()

            imageString = Base64.getEncoder().encodeToString(imageBytes)

            bos.close()
        } catch (ioe: IOException) {
            logger.warn("Input output exception on QRCode")
        }
        return imageString
    }
}
