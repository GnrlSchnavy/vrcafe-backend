package nl.vrcafe.backend.service

import MessageType
import SessionView
import jakarta.transaction.Transactional
import nl.vrcafe.backend.controller.*
import nl.vrcafe.backend.handler.WebSocketHandler
import nl.vrcafe.backend.model.ActorEnum
import nl.vrcafe.backend.model.DataContainer
import nl.vrcafe.backend.model.ProductTypeEnum
import nl.vrcafe.backend.model.form.AddProductsToSessionForm
import nl.vrcafe.backend.model.jpa.account.Account
import nl.vrcafe.backend.model.jpa.session.Session
import nl.vrcafe.backend.model.jpa.session.SessionProduct
import nl.vrcafe.backend.model.view.AccountView
import nl.vrcafe.backend.model.view.AddProductToSessionView
import nl.vrcafe.backend.model.view.SessionProductView
import nl.vrcafe.backend.model.view.SessionSummaryView
import nl.vrcafe.backend.repo.SessionProductRepo
import nl.vrcafe.backend.repo.SessionRepo
import nl.vrcafe.backend.util.Utils
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.sql.Timestamp
import java.time.Duration
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime

@Service
class SessionService {

    @Autowired
    lateinit var accountService: AccountService

    @Autowired
    lateinit var sessionRepo: SessionRepo

    @Autowired
    lateinit var locationService: LocationService

    @Autowired
    lateinit var xpService: XpService

    @Autowired
    lateinit var xpLevelMapService: XpLevelMapService

    @Autowired
    lateinit var xpAccountHistoryService: XpAccountHistoryService

    @Autowired
    lateinit var sessionProductRepo: SessionProductRepo

    @Autowired
    lateinit var productService: ProductService

    @Autowired
    lateinit var priceService: PriceService

    @Autowired
    lateinit var achievementService: AchievementService

    @Autowired
    lateinit var emailService: EmailService

    private val logger = LoggerFactory.getLogger(AccountService::class.java)

    @Transactional
    fun scanSession(qrCode: String): SessionView {
        val account = accountService.findAccountByQrCode(qrCode)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, ACCOUNT_NOT_FOUND)
        val locationId = locationService.getAllLocations()
            ?.first()?.id //TODO YS determine location based on loggedin admins location (Needs to be modular in future)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, LOCATION_NOT_FOUND)
        val lastOrCurrentSession = sessionRepo.findFirstByAccountIdOrderByIdDesc(account.id)
        val session = if (lastOrCurrentSession == null || lastOrCurrentSession.checkOut != null) {
            openSession(account, locationId)
        } else {
            val closedSession = closeSession(lastOrCurrentSession)
            updateXp(closedSession, account)
            return SessionView(closedSession.id, closedSession.checkIn, closedSession.checkOut, closedSession.minutes)
        }
        return session
    }

    private fun updateXp(
        closedSession: Session,
        account: Account
    ) {
        val sessionProducts = sessionProductRepo.findAllBySession(closedSession)
        val newXp = calculateNewXp(account, sessionProducts, closedSession)
        val newLevel = xpLevelMapService.getLevelByXp(newXp)
        val newRank = xpLevelMapService.getRankNameByLevel(newLevel)
        achievementService.updateAchievementProgress(account, sessionProducts, newXp)
        accountService.updateAccount(AccountView(id = account.id, xp = newXp, level = newLevel, rankName = newRank))
        val overview: SessionSummaryView = getLatestSessionSummary(account.id)
        notifyUser(account.email, overview, MessageType.USER_SESSION_CHECKOUT)
    }

  private fun calculateNewXp(
    account: Account,
    sessionProducts: List<SessionProduct>,
    closedSession: Session
  ) = account.xp + sessionProducts.sumOf {
    val productType = ProductTypeEnum.valueOf(it.product.productType)
    val xp = if (productType == ProductTypeEnum.PLAYTIME) {
      xpService.calculateSessionXp(closedSession, closedSession.minutes!!)
    } else {
      xpService.calculateProductXp(it.product, it.amount)
    }
    xpAccountHistoryService.addToHistory(
      it.product.id,
      account,
      productType,
      it.amount,
      xp,
      ActorEnum.SESSION_CLOSE.name
    )
    xp
  }

  private fun openSession(account: Account, locationId: Long): SessionView {
        logger.info("Creating new session for account ${account.id} with location $locationId")
        val startSession = createNewSession(account, locationId)
        val overview: SessionSummaryView = getLatestSessionSummary(account.id)
        notifyUser(account.email, overview, MessageType.USER_SESSION_CHECKIN)
        return SessionView(startSession.id, startSession.checkIn, startSession.checkOut, null)
    }

    private fun createNewSession(account: Account, locationId: Long): Session {
        val location = locationService.getLocation(locationId)
        val session = sessionRepo.save(Session(account, location))
        val productId = productService.getProductByProductType(ProductTypeEnum.PLAYTIME).id
        addProductsToSession(account.qrCode, listOf(AddProductsToSessionForm(productId, 0L)))
        return session
    }

    private fun closeSession(session: Session): Session {
        logger.info("Closing session ${session.id}")
        session.checkOut = Utils().currentLocalDateTime()
        session.minutes = calculateMinutesBetween(session.checkIn, session.checkOut!!)
        session.correctCheckout = true
        val updatedSession = sessionRepo.save(session)
        updatePlayTime(session)
        return updatedSession
    }

    fun updatePlayTime(session: Session) {
        val product = productService.getProductByProductType(ProductTypeEnum.PLAYTIME)
        val sessionProductToUpdate = sessionProductRepo.findFirstBySessionAndProduct(session, product)
        sessionProductRepo.save(
            SessionProduct(
                sessionProductToUpdate.id, session, product, session.account,
                session.minutes!!,
                product.price,
                priceService.calculateSessionPrice(product, session.minutes!!),
                xpService.calculateSessionXp(session, session.minutes!!),
                sessionProductToUpdate.created
            )
        )
    }

    private fun calculateMinutesBetween(instant1: LocalDateTime, instant2: LocalDateTime): Long {
        return Duration.between(instant1, instant2).toMinutes()
    }

    private fun notifyUser(email: String, message: Any, messageType: MessageType) {
        WebSocketHandler().sendToUser(email, message, messageType)
    }

    @Transactional
    fun addProductsToSession(
        qrCode: String,
        addProductsToSessionForm: List<AddProductsToSessionForm>
    ): List<AddProductToSessionView> {
        val account = accountService.findAccountByQrCode(qrCode)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, ACCOUNT_NOT_FOUND)
        val session = sessionRepo.findFirstByAccountIdOrderByIdDesc(account.id)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, NO_ACTIVE_SESSION_FOUND)
        if (session.isActive()) {
            val toAddProducts: List<Pair<AddProductToSessionView, String>> = addProductsToSessionForm.map {
                val toAddProduct = productService.getProductById(it.productId)
                    ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, PRODUCT_NOT_FoUND)
                val subTotalPrice = toAddProduct.price * it.amount
                val subTotalXp = xpService.calculateProductXp(toAddProduct, it.amount)
                Pair(
                    sessionProductRepo.save(
                        SessionProduct(
                            -1,
                            session,
                            toAddProduct,
                            account,
                            it.amount,
                            toAddProduct.price,
                            subTotalPrice, subTotalXp
                        )
                    )
                        .toAddProductSessionView(),
                    toAddProduct.productType
                )
            }
            if (toAddProducts.none { it.second == ProductTypeEnum.PLAYTIME.name }) {
                val products = toAddProducts.map { it.first }
                val productsList = products.map {
                    SessionProductView(
                        it.id,
                        it.name,
                        it.unitPrice,
                        it.unitXp,
                        it.subTotalPrice,
                        it.subTotalXp,
                        it.amount,
                        Timestamp(ZonedDateTime.now( ZoneId.of( "Europe/Amsterdam" ) ).toInstant().toEpochMilli()) //TODO YS: fix this :p
                    )
                }
                val productsAddedSummary = SessionSummaryView(
                    session.id,
                    session.checkIn,
                    session.checkOut,
                    session.minutes,
                    productsList,
                    productsList.sumOf { it.subTotalXp!! },
                    productsList.sumOf { it.subTotalPrice!! }
                )
                notifyUser(account.email, productsAddedSummary, MessageType.USER_SESSION_PRODUCT_ADDED)
                logger.info("Adding products: ${addProductsToSessionForm.joinToString { it.productId.toString() }} to session with id: ${session.id} for user $account.email")
            }
            return toAddProducts.map { it.first }
        } else {
            throw ResponseStatusException(
                HttpStatus.NOT_FOUND,
                "User is not in session"
            ) //TODO instead add product without a session
        }
    }

    //    returns active session by any accountId or by requesting user accountId
    private fun getCurrentSession(accountId: Long? = null): Session? {
        val session: Session? = accountId?.let {
            sessionRepo.findFirstByAccountIdOrderByIdDesc(accountId)
        }
            ?: sessionRepo.findFirstByAccountIdOrderByIdDesc(Utils().getActiveUser().id)
        return session
    }

    fun getLatestSessionSummary(accountId: Long? = null): SessionSummaryView {
        val session = getCurrentSession(accountId) ?: return SessionSummaryView(-1, null, null, null, emptyList(), 0, 0)
        val currentPlayMinutes =
            Utils.getDuration(session.checkIn, session.checkOut ?: Utils().currentLocalDateTime()).toMinutes()
        return createSessionView(session, currentPlayMinutes)
    }

    private fun getSessionProducts(
        session: Session,
        currentPlayMinutes: Long
    ) = sessionProductRepo.findAllBySession(session).map { sessionProduct ->
        //      Calcalate for playtime
        if (sessionProduct.product.productType == ProductTypeEnum.PLAYTIME.name) {
            val subTotalXp = xpService.calculateSessionXp(session, currentPlayMinutes)
            val xp = if (currentPlayMinutes == 0L) 1L else subTotalXp / currentPlayMinutes
            SessionProductView(
                sessionProduct.product.id, sessionProduct.product.name, sessionProduct.product.price,
                xp,
                priceService.calculateSessionPrice(sessionProduct.product, currentPlayMinutes),
                subTotalXp,
                currentPlayMinutes,
                sessionProduct.created
            )
        } else {
            val subTotalXp = xpService.calculateProductXp(sessionProduct.product, sessionProduct.amount)
            val xp = sessionProduct.product.xpMap.reward
            SessionProductView(
                sessionProduct.product.id, sessionProduct.product.name, sessionProduct.product.price,
                xp,
                priceService.calculateProductPrice(sessionProduct),
                subTotalXp,
                sessionProduct.amount,
                sessionProduct.created
            )
        }
    }

    fun getSessionsAsAdmin(offset: Int, size: Int): DataContainer<List<SessionView>> {
        val pageRequest = PageRequest.of(offset / size, size)
        val isAdmin = Utils().isAdmin()
        val sessions = if (isAdmin) {
            getSessionsAsAdmin(pageRequest)
        } else {
            getSessionFromCurrentUser(pageRequest)
        }
        return DataContainer(
            sessions.content.sortedByDescending { it.sessionId },
            sessions.totalElements,
            sessions.pageable.pageSize,
            offset + size
        )
    }

    fun getSessionsAsAdmin(pageRequest: PageRequest): Page<SessionView> {
        return sessionRepo.findAllByOrderByIdDesc(pageRequest).map {
            val sessionView = it.toView()
            val totalPrice = it.sessionProducts.sumOf { it.subTotalPrice }
            val totalXp = it.sessionProducts.sumOf { it.subTotalXp }
            sessionView.copy(totalPrice = totalPrice, totalXp = totalXp)
        }
    }

    fun getSessionFromCurrentUser(pageRequest: PageRequest): Page<SessionView> {
        return sessionRepo.findAllByAccountIdOrderByIdDesc(
            Utils().getActiveUser().id,
            pageRequest
        ).map {
            val sessionView = it.toView()
            val totalPrice = it.sessionProducts.sumOf { it.subTotalPrice }
            val totalXp = it.sessionProducts.sumOf { it.subTotalXp }
            sessionView.copy(totalPrice = totalPrice, totalXp = totalXp)
        }
    }

    fun getSession(id: Long): SessionSummaryView {
        val session =
            sessionRepo.findByIdOrNull(id) ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, SESSION_NOT_FOUND)
        val currentPlayMinutes =
            Utils.getDuration(session.checkIn, session.checkOut ?: Utils().currentLocalDateTime()).toMinutes()
        return createSessionView(session, currentPlayMinutes)
    }

    private fun createSessionView(
        session: Session,
        currentPlayMinutes: Long
    ): SessionSummaryView {
        val products = getSessionProducts(session, currentPlayMinutes).sortedByDescending { it.created }
        val xpSession = products.sumOf { it.subTotalXp!! }
        val priceSession = products.sumOf { it.subTotalPrice!! }
        return SessionSummaryView(
            session.id,
            session.checkIn,
            session.checkOut,
            currentPlayMinutes,
            products,
            xpSession,
            priceSession
        )
    }

    @Scheduled(cron = "0 0 2 * * *")
    fun deleteOpenSessions() {
        val openSessions = sessionRepo.findAllByCheckOutIsNull()
        logger.info("Cleaning up ${openSessions.size} open sessions")
        openSessions.forEach { session ->
            session.checkOut = Utils().currentLocalDateTime()
            session.correctCheckout = false
            val account = session.account
            accountService.updateAccount(account.toView().copy(missedCheckouts = account.missedCheckouts + 1))
            sessionRepo.save(session)
//      maybe email user?
//      maybe email cafe?
//      maybe notify user through ws?
        }
        if (openSessions.isNotEmpty()) {
            emailService.lingeringSessions(openSessions)
        }
    }
}
