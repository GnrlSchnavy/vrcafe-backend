package nl.vrcafe.backend.service

import nl.vrcafe.backend.controller.ROLE_NOT_FOUND
import nl.vrcafe.backend.model.form.CreateRoleForm
import nl.vrcafe.backend.model.jpa.account.Role
import nl.vrcafe.backend.repo.RoleRepo
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class RoleService {

  @Autowired
  lateinit var roleRepo: RoleRepo

  val logger = LoggerFactory.getLogger(RoleService::class.java)

  fun createRole(role: CreateRoleForm): Role {
    logger.info("Creating role: $role")
    return roleRepo.save(Role(-1, role.name))
  }

  fun deleteRole(id: Long) {
    logger.info("Deleting role with id: $id")
    val role = getRole(id)
    return roleRepo.delete(role)
  }

  fun getRole(id: Long): Role {
    return roleRepo.findByIdOrNull(id)
      ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, ROLE_NOT_FOUND)
  }

  fun updateRole(id: Long, role: CreateRoleForm): Role {
    logger.info("Updating role with id: $id")
    val existingRole = getRole(id)
    val newRole = Role(existingRole.id, role.name)
    return roleRepo.save(newRole)
  }

  fun getAllRoles(): List<Role> {
    return roleRepo.findAll()
  }

}
