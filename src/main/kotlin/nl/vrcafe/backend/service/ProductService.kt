package nl.vrcafe.backend.service

import nl.vrcafe.backend.controller.PRODUCT_NOT_FoUND
import nl.vrcafe.backend.model.DataContainer
import nl.vrcafe.backend.model.ProductTypeEnum
import nl.vrcafe.backend.model.form.CreateProductForm
import nl.vrcafe.backend.model.jpa.Product
import nl.vrcafe.backend.model.jpa.xp.XpMap
import nl.vrcafe.backend.model.view.ProductView
import nl.vrcafe.backend.repo.ProductRepo
import nl.vrcafe.backend.util.Utils
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class ProductService : AbstractSearchService() {

  @Autowired
  lateinit var productRepository: ProductRepo

  @Autowired
  lateinit var xpMapService: XpMapService

  val logger = LoggerFactory.getLogger(ProductService::class.java)

  fun createProduct(product: CreateProductForm): ProductView {
    logger.info("Creating product with name: ${product.name}")
    val xpMap = xpMapService.createXpMap(
      XpMap(
        -1,
        null,
        product.xpMap?.amount ?: Utils().cantBeNull("xpAmount"),
        product.xpMap.reward ?: Utils().cantBeNull("xpReward")
      )
    )
    val product = Product(
      -1,
      product.name ?: Utils().cantBeNull("Product name"),
      product.price ?: Utils().cantBeNull("Product price"),
      xpMap,
      product.productType ?: Utils().cantBeNull("Product type")
    )
    return productRepository.save(product).toView()
  }

  fun getAllProducts(offset: Int, size: Int, query: String?): DataContainer<List<ProductView>> {
    val pageRequest = PageRequest.of(offset / size, size)
    val fields = createQueryFields(query ?: "", "Product")
    val products = productRepository.findByNameContainingIgnoreCaseAndProductTypeContainingIgnoreCase(
      fields["name"] ?: "",
      fields["type"] ?: "",
      pageRequest
    ).map { it.toView() }
    return DataContainer(products.content, products.totalElements, products.pageable.pageSize, offset + size)
  }

  fun getProductById(id: Long): Product? {
    return productRepository.findByIdOrNull(id) ?: throw ResponseStatusException(
      HttpStatus.NOT_FOUND,
      PRODUCT_NOT_FoUND
    )
  }

  fun updateProduct(product: CreateProductForm): Product {
    logger.info("Updating product with name: ${product.name}")
    val productToUpdate = getProductById(product.id!!)
    val xpMap = product.xpMap?.let { xpMapService.updateXpMap(it) }
    val newProduct = Product(
      product.id,
      product.name ?: productToUpdate!!.name,
      product.price ?: productToUpdate!!.price,
      xpMap ?: productToUpdate!!.xpMap,
      product.productType ?: productToUpdate!!.productType
    )
    return productRepository.save(newProduct)
  }

  fun removeProduct(id: Long) {
    logger.info("Removing product with id: $id")
    productRepository.deleteById(id)
  }

  fun getProductByProductType(productType: ProductTypeEnum): Product {
    return productRepository.findProductByProductType(productType.name) ?: throw ResponseStatusException(
      HttpStatus.NOT_FOUND,
      PRODUCT_NOT_FoUND
    )
  }

}
