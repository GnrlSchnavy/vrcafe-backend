package nl.vrcafe.backend.service

import org.springframework.beans.factory.annotation.Value
import org.springframework.security.crypto.encrypt.Encryptors
import org.springframework.security.crypto.encrypt.TextEncryptor
import org.springframework.stereotype.Service


@Service
class EncryptionService {
    @Value("\${security.crypto.password:password}")
    private var password = ""

    @Value("\${security.crypto.salt:3b2b844f1ef02cf2}")
    private var salt = ""

    val encryptor: TextEncryptor by lazy {
        Encryptors.text(password, salt)
    }

    fun encrypt(value: String): String {
        return encryptor.encrypt(value)
    }

    fun decrypt(value: String): String {
        return encryptor.decrypt(value)
    }
}
