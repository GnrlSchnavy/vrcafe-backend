package nl.vrcafe.backend.service

import nl.vrcafe.backend.config.WebSecurityConfig
import nl.vrcafe.backend.controller.ACCOUNT_NOT_FOUND
import nl.vrcafe.backend.repo.AccountRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException


@Service
class MyUserDetailsService : UserDetailsService {

    @Autowired
    lateinit var accountRepository: AccountRepo

    override fun loadUserByUsername(email: String): UserDetails {
        val account = accountRepository.findByEmail(email)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, ACCOUNT_NOT_FOUND)
        return WebSecurityConfig.MyUserPrincipal(account)
    }
}