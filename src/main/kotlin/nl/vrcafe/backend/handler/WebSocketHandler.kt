package nl.vrcafe.backend.handler

import MessageType
import WebsocketMessage
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import nl.vrcafe.backend.controller.WEBSOCKET_NOT_ACTIVE
import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException
import org.springframework.web.socket.CloseStatus
import org.springframework.web.socket.TextMessage
import org.springframework.web.socket.WebSocketSession
import org.springframework.web.socket.handler.TextWebSocketHandler

class WebSocketHandler : TextWebSocketHandler() {
  private val logger = org.slf4j.LoggerFactory.getLogger(WebSocketHandler::class.java)
  private val mapper = jacksonObjectMapper().registerModule(JavaTimeModule())

  companion object {
    val webSocketSessions: HashMap<String, WebSocketSession> = hashMapOf()
  }

  override fun afterConnectionEstablished(session: WebSocketSession) {
    logger.info("Connection established for $session.id")
    val user = "unknown_user-${session.id}" //TODO add datetime to remove unknown users after 24 hours
    webSocketSessions[user] = session
    logger.info("Open connections: $webSocketSessions.keys")
//    sendToUser(user, "connection established", MessageType.USER_WEBSOCKET_ESTABLISHED) //TODO YS add later to do confirmation?
  }

  override fun handleTextMessage(session: WebSocketSession, message: TextMessage) {
    val processedMessage = processMessage(message.payload)
    when (processedMessage.command) {
      Command.LOGIN -> {
        webSocketSessions[processedMessage.email] = session
        webSocketSessions.remove("unknown_user-${session.id}")
        logger.info("Open connections: $webSocketSessions.keys")
//        sendToUser(processedMessage.email, "$processedMessage logged in succesful", MessageType.LOGIN) //TODO YS add later to do confirmation on client side?
      }
      Command.LOGOUT -> {
        sendToUser(processedMessage.email, "{closing connection}", MessageType.LOGOUT)
//        webSocketSessions.remove(processedMessage.email)
      }
      Command.PING -> {
        sendToUser(processedMessage.email, "PONG", MessageType.PONG, false)
      }
    }
  }

  override fun afterConnectionClosed(session: WebSocketSession, status: CloseStatus) {
    logger.info("Connection closed for $session.id")
    val key = webSocketSessions.filterValues { it.id == session.id }.keys
    if(key.isNotEmpty()) {
      webSocketSessions.remove(key.first())
    }
  }

  fun sendToUser(email: String, message: Any, type: MessageType, throwError: Boolean = true) {
    val session = webSocketSessions[email] ?: run{
      logger.info("User $email is not connected")
      if(throwError) {
        throw ResponseStatusException(HttpStatus.BAD_REQUEST, WEBSOCKET_NOT_ACTIVE)
      } else {
        return
      }
    }
    val websocketMessage = WebsocketMessage(type, message)
    val jsonWebsocketMessage = mapper.writeValueAsString(websocketMessage)
    logger.info("Notifying user $email with message $message")
    session.sendMessage(TextMessage(jsonWebsocketMessage))
  }

  fun sendToAllUsers(s: String, c: MessageType) {
    webSocketSessions.values.forEach {
      it.sendMessage(TextMessage(mapper.writeValueAsString(WebsocketMessage(c, s))))
    }
  }

  private fun processMessage(message: String): Message {
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    return mapper.readValue(message, Message::class.java) //TODO handle exceptions on missing or wrong properties
  }

  fun logout(user: String) {
    webSocketSessions.remove(user)
  }

  data class Message(
    val command: Command,
    val email: String,
    val data: MessageData? = null
  )

  data class MessageData(
    val param1: String,
    val param2: String
  )

  enum class Command {
    LOGIN, LOGOUT, PING
  }


}
