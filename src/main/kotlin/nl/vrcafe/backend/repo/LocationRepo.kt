package nl.vrcafe.backend.repo

import nl.vrcafe.backend.model.jpa.Location
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface LocationRepo : JpaRepository<Location, Long> {
    fun findLocationById(id: Long): Location?
    fun findLocationsByIdIn(ids: List<Long>): List<Location>
}
