package nl.vrcafe.backend.repo

import nl.vrcafe.backend.model.jpa.ChatMessage
import nl.vrcafe.backend.model.jpa.FriendList
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface ChatMessageRepo: JpaRepository<ChatMessage, Long>{
    @Query(
        value =
        "SELECT cm.* from chat_message cm " +
                "where (cm.sender_id = ?1 and cm.recipient_id = ?2) " +
                "or (cm.sender_id = ?2 and cm.recipient_id = ?1) " +
                "order by cm.created desc",
        nativeQuery = true
    )
    fun findSendAndReceivedMessages(accountId: Long, correspondingId: Long ,pageable: Pageable): Page<ChatMessage>
}
