package nl.vrcafe.backend.repo

import nl.vrcafe.backend.model.jpa.xp.XpEvent
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.time.LocalDateTime


@Repository
interface XpEventRepo : JpaRepository<XpEvent, Long> {
  fun findAllByEventStartBeforeAndEventEndAfterAndActivated(start: LocalDateTime, end: LocalDateTime, activated: Boolean): List<XpEvent>
  fun findAllByEventEndBeforeAndActivated(end: LocalDateTime, activated: Boolean): List<XpEvent>
}
