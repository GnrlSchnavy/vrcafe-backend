package nl.vrcafe.backend.repo

import nl.vrcafe.backend.model.jpa.achievement.Achievement
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository


@Repository
interface AchievementRepo: JpaRepository<Achievement, Long> {
  fun findByName(s: String): Achievement?

}
