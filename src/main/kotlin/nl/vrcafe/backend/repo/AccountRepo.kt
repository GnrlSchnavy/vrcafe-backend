package nl.vrcafe.backend.repo

import nl.vrcafe.backend.model.jpa.Profile
import nl.vrcafe.backend.model.jpa.account.Account
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.time.Instant
import java.util.*

@Repository
interface AccountRepo : JpaRepository<Account, Long> {
  fun findByProfile(profile: Profile): Account?
  fun findByEmail(email: String): Account?
  fun findByQrCode(qrCode: String): Account?
  fun findByForgotPasswordToken(token: UUID): Account?
  fun findByRecoverPasswordTimeBefore(now: Instant): List<Account>
  fun findByEmailContainingIgnoreCaseAndRolesName(
    email: String? = "",
    role: String,
    pageable: Pageable
  ): Page<Account>

  @Query(
    value =
      "SELECT a.* from account a " +
      "left join session s on a.id = s.account_id " +
      "where a.email like %:email% " +
          "order by s.check_in desc",
       nativeQuery = true
  )
  fun getAccountsSortByLatestSesssion(email: String, pageable: Pageable): Page<Account>

  @Query(
    value =
    "SELECT a.* from account a " +
            "left join friend_list fl on a.id = fl.account_id " +
            "where fl.account_id = ?1",
    nativeQuery = true
  )
  fun findAllFriends(accountId: Long, pageable: Pageable): Page<Account>

}
