package nl.vrcafe.backend.repo

import nl.vrcafe.backend.model.jpa.Product
import nl.vrcafe.backend.model.jpa.session.Session
import nl.vrcafe.backend.model.jpa.session.SessionProduct
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface SessionProductRepo: JpaRepository<SessionProduct, Long> {
  fun findAllBySession(session: Session): List<SessionProduct>
  fun findFirstBySessionAndProduct(session: Session, product: Product): SessionProduct
}
