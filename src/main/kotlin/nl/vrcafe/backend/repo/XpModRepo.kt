package nl.vrcafe.backend.repo

import nl.vrcafe.backend.model.jpa.xp.XpMod
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface XpModRepo : JpaRepository<XpMod, Long> {
}
