package nl.vrcafe.backend.repo

import nl.vrcafe.backend.model.jpa.account.Account
import nl.vrcafe.backend.model.jpa.achievement.Achievement
import nl.vrcafe.backend.model.jpa.achievement.AchievementProgress
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository


@Repository
interface AchievementProgressRepo: JpaRepository<AchievementProgress, Long> {
  fun findByAccountAndAchievement(account: Account, achievement: Achievement): AchievementProgress?
  fun findAllByAccountIdOrderByPercentageDesc(id: Long, pageRequest: Pageable): Page<AchievementProgress>
}
