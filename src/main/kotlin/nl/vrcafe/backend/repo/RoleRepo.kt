package nl.vrcafe.backend.repo

import nl.vrcafe.backend.model.jpa.account.Role
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface RoleRepo: JpaRepository<Role, Long> {
    fun findByName(name: String): Role?
}
