package nl.vrcafe.backend.repo

import nl.vrcafe.backend.model.jpa.Product
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ProductRepo: JpaRepository<Product, Long> {
    fun findProductByProductType(productType: String): Product?
    fun findByNameContainingIgnoreCaseAndProductTypeContainingIgnoreCase(name: String, productType: String, pageable: Pageable): Page<Product>
}
