package nl.vrcafe.backend.repo

import nl.vrcafe.backend.model.jpa.xp.XpMap
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository


@Repository
interface XpMapRepo : JpaRepository<XpMap, Long> {
}
