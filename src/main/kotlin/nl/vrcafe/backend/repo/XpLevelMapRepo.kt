package nl.vrcafe.backend.repo

import nl.vrcafe.backend.model.jpa.xp.XpLevelMap
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository


@Repository
interface XpLevelMapRepo : JpaRepository<XpLevelMap, Long> {

    fun findByXpGreaterThanEqual(xp: Long): List<XpLevelMap>
    fun findByLevel(level: Long): XpLevelMap
}
