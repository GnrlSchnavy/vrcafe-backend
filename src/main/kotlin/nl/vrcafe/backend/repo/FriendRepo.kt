package nl.vrcafe.backend.repo

import nl.vrcafe.backend.model.jpa.FriendList
import nl.vrcafe.backend.model.jpa.account.Account
import org.bouncycastle.math.ec.rfc7748.X448.Friend
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface FriendRepo: JpaRepository<FriendList, Long>{
    @Query(
        value =
        "SELECT fl.* from friend_list fl " +
                "left join account a on a.id = fl.account_id " +
                "where fl.friend_id = ?1 " +
                "and fl.request_status = 'ACCEPTED'",
        nativeQuery = true
    )
    fun findAllFriends(accountId: Long, pageable: Pageable): Page<FriendList>
    fun deleteByAccountAndFriend(account: Account, it: Account)
    fun findByAccountAndFriend(account: Account, friendAccount: Account): FriendList?
    fun findAllByAccountAndRequestStatus(account: Account, name: String, pageable: Pageable): Page<FriendList>

    @Query(
        value =
        "SELECT fl.* from friend_list fl " +
                "left join account a on a.id = fl.account_id " +
                "where ((fl.account_id = ?1 and fl.friend_id = ?2)" +
                "or (fl.friend_id =?1 and fl.account_id = ?2))" +
                "and fl.request_status = 'ACCEPTED'",
        nativeQuery = true
    )
    fun findFriendRelation(accountId1: Long, accountId2: Long, pageable: Pageable): Page<FriendList>
}
