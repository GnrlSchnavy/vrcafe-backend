package nl.vrcafe.backend.repo

import nl.vrcafe.backend.model.jpa.xp.XpMapHistory
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface XpMapHistoryRepo : JpaRepository<XpMapHistory, Long>
