package nl.vrcafe.backend.repo

import nl.vrcafe.backend.model.jpa.Profile
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ProfileRepo : JpaRepository<Profile, Long> {

    fun findDistinctFirstByUsername(username: String): Profile?

}
