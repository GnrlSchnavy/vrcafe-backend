package nl.vrcafe.backend.repo

import nl.vrcafe.backend.model.jpa.account.Account
import nl.vrcafe.backend.model.jpa.xp.XpAccountHistory
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface XpAccountHistoryRepo : JpaRepository<XpAccountHistory, Long> {
  fun findByAccountOrderByCreatedDesc(account: Account, pageable: Pageable): Page<XpAccountHistory>
}
