package nl.vrcafe.backend.repo

import nl.vrcafe.backend.model.jpa.achievement.Achievement
import nl.vrcafe.backend.model.jpa.achievement.AchievementLevelMap
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface AchievementLevelMapRepo: JpaRepository<AchievementLevelMap, Long> {
    fun findFirstByAchievementAndMinValueLessThanEqualAndMaxValueAfter(achievement: Achievement, minValue: Long, maxValue: Long): AchievementLevelMap
    fun findByAchievementAndLevel(achievement: Achievement, i: Long): AchievementLevelMap
}
