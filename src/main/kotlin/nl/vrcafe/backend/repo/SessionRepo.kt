package nl.vrcafe.backend.repo

import nl.vrcafe.backend.model.jpa.session.Session
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface SessionRepo : JpaRepository<Session, Long> {
    fun findFirstByAccountIdOrderByIdDesc(id: Long): Session?
    fun findAllByAccountIdOrderByIdDesc(id: Long, pageable: Pageable): Page<Session>
    fun findAllByOrderByIdDesc (pageable: Pageable): Page<Session>
    fun findAllByCheckOutIsNull(): List<Session>
}
