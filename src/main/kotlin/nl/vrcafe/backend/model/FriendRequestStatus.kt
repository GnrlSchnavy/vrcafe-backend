package nl.vrcafe.backend.model

enum class FriendRequestStatus {
    PENDING, ACCEPTED, DECLINED
}