package nl.vrcafe.backend.model.form

data class RegisterForm (
    val email: String,
    val password: String
)
