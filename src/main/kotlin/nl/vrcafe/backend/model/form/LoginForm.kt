package nl.vrcafe.backend.model.form


data class LoginForm (
  val email: String,
  val password: String
)

