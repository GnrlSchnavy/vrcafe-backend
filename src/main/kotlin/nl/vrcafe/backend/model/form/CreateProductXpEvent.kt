package nl.vrcafe.backend.model.form

data class CreateProductXpEvent(
  val productId: Long,
  val tempAmount: Long,
  val tempReward: Long,
)
