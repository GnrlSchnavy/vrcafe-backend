package nl.vrcafe.backend.model.form

import java.time.LocalDateTime

data class CreateXpEventForm(
  val id : Long? = null,
  val name: String? = null,
  val description: String? = null,
  val eventStart: LocalDateTime? = null,
  val eventEnd: LocalDateTime? = null,
  val eventProducts: List<CreateProductXpEvent>? = null
)
