package nl.vrcafe.backend.model.form

data class RoleToAccountForm (
  val email: String,
  val role: String
  )

