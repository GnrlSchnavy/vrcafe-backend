package nl.vrcafe.backend.model.form

data class UpdateProfileForm(
    val firstName: String? = null,
    val lastName: String? = null,
    val username: String? = null ,
    val profilePicture: String? = null
)