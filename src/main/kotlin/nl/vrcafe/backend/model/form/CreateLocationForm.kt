package nl.vrcafe.backend.model.form

data class CreateLocationForm (
    val name: String? = null,
    val city: String? = null,
    val street: String? = null,
    val houseNumber: String? = null,
)
