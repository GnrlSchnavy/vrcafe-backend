package nl.vrcafe.backend.model.form

import nl.vrcafe.backend.model.view.XpMapView

data class CreateProductForm(
  val id: Long? = null,
  val name: String? = null,
  val price: Long? = null,
  val xpMap: XpMapView? = null,
  val productType: String? = null

)
