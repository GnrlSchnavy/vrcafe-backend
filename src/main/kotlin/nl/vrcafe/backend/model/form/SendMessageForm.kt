package nl.vrcafe.backend.model.form

data class SendMessageForm(
    val message: String,
    val receiverId: Long
)