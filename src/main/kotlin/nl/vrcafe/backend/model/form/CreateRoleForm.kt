package nl.vrcafe.backend.model.form

data class CreateRoleForm(
  val name: String
)
