package nl.vrcafe.backend.model.form

import java.util.*

data class UpdatePasswordForm(
  val password: String,
  val token: UUID
)

