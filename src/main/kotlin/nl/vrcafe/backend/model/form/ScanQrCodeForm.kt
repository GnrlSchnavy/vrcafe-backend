package nl.vrcafe.backend.model.form
data class ScanQrCodeForm(
        val qrCode: String,
)
