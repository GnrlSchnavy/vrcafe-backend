package nl.vrcafe.backend.model.form

data class AddProductsToSessionForm(
 val productId: Long,
 val amount: Long
)
