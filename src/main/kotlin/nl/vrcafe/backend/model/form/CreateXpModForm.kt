package nl.vrcafe.backend.model.form


data class CreateXpModForm(
  val xpMapId: Long,
  val tempAmount: Long,
  val tempReward: Long
)
