package nl.vrcafe.backend.model.view

data class ProductView(
  val id: Long? = null,
  val name: String? = null,
  val price: Long? = null,
  val type: String? = null
)
