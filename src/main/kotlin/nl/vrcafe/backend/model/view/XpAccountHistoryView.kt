package nl.vrcafe.backend.model.view

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import java.sql.Timestamp


@JsonIgnoreProperties(ignoreUnknown = true)
data class XpAccountHistoryView(
  val id: Long? = null,
  val account: AccountView? = null,
  val sessionProductView: SessionProductView? = null,
  val amount: Long? = null,
  val reward: Long? = null,
  val actorName: String? = null,
  val created: Timestamp? = null
)

