package nl.vrcafe.backend.model.view

//TODO maybe consolidate this with SessionProductView
data class AddProductToSessionView(
  val id: Long? = null,
  val productId: Long? = null,
  val name: String? = null,
  val amount: Long? = null,
  val unitPrice: Long? = null,
  val unitXp: Long? = null,
  val subTotalPrice: Long? = null,
  val subTotalXp: Long? = null,
  val created: String? = null
)
