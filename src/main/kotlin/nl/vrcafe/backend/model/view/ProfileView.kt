package nl.vrcafe.backend.model.view

data class ProfileView(
    val id: Long? = null,
    val firstName: String? = null,
    val lastName: String? = null,
    val username: String? = null,
    val profilePicture: String? = null,
)