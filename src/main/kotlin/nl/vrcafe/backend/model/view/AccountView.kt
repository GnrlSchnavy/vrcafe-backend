package nl.vrcafe.backend.model.view

import java.time.Instant
import java.util.*

data class AccountView(
    val id: Long? = null,
    val jwt: String? = null,
    val qrCode: String? = null,
    val email: String? = null,
    val role: String? = null,
    val encodedQrCode: String? = null,
    val xp: Long? = null,
    val level: Long? = null,
    val forgotPasswordToken: UUID? = null,
    val password: String? = null,
    val recoverPasswordTime: Instant? = null,
    val rankName: String? = null,
    val missedCheckouts: Long? = null,
    val profileView: ProfileView? = null,
) {
}
