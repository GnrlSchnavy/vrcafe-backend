data class WebsocketMessage(
  val type: MessageType,
  val data: Any
)

enum class MessageType{
  USER_SESSION_CHECKIN,
  USER_SESSION_CHECKOUT,
  USER_SESSION_PRODUCT_ADDED,
  LOGOUT,
  SERVER_RESTART,
  CHAT_MESSAGE,
  PONG,
  FRIEND_REQUEST
}

