package nl.vrcafe.backend.model.view

data class ChatMessageView(
    val chatMessage: String,
    val senderId: Long,
    val recipientId: Long,
    var senderName: String? = null,
    var recipientName: String? = null,
    val created: String
)