package nl.vrcafe.backend.model.view

data class TinyAccountView(
  val accountId: Long,
  val email: String,
  val xp: Long,
  val rank: String,
  val level: Long,
  val levelPercent: Double,
  val username: String
)
