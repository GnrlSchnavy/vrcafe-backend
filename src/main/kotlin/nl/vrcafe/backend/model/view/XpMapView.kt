package nl.vrcafe.backend.model.view

import nl.vrcafe.backend.model.jpa.xp.XpMod

data class XpMapView(
  val id: Long?,
  val xpMod: XpMod?,
  val amount: Long?,
  val reward: Long?
)
