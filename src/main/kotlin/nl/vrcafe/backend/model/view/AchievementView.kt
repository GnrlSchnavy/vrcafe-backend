package nl.vrcafe.backend.model.view

data class AchievementView(
  val id: Long,
  val achievementName: String,
  val achievementDescription: String,
  val currentRank: String,
  val percentage: Number,
)
