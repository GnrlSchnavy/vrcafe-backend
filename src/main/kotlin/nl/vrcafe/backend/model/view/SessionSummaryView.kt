package nl.vrcafe.backend.model.view

import java.sql.Timestamp
import java.time.LocalDateTime

data class SessionSummaryView(
  val sessionId: Long,
  val checkIn: LocalDateTime?,
  val checkOut: LocalDateTime?,
  val minutes: Long?,
  val products: List<SessionProductView>? = emptyList(),
  val xp: Long?,
  val price: Long?
)

data class SessionProductView(
  val id: Long? = null,
  val name: String? = null,
  val unitPrice: Long? = null,
  val unitXp: Long? = null,
  val subTotalPrice: Long? = null,
  val subTotalXp: Long? = null,
  val amount: Long? = null,
  val created: Timestamp? = null
)
