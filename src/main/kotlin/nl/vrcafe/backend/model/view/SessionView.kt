import java.time.LocalDateTime

data class SessionView(
    val sessionId: Long,
    val checkIn: LocalDateTime,
    val checkOut: LocalDateTime?,
    val minutes: Long?,
    val totalXp: Long? = null,
    val totalPrice: Long? = null,
    val email: String? = null
)

