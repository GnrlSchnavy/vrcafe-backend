package nl.vrcafe.backend.model

data class DataContainer<T>(
  val data: T,
  val total: Long,
  val size: Int,
  val offset: Int
)
