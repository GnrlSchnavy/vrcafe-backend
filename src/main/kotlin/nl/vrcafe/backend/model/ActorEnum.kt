package nl.vrcafe.backend.model

enum class ActorEnum {
  SYSTEM, PLAYTIME, SESSION_CLOSE
}
