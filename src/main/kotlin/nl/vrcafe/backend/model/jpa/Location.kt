package nl.vrcafe.backend.model.jpa

import nl.vrcafe.backend.model.jpa.session.Session
import jakarta.persistence.*
import kotlin.reflect.full.memberProperties

@Entity
data class Location(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "location_sequence")
    val id: Long = 0,
    val name: String,
    val city: String,
    val street: String?,
    val houseNumber: String?,
)
{
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "location")
    var sessions: MutableList<Session> = mutableListOf()
    fun withSessions(sessions: MutableList<Session>): Location {
        this.sessions = sessions
        return this
    }
    inline fun <reified T : Any> T.asMap() : Map<String, Any?> {
        val props = T::class.memberProperties.associateBy { it.name }
        return props.keys.associateWith { props[it]?.get(this) }
    }
}
