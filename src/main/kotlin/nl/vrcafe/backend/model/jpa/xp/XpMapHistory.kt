package nl.vrcafe.backend.model.jpa.xp

import com.fasterxml.jackson.annotation.JsonIgnore
import java.time.LocalDateTime
import jakarta.persistence.*


@Entity
class XpMapHistory (
  @Id @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "xp_map_history_sequence")
    val id: Long,
  @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "xp_map_id")
    @JsonIgnore
    val xpMap: XpMap,
  val amount: Long,
  val reward: Long,
  val actorName: String,
  val created: LocalDateTime
)
