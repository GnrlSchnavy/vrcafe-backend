package nl.vrcafe.backend.model.jpa.account

import jakarta.persistence.*
import nl.vrcafe.backend.model.jpa.Profile
import nl.vrcafe.backend.model.jpa.achievement.AchievementProgress
import nl.vrcafe.backend.model.jpa.session.Session
import nl.vrcafe.backend.model.jpa.xp.XpAccountHistory
import nl.vrcafe.backend.model.view.AccountView
import nl.vrcafe.backend.model.view.TinyAccountView
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.*

@Entity
class Account(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "account_sequence")
    val id: Long,
    val email: String,
    val password: String,
    val qrCode: String,
    val encodedQrCode: String,
    val xp: Long,
    val level: Long,
    val rank: String,
    val forgotPasswordToken: UUID? = null,
    val recoverPasswordTime: Instant?,
    var missedCheckouts: Long =  0,
    @OneToOne(cascade = [CascadeType.ALL])
    val profile: Profile,
    val created: LocalDateTime? = ZonedDateTime.now( ZoneId.of( "Europe/Amsterdam" ) ).toLocalDateTime()
) {

    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    var sessions: MutableList<Session> = mutableListOf()
    fun withSessions(sessions: MutableList<Session>): Account {
        this.sessions = sessions
        return this
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
    var xpAccountHistory: List<XpAccountHistory> = mutableListOf()
    fun withXpAccountHistory(xpAccountHistory: List<XpAccountHistory>): Account {
        this.xpAccountHistory = xpAccountHistory
        return this
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
    var achievementProgess: List<AchievementProgress> = mutableListOf()
    fun withAchievementProgress(achievementProgress: List<AchievementProgress>): Account {
        this.achievementProgess = achievementProgress
        return this
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name = "account_role",
        joinColumns = [JoinColumn(name = "account_id")],
        inverseJoinColumns = [JoinColumn(name = "role_id")]
    )
    var roles: MutableList<Role> = mutableListOf()
    fun withRoles(roles: MutableList<Role>): Account {
        this.roles = roles
        return this
    }

    fun toTinyAccountView(): TinyAccountView{
        return TinyAccountView(
            email = this.email,
            xp = this.xp,
            level = this.level,
            accountId = this.id,
            rank = this.rank,
            levelPercent = 0.0,
            username =  this.profile.username
        )
    }

    fun toView(): AccountView {
        return AccountView(
            id = this.id,
            email = this.email,
            qrCode = this.qrCode,
            encodedQrCode = this.encodedQrCode,
            xp = this.xp,
            level = this.level,
            role = this.roles.firstOrNull()?.name,
            rankName = this.rank,
            profileView = this.profile.toView(),
        )
    }
}
