package nl.vrcafe.backend.model.jpa.achievement

import com.fasterxml.jackson.annotation.JsonIgnore
import jakarta.persistence.*

@Entity
class Achievement(
  @Id @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "achievement_sequence")
  val id: Long,
  val name: String,
  val description: String
  ){
  @JsonIgnore
  @OneToMany(mappedBy = "achievement", fetch = FetchType.LAZY)
  var achievementLevelMaps: List<AchievementLevelMap> = listOf()
  fun withAchievementLevelMaps(achievementLevelMaps: List<AchievementLevelMap>): Achievement {
    this.achievementLevelMaps = achievementLevelMaps
    return this
  }
}
