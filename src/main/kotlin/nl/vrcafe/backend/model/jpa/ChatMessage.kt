package nl.vrcafe.backend.model.jpa

import nl.vrcafe.backend.listener.ChatMessageListener
import nl.vrcafe.backend.model.view.ChatMessageView
import java.time.LocalDateTime
import jakarta.persistence.*

@Entity
@EntityListeners(ChatMessageListener::class)
data class ChatMessage(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "chat_message_sequence")
    val id: Long,
    var chatMessage: String,
    val senderId: Long,
    val recipientId: Long,
    val created: LocalDateTime? = LocalDateTime.now()
) {
    fun withNames(senderName: String, recipientName: String): ChatMessageView {
        return ChatMessageView(
            this.chatMessage,
            this.senderId,
            this.recipientId,
            senderName,
            recipientName,
            this.created.toString()
        )
    }
}