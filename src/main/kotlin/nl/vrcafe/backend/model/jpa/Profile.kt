package nl.vrcafe.backend.model.jpa

import nl.vrcafe.backend.model.view.ProfileView
import java.time.LocalDateTime
import jakarta.persistence.*

@Entity
class Profile (
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "profile_sequence")
    val id : Long = 0,
    val firstName : String,
    val lastName: String,
    val username: String,
    val profilePicture: String,
    val created: LocalDateTime? = LocalDateTime.now()
) {
    constructor() : this(0, "", "", "", "", LocalDateTime.now())
    constructor(profile: ProfileView) : this(profile.id!!, profile.firstName!!, profile.lastName!!, profile.username!!, profile.profilePicture!!)

    fun toView() = ProfileView(
        id = id,
        firstName = firstName,
        lastName = lastName,
        username = username,
        profilePicture = profilePicture
    )
}