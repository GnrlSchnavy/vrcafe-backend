package nl.vrcafe.backend.model.jpa.xp

import com.fasterxml.jackson.annotation.JsonIgnore
import nl.vrcafe.backend.model.jpa.account.Account
import nl.vrcafe.backend.model.jpa.Product
import java.sql.Timestamp
import jakarta.persistence.*

@Entity
class XpAccountHistory (
  @Id @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "xp_type_history_sequence")
    val id: Long,
  @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id")
    val account: Account,
  @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_type_id")
    val product: Product,
  val amount: Long,
  val reward: Long,
  val actorName: String,
  val created: Timestamp = Timestamp(System.currentTimeMillis())
)
{

}
