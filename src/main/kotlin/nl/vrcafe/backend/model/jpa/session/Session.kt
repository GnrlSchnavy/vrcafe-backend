package nl.vrcafe.backend.model.jpa.session

import SessionView
import com.fasterxml.jackson.annotation.JsonIgnore
import nl.vrcafe.backend.model.jpa.Location
import nl.vrcafe.backend.model.jpa.account.Account
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime
import jakarta.persistence.*


@Entity
class Session(
  @Id @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "session_sequence")
  val id: Long = 0,
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "account_id")
  @JsonIgnore
  val account: Account,
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "location_id")
  @JsonIgnore
  val location: Location,
  val checkIn: LocalDateTime,
  var checkOut: LocalDateTime?,
  var minutes: Long?,
  var correctCheckout: Boolean? = null
) {

  constructor(
    account: Account,
    location: Location
  ) : this(-1, account, location, ZonedDateTime.now( ZoneId.of( "Europe/Amsterdam" ) ).toLocalDateTime() , null, null)

  @OneToMany(mappedBy = "session", fetch = FetchType.LAZY)
  var sessionProducts: List<SessionProduct> = listOf()
  fun withSessionProducts(sessionProducts: List<SessionProduct>): Session {
    this.sessionProducts = sessionProducts
    return this
  }

  fun isActive(): Boolean {
    return this.checkOut == null
  }

  fun toView(): SessionView {
    return SessionView(this.id, this.checkIn, this.checkOut, this.minutes, email = account.email)
  }
}

