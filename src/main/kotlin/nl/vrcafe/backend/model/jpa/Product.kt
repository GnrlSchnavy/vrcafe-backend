package nl.vrcafe.backend.model.jpa

import nl.vrcafe.backend.model.jpa.xp.XpMap
import nl.vrcafe.backend.model.view.ProductView
import java.sql.Timestamp
import jakarta.persistence.*


@Entity
class Product(
  @Id @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "product_sequence")
  val id: Long = 0,
  @Column(name = "name")
  val name: String,
  val price: Long,
  @OneToOne(fetch = FetchType.LAZY, cascade = [CascadeType.REMOVE])
  @JoinColumn(name = "xp_map_id", referencedColumnName = "id")
  var xpMap: XpMap,
  val productType: String,
  val created: Timestamp = Timestamp(System.currentTimeMillis()),
  ){
  fun toView(): ProductView {
    return ProductView(
      id = this.id,
      name = this.name,
      price = this.price,
      type = this.productType,
    )
  }
}
