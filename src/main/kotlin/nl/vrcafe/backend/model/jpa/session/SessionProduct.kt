package nl.vrcafe.backend.model.jpa.session

import com.fasterxml.jackson.annotation.JsonIgnore
import nl.vrcafe.backend.model.jpa.Product
import nl.vrcafe.backend.model.jpa.account.Account
import nl.vrcafe.backend.model.view.AddProductToSessionView
import java.sql.Timestamp
import jakarta.persistence.*

@Entity
class SessionProduct(
  @Id @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "account_sequence")
  val id: Long,
  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "session_id")
  val session: Session?,
  @OneToOne
  @JoinColumn(name = "product_id")
  val product: Product,
  @JsonIgnore
  @OneToOne
  @JoinColumn(name = "account_id")
  val account: Account,
  val amount: Long,
  val price: Long,
  val subTotalPrice: Long,
  val subTotalXp: Long,
  val created: Timestamp = Timestamp(System.currentTimeMillis()),
) {

  fun toAddProductSessionView(): AddProductToSessionView {
    val xp = if (this.amount == 0L) 1L else this.subTotalXp / this.amount
    return AddProductToSessionView(
      this.id,
      this.product.id,
      this.product.name,
      this.amount,
      this.price,
      xp,
      this.subTotalPrice,
      this.subTotalXp,
      this.created.toLocalDateTime().toString()
    )
  }

}
