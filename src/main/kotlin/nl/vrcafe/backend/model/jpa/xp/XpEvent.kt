package nl.vrcafe.backend.model.jpa.xp

import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime
import jakarta.persistence.*

import kotlin.reflect.full.memberProperties

@Entity
class XpEvent(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "xp_event_sequence")
    val id: Long,
    @Column(name="event_name")
    val name: String,
    val description: String,
    val eventStart: LocalDateTime,
    val eventEnd: LocalDateTime?,
    var activated: Boolean,
    val createdBy: String,
    val created: LocalDateTime? = ZonedDateTime.now( ZoneId.of( "Europe/Amsterdam" ) ).toLocalDateTime()
){
    @OneToMany(mappedBy = "xpEvent")
    var xpMods: List<XpMod> = listOf()
    fun withXpMods(xpMods: MutableList<XpMod>): XpEvent {
        this.xpMods = xpMods
        return this
    }
    inline fun <reified T : Any> T.asMap() : Map<String, Any?> {
        val props = T::class.memberProperties.associateBy { it.name }
        return props.keys.associateWith { props[it]?.get(this) }
    }

}
