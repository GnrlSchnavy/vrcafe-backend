package nl.vrcafe.backend.model.jpa.achievement

import com.fasterxml.jackson.annotation.JsonIgnore
import nl.vrcafe.backend.model.jpa.account.Account
import nl.vrcafe.backend.model.view.AchievementView
import jakarta.persistence.*

@Entity
class AchievementProgress(
  @Id @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "achievement_progress_sequence")
  val id: Long,
  @JsonIgnore
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "account_id")
  val account: Account,
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "achievement_id")
  val achievement: Achievement,
  val currentValue: Long,
  val currentRank: String,
  val percentage: Double
) {
  fun toView(): AchievementView {
    return AchievementView(
      id = id,
      currentRank = currentRank,
      percentage = percentage,
      achievementName = achievement.name,
      achievementDescription = achievement.description,
    )
  }
}

