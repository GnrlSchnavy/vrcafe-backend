package nl.vrcafe.backend.model.jpa.account

import com.fasterxml.jackson.annotation.JsonIgnore
import jakarta.persistence.*

@Entity
class Role(
  @Id @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "role_sequence")
  val id: Long,
  val name: String
  ){
  @JsonIgnore
  @ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY)
  var accounts: MutableList<Account> = mutableListOf()
}
