package nl.vrcafe.backend.model.jpa.xp

import com.fasterxml.jackson.annotation.JsonIgnore
import jakarta.persistence.*


@Entity
class XpMod(
  @Id @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "xp_mod_sequence")
  val id: Long,
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "xp_event_id")
  @JsonIgnore
  val xpEvent: XpEvent,
  @JsonIgnore
  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "xp_map_id")
  val xpMap: XpMap,
  val oldAmount: Long,
  val oldReward: Long,
  val tempAmount: Long,
  val tempReward: Long
)
