package nl.vrcafe.backend.model.jpa.xp

import jakarta.persistence.*


@Entity
class XpLevelMap(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "xp_level_map_sequence")
    val id: Long,
    val level: Long,
    val xp: Long,
    val rankName: String,
)
