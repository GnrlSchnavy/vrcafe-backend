package nl.vrcafe.backend.model.jpa.xp

import com.fasterxml.jackson.annotation.JsonIgnore
import jakarta.persistence.*


@Entity
class XpMap(
  @Id @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "xp_map_sequence")
    val id: Long,
  @OneToOne(fetch = FetchType.LAZY, cascade = [CascadeType.REMOVE])
    @JoinColumn(name = "xp_mod_id")
    @JsonIgnore
    val xpMod: XpMod?,
  val amount: Long,
  val reward: Long
){

    @OneToMany(mappedBy = "xpMap", fetch = FetchType.LAZY)
    var xpMapHistory: MutableList<XpMapHistory> = mutableListOf()
    fun withXpMapHistory(xpMapHistory: MutableList<XpMapHistory>): XpMap {
        this.xpMapHistory = xpMapHistory
        return this
    }

}
