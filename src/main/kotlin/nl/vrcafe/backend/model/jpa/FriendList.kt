package nl.vrcafe.backend.model.jpa

import jakarta.persistence.*
import nl.vrcafe.backend.model.jpa.account.Account


@Entity
data class FriendList(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "friend_list_sequence")
    val id: Long = 0,
    @OneToOne
    val account: Account,
    @OneToOne
    val friend: Account,
    var requestStatus: String

)