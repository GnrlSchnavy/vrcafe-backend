package nl.vrcafe.backend.model.jpa.achievement

import com.fasterxml.jackson.annotation.JsonIgnore
import jakarta.persistence.*

@Entity
class AchievementLevelMap(
  @Id @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "achievement_level_map_sequence")
  val id: Long,
  val minValue: Long,
  val maxValue: Long,
  val level: Long,
  @JsonIgnore
  @ManyToOne(fetch = FetchType.LAZY)
  val achievement: Achievement,
  val rankName: String
)
