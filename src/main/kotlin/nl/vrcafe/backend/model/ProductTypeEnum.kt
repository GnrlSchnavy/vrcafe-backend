package nl.vrcafe.backend.model

enum class ProductTypeEnum {
    PLAYTIME, FOOD, DRINKS
}
