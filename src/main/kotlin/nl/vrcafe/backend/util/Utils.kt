package nl.vrcafe.backend.util

import nl.vrcafe.backend.config.WebSecurityConfig.MyUserPrincipal
import nl.vrcafe.backend.model.jpa.account.Account
import org.springframework.http.HttpStatus
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.server.ResponseStatusException
import java.time.Duration
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime

open class Utils {
  companion object {
    fun getDuration(start: LocalDateTime, end: LocalDateTime): Duration {
      return Duration.between(start, end)
    }
  }

  fun cantBeNull(name: String): Nothing {
    throw ResponseStatusException(HttpStatus.BAD_REQUEST, "$name cannot be null")
  }

  fun getActiveUser(): Account {
    return (SecurityContextHolder.getContext().authentication.principal as MyUserPrincipal).account
  }

  fun currentLocalDateTime(): LocalDateTime {
    return ZonedDateTime.now( ZoneId.of( "Europe/Amsterdam" )).toLocalDateTime()
  }

  fun isAdmin(): Boolean{
    return (SecurityContextHolder.getContext().authentication.principal as MyUserPrincipal).account.roles.any{ it.name == "ROLE_ADMIN" }
  }

}
