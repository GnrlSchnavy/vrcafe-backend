package nl.vrcafe.backend.util

import io.jsonwebtoken.*
import nl.vrcafe.backend.config.ConfigProperties
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit
import java.util.*

@Component
class JwtUtil {

  @Autowired
  lateinit var properties: ConfigProperties

  val logger: Logger = LoggerFactory.getLogger(JwtUtil::class.java)

  fun extractUsername(token: String): String {
    try {
      val claims: Jws<Claims> = Jwts.parser().setSigningKey(properties.jwtSecret).parseClaimsJws(token)
      return claims.body.subject
    } catch (e: ExpiredJwtException){
      throw ResponseStatusException(HttpStatus.UNAUTHORIZED, "JWT expired")
    } catch (e: SignatureException){
      throw ResponseStatusException(HttpStatus.BAD_REQUEST, "LOGOUT")
    }
  }

  fun generateToken(userDetails: UserDetails): String {
    val claims: Map<String, Any> = hashMapOf()
    return createToken(claims, userDetails.username)
  }

  private fun createToken(claims: Map<String, Any>, username: String?): String {
    val now = ZonedDateTime.now( ZoneId.of( "Europe/Amsterdam" ) ).toLocalDateTime()
    val  validityInMilliseconds: Long = 86400000 * 24// one month
    val validity = now.plus(validityInMilliseconds, ChronoUnit.MILLIS)

    return Jwts.builder()
      .setClaims(claims)
      .setSubject(username)
      .setIssuedAt(Date.from(now.atZone(ZoneId.systemDefault()).toInstant()))
      .setExpiration(Date.from(validity.atZone(ZoneId.systemDefault()).toInstant()))
      .signWith(SignatureAlgorithm.HS512, properties.jwtSecret).compact()
  }

  fun validateToken(token: String, userDetails: UserDetails): Boolean {
    try {
      val claims: Jws<Claims> = Jwts.parser().setSigningKey(properties.jwtSecret).parseClaimsJws(token)
      if (claims.body.expiration.before(Date())) {
        return false
      }
      if (extractUsername(token) != userDetails.username) {
        return false
      }
      return true
    } catch (jwte: JwtException) {
      logger.warn("JWTException: ", jwte)
      return false
    } catch (iae: IllegalArgumentException) {
      logger.warn("IllegalArgumentException: ", iae)
      return false
    } catch (ex: Exception) {
      logger.error("Unknown exception: ", ex)
      return false
    }
    catch(ex: SignatureException) {
      logger.error("SignatureException: ", ex)
      return false
    }
  }
}
