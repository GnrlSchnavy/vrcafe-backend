package nl.vrcafe.backend.listener

import jakarta.persistence.PostLoad
import jakarta.persistence.PostUpdate
import jakarta.persistence.PrePersist
import jakarta.persistence.PreUpdate
import nl.vrcafe.backend.model.jpa.ChatMessage
import nl.vrcafe.backend.service.EncryptionService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component


@Component
class ChatMessageListener {

    @Autowired
    lateinit var encryptionService: EncryptionService

    @PrePersist
    fun prePersist(chatMessage: ChatMessage) {
        chatMessage.chatMessage = encryptionService.encrypt(chatMessage.chatMessage)
    }

    @PreUpdate
    fun preUpdate(chatMessage: ChatMessage) {
        chatMessage.chatMessage = encryptionService.encrypt(chatMessage.chatMessage)
    }

    @PostLoad
    fun postLoad(chatMessage: ChatMessage) {
        chatMessage.chatMessage = encryptionService.decrypt(chatMessage.chatMessage)
    }

    @PostUpdate
    fun postUpdate(chatMessage: ChatMessage) {
        chatMessage.chatMessage = encryptionService.decrypt(chatMessage.chatMessage)
    }
}