package nl.vrcafe.backend.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties("vrcafe")
class ConfigProperties {

  lateinit var jwtSecret: String

  lateinit var awsKey: String

  lateinit var awsSecret: String

}
