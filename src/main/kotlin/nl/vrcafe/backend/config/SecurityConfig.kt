package nl.vrcafe.backend.config

import nl.vrcafe.backend.model.jpa.account.Account
import nl.vrcafe.backend.util.JwtRequestFilter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.web.SecurityFilterChain
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter


@Configuration
@EnableWebSecurity
@EnableMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
class WebSecurityConfig {

    @Autowired
    lateinit var myUserDetailsService: UserDetailsService

    @Autowired
    lateinit var jwtRequestFilter: JwtRequestFilter

    @Autowired
    fun configureGlobal(auth: AuthenticationManagerBuilder) {
        auth.userDetailsService(myUserDetailsService)
    }


    @Bean
    fun filterChain(http: HttpSecurity): SecurityFilterChain {

        http.exceptionHandling { }
        http.sessionManagement { it.sessionCreationPolicy(SessionCreationPolicy.STATELESS) }
        http.csrf().disable()
        http.authorizeHttpRequests {
            it.requestMatchers(HttpMethod.POST, "/api/register/**").permitAll()
            it.requestMatchers(HttpMethod.POST, "/api/login/**").permitAll()
                .requestMatchers("/v3/api-docs/**", "/swagger-ui/**").permitAll()
            it.anyRequest().permitAll()
        }
        http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter::class.java)
        return http.build()
    }

    class MyUserPrincipal(val account: Account) : UserDetails {
        override fun getAuthorities(): MutableCollection<out GrantedAuthority> = mutableListOf()

        override fun isEnabled(): Boolean = true

        override fun getUsername(): String = account.email

        override fun isCredentialsNonExpired(): Boolean = true

        override fun getPassword(): String = account.password

        override fun isAccountNonExpired(): Boolean = true

        override fun isAccountNonLocked(): Boolean = true

    }


}
