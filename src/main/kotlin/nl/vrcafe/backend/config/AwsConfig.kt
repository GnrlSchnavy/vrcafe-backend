package nl.vrcafe.backend.config

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
class AwsConfig {

  @Autowired
  lateinit var properties: ConfigProperties

  fun awsCredentials(): AWSStaticCredentialsProvider? {
    val credentials = BasicAWSCredentials(properties.awsKey, properties.awsSecret)
    return AWSStaticCredentialsProvider(credentials)
  }

  @Bean
  fun amazonSimpleEmailService(): AmazonSimpleEmailService? {
    return AmazonSimpleEmailServiceClientBuilder.standard().withCredentials(awsCredentials())
      .withRegion("eu-central-1").build()
  }

}
