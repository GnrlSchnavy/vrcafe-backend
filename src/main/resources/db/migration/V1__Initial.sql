create sequence if not exists hibernate_sequence;

create sequence if not exists account_sequence;
create table if not exists account
(
    id                    bigint       not null default nextval('account_sequence')
        constraint vr_account_pkey primary key,
    email                 varchar(255) not null unique,
    password              varchar(255) not null,
    qr_code               varchar(255),
    encoded_qr_code       varchar,
    xp                    bigint,
    level                 bigint,
    rank                  varchar(255),
    forgot_password_token uuid,
    recover_password_time timestamp,
    missed_checkouts      bigint,
    profile_id            bigint,
    created               timestamp    not null
);

create table if not exists account_role
(
    id         bigint not null default nextval('hibernate_sequence')
        constraint vr_account_role_pkey primary key,
    account_id bigint not null,
    role_id    bigint not null
);

create sequence if not exists role_sequence;
create table if not exists role
(
    id   bigint       not null default nextval('role_sequence')
        constraint vr_role_pkey primary key,
    name varchar(255) not null unique
);

create table if not exists account_role
(
    id         bigint not null default nextval('hibernate_sequence')
        constraint vr_account_role_pkey primary key,
    account_id bigint not null,
    role_id    bigint not null
);

create sequence if not exists location_sequence;
create table if not exists location
(
    id           bigint not null default nextval('location_sequence')
        constraint vr_location_pkey primary key,
    name         varchar(255),
    city         varchar(255),
    street       varchar(255),
    house_number varchar(255),
    postal_code  varchar(255)
);

create sequence if not exists session_sequence;
create table if not exists session
(
    id               bigint not null default nextval('session_sequence')
        constraint vr_session_pkey primary key,
    account_id       bigint
        constraint vr_session_account_id_fkey
            references account (id),
    location_id      bigint,
    check_in         timestamp,
    check_out        timestamp,
    correct_checkout boolean,
    minutes          bigint
);

create sequence if not exists xp_account_history_sequence;
create table if not exists xp_account_history
(
    id              bigint       not null default nextval('xp_account_history_sequence')
        constraint vr_xp_account_history_pkey primary key,
    account_id      bigint       not null,
    amount          bigint       not null,
    reward          bigint       not null,
    actor_name      varchar(255) not null,
    product_type_id bigint       not null,
    created         timestamp    not null
);

create sequence if not exists xp_map_sequence;
create table if not exists xp_map
(
    id        bigint not null default nextval('xp_map_sequence')
        constraint vr_xp_map_pkey primary key,
    amount    bigint not null,
    reward    bigint not null,
    xp_mod_id bigint

);

create sequence if not exists xp_mod_sequence;
create table if not exists xp_mod
(
    id          bigint not null default nextval('xp_mod_sequence')
        constraint vr_xp_mod_pkey primary key,
    xp_event_id bigint not null,
    xp_map_id   bigint not null,
    old_amount  bigint not null,
    old_reward  bigint not null,
    temp_amount bigint not null,
    temp_reward bigint not null
);

create sequence if not exists xp_event_sequence;
create table if not exists xp_event
(
    id          bigint        not null default nextval('xp_event_sequence')
        constraint vr_xp_event_pkey primary key,
    event_name  varchar(255)  not null,
    description varchar(1025) not null,
    event_start timestamp     not null,
    event_end   timestamp     not null,
    activated   bool                   default false not null,
    created_by  varchar(255)  not null,
    created     timestamp     not null
);

create sequence if not exists xp_map_history_sequence;
create table if not exists xp_map_history
(
    id         bigint       not null default nextval('xp_map_history_sequence')
        constraint vr_xp_map_history_pkey primary key,
    xp_map_id  bigint       not null,
    amount     bigint       not null,
    reward     bigint       not null,
    actor_name varchar(255) not null,
    created    timestamp    not null
);

create sequence if not exists xp_level_map_sequence;
create table if not exists xp_level_map
(
    id        bigint       not null default nextval('xp_level_map_sequence')
        constraint vr_xp_level_map_pkey primary key,
    level     bigint       not null,
    xp        bigint       not null,
    rank_name varchar(255) not null
);

create sequence if not exists product_sequence;
create table if not exists product
(
    id           bigint       not null default nextval('product_sequence')
        constraint vr_product_pkey primary key,
    name         varchar(255) not null,
    price        bigint       not null,
    product_type varchar(255) not null,
    xp_map_id    bigint       not null,
    created      timestamp    not null
);

create sequence if not exists session_product_sequence;
create table if not exists session_product
(
    id              bigint    not null default nextval('session_product_sequence')
        constraint vr_session_product_pkey primary key,
    session_id      bigint,
    product_id      bigint    not null,
    account_id      bigint    not null,
    amount          bigint    not null,
    price           bigint    not null,
    sub_total_price bigint    not null,
    sub_total_xp    bigint    not null,
    created         timestamp not null
);

create sequence if not exists achievement_level_map_sequence;
create table if not exists achievement_level_map
(
    id             bigint       not null default nextval('achievement_level_map_sequence')
        constraint vr_achievement_level_map_pkey primary key,
    min_value      bigint       not null,
    max_value      bigint       not null,
    achievement_id bigint       not null,
    level          bigint       not null,
    rank_name      varchar(255) not null
);

create sequence if not exists achievement_sequence;
create table if not exists achievement
(
    id          bigint        not null default nextval('achievement_sequence')
        constraint vr_achievement_pkey primary key,
    name        varchar(255)  not null,
    description varchar(1025) not null
);

create sequence if not exists achievement_progress_sequence;
create table if not exists achievement_progress
(
    id             bigint           not null default nextval('achievement_progress_sequence')
        constraint vr_achievement_progres_pkey primary key,
    account_id     bigint           not null,
    achievement_id bigint           not null,
    current_value  bigint           not null,
    current_rank   varchar(255)     not null,
    percentage     double precision not null

);

create sequence if not exists friend_list_sequence;
create table if not exists friend_list
(
    id             bigint       not null default nextval('friend_list_sequence')
        constraint vr_friend_list_pkey primary key,
    account_id     bigint       not null,
    friend_id      bigint       not null,
    request_status varchar(255) not null
);

create sequence if not exists chat_message_sequence;
create table if not exists chat_message
(
    id           bigint        not null default nextval('chat_message_sequence')
        constraint vr_message_pkey primary key,
    sender_id    bigint        not null,
    recipient_id bigint        not null,
    chat_message varchar(2047) not null,
    created      timestamp     not null
);

create sequence if not exists profile_sequence;
create table if not exists profile
(
    id              bigint   not null default nextval('profile_sequence')
        constraint vr_profile_pkey primary key,
    first_name      varchar(255),
    last_name       varchar(255),
    username        varchar(255),
    profile_picture varchar,
    created         timestamp not null
);


