FROM adoptopenjdk/openjdk11:latest

RUN mkdir -p /opt/spring/bin/config
COPY /target/vrcafe-backend.jar /opt/spring/bin/vrcafe-backend.jar

EXPOSE 8080

USER 997

CMD java -jar  /opt/spring/bin/vrcafe-backend.jar
